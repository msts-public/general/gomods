package env

import (
	"fmt"
	"os"
	"strings"
)

// Env - contains environment values
type Env struct {
	env      map[string]string
	logLevel string
}

// NewEnv - Create a new environment
func NewEnv(envItems []string) *Env {

	e := &Env{
		env:      map[string]string{},
		logLevel: "info", // default log level
	}

	err := e.Init(envItems)
	if err != nil {
		panic(fmt.Sprintf("NewEnv failed for %v", err))
	}

	return e
}

// Init -
func (e *Env) Init(envItems []string) error {

	// if re-initializing env, clear any previous settings
	if len(e.env) > 0 {
		e.env = map[string]string{}
	}

	for _, envItem := range envItems {
		e.env[envItem] = os.Getenv(envItem)
		// set log lavel specified by env variable
		if strings.Contains(envItem, "LOG_LEVEL") {
			e.logLevel = e.env[envItem]
		}
	}

	// log when debug
	if e.logLevel == "debug" {
		for key, val := range e.env {
			// don't show passwords or keys
			if strings.Contains(key, "PASS") == false && strings.Contains(key, "KEY") == false {
				fmt.Printf("ENV key %-45s = %s\n", key, val)
			} else {
				fmt.Printf("ENV key %-45s = ********\n", key)
			}
		}
	}

	return nil
}

// Add -
func (e *Env) Add(envItems []string) {

	for _, envItem := range envItems {
		if _, ok := e.env[envItem]; ok {
			// already set
			continue
		}
		e.env[envItem] = os.Getenv(envItem)

		// log when debug
		if e.logLevel == "debug" {
			// don't show passwords or keys
			if strings.Contains(envItem, "PASS") == false && strings.Contains(envItem, "KEY") == false {
				fmt.Printf("ENV key %-45s = %s\n", envItem, e.env[envItem])
			} else {
				fmt.Printf("ENV key %-45s = ********\n", envItem)
			}
		}
	}
}

// Update -
func (e *Env) Update(envItems []string) {

	for _, envItem := range envItems {
		e.env[envItem] = os.Getenv(envItem)

		// log when debug
		if e.logLevel == "debug" {
			// don't show passwords or keys
			if strings.Contains(envItem, "PASS") == false && strings.Contains(envItem, "KEY") == false {
				fmt.Printf("ENV key %-45s = %s\n", envItem, e.env[envItem])
			} else {
				fmt.Printf("ENV key %-45s = ********\n", envItem)
			}
		}
	}
}

// CheckRequired - Tests that the provided list of environment variables
// have been set, and returns a list of missing variables
func (e *Env) CheckRequired(reqd []string) []string {

	missing := []string{}

	// required check
	for _, reqItem := range reqd {
		if val, ok := e.env[reqItem]; !ok || val == "" {
			missing = append(missing, reqItem)
		}
	}

	if len(missing) > 0 {
		return missing
	}

	return nil
}

// Get an env key
func (e *Env) Get(k string) string {
	if v, ok := e.env[k]; ok {
		return v
	}

	// if not already set add and return
	e.Add([]string{k})
	return e.env[k]
}

// GetMatch env key/value pairs where key matches 'm'
func (e *Env) GetMatch(m string) map[string]string {
	matches := map[string]string{}

	for k, v := range e.env {
		if strings.Contains(k, m) {
			matches[k] = v
		}
	}

	return matches
}

// Set an env key
func (e *Env) Set(k string, v string) {
	e.env[k] = v
}

// SetVars - Set a set of env keys
func (e *Env) SetVars(vars map[string]string) {
	for k, v := range vars {
		e.env[k] = v
	}
}

// Print -
func (e *Env) Print() {
	for key, val := range e.env {
		// don't show passwords or keys
		if strings.Contains(key, "PASS") == false && strings.Contains(key, "KEY") == false {
			fmt.Printf("ENV key %-45s = %s\n", key, val)
		} else {
			fmt.Printf("ENV key %-45s = ********\n", key)
		}
	}
}
