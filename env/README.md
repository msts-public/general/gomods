# env

Module for handling environment variables

Variables are read from environment variables when defined and stored in a map for access in code

### Setup

```golang
import "gitlab.com/msts-public/general/gomods/env"

var envVars []string{"APP_NAME", "APP_VERSION", "APP_VALUE", "APP_OTHER_VALUE"}

func main() {
  e := env.NewEnv(envVars)

  // add another env var value
  e.Add([]string{"APP_EXTRA_VALUE"})

  // get a value
  e.Get("APP_NAME")

  // override/create an env var value
  e.Set("APP_NAME", "other name")

  // override/create a set of env var values of a var
  e.SetVars(map[string]string{"APP_NAME":"other name"})

  // reload var's env value
  e.Update([]string{"APP_NAME"})

  // check vars have a value
  missing := e.CheckRequired([]string{"APP_NAME", "APP_VERSION"})

  // Get all vars that match a pattern
  logs := e.GetMatch("LOG")

  // print current env values - any var with 'PASS' or 'KEY' in the name will be masked
  e.Print()
}
```
