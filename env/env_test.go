// +build !test_fixtures,!test_integration

package env

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func init() {
	os.Setenv("APP_HOME", "env/test")
	os.Setenv("APP_ENV", "test")
	os.Setenv("APP_LOG_LEVEL", "info")
	os.Setenv("APP_PRETTY_LOGS", "1")
	os.Setenv("APP_TEST_KEY", "xyz")
}

func TestEnv(t *testing.T) {
	require := require.New(t)

	envItems := []string{"APP_HOME", "APP_ENV", "APP_LOG_LEVEL", "APP_PRETTY_LOGS"}
	// test NewEnv
	e := NewEnv(envItems)

	// test Set and Get
	appHome := e.Get("APP_HOME")
	require.NotNil(appHome, "APP_HOME should be set")
	require.NotEqual(appHome, "", "APP_HOME should have a value")

	e.Set("APP_HOME", "CORRECT")
	require.Equal(e.Get("APP_HOME"), "CORRECT", "APP_HOME should be equal to expected")

	// test reinitialization
	e.Init(envItems)
	require.Equal(e.Get("APP_HOME"), appHome, "APP_HOME should be reset")

	// test reinitialization removing variable
	envItems = []string{"APP_HOME", "APP_ENV", "APP_LOG_LEVEL"}
	e.Init(envItems)
	require.Empty(e.env["APP_PRETTY_LOGS"], "APP_PRETTY_LOGS should be unset")
	// test Get will fetch tha variable if it is available
	require.NotEmpty(e.Get("APP_PRETTY_LOGS"), "APP_PRETTY_LOGS should be fetched and set")

	// test requirement checking
	missing := e.CheckRequired([]string{"APP_HOME", "APP_ENV", "APP_LOG_LEVEL"})
	require.Empty(missing, "should not report missing keys")

	e.Init(envItems)
	// test failing requirement checking - undefined variable
	missing = e.CheckRequired([]string{"APP_HOME", "APP_ENV", "APP_LOG_LEVEL", "APP_PRETTY_LOGS"})
	require.NotEmpty(missing, "should respond with unconfigured key")
	require.Len(missing, 1, "should report the expected number of missing keys")
	require.Contains(missing, "APP_PRETTY_LOGS", "should get expected missing key")

	// check adding variables
	e.Add([]string{"APP_PRETTY_LOGS", "APP_EMPTY", "APP_TEST_KEY"})
	require.NotEmpty(e.Get("APP_PRETTY_LOGS"), "APP_PRETTY_LOGS should be set")

	// test failing requirement checking - multiple unset variables
	missing = e.CheckRequired([]string{"APP_HOME", "APP_ENV", "APP_LOG_LEVEL", "APP_EMPTY", "APP_OTHER", "APP_TEST_KEY"})
	require.NotEmpty(missing, "should respond with unconfigured key")
	require.Len(missing, 2, "should report the expected number of missing keys")
	require.Contains(missing, "APP_EMPTY", "should get expected missing key")
	require.Contains(missing, "APP_OTHER", "should get expected missing key")

	// check updating variable
	os.Setenv("APP_TEST_KEY", "newvalue")
	e.Update([]string{"APP_TEST_KEY"})
	require.Equal("newvalue", e.Get("APP_TEST_KEY"), "should have new value")

	// check set var
	e.SetVars(map[string]string{"ALT_KEY": "something", "ALT_LOG": "warn"})
	require.Equal("something", e.Get("ALT_KEY"))

	e.Print()
}

func TestEnvDebug(t *testing.T) {
	require := require.New(t)

	os.Setenv("APP_LOG_LEVEL", "debug")
	os.Setenv("APP_API_KEY", "secretkey")
	os.Setenv("APP_DB_PASS", "dbpassword")

	envItems := []string{"APP_HOME", "APP_ENV", "APP_LOG_LEVEL", "APP_PRETTY_LOGS", "APP_API_KEY", "APP_DB_PASS"}
	// test NewEnv, should not error, should output vars in verbose mode
	// 'KEY' and 'PASS' var values shoulld not be output in plain text
	e := NewEnv(envItems)

	// check adding variables
	e.Add([]string{"APP_PRETTY_LOGS", "APP_EMPTY"})
	require.NotEmpty(e.Get("APP_PRETTY_LOGS"), "APP_PRETTY_LOGS should be set")

	e.Add([]string{"APP_TEST_KEY"})
	require.NotEmpty(e.Get("APP_TEST_KEY"), "APP_TEST_KEY should be set")

	// check updating variable
	os.Setenv("APP_TEST_KEY", "newvalue")
	e.Update([]string{"APP_TEST_KEY", "APP_LOG_LEVEL"})
	require.Equal("newvalue", e.Get("APP_TEST_KEY"), "should have new value")

	os.Setenv("APP_LOG_LEVEL", "info")
}

func TestGetMatch(t *testing.T) {
	require := require.New(t)

	envItems := []string{"APP_HOME", "APP_ENV", "APP_LOG_LEVEL", "APP_PRETTY_LOGS"}
	// test NewEnv
	e := NewEnv(envItems)

	m := e.GetMatch("LOG")
	require.EqualValues(map[string]string{"APP_LOG_LEVEL": "info", "APP_PRETTY_LOGS": "1"}, m, "should return the matching keys")
}
