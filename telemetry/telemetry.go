package telemetry

import (
	"context"
	"fmt"
	"io"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	"go.opentelemetry.io/otel/semconv/v1.7.0"
)

// package constants
const (
	StdOut = "stdout"
	Sumo   = "sumo"
	File   = "file"
	None   = "none"
)

// Config -
type Config struct {
	Mode       string
	Endpoint   string
	File       io.Writer
	Attributes map[string]string
}

// NewTracer -
func NewTracer(c Config) (*trace.TracerProvider, error) {
	var traceExporter trace.SpanExporter
	var err error

	switch c.Mode {
	case Sumo:
		traceExporter, err = SumoExporter(c.Endpoint)
	case StdOut:
		traceExporter, err = StdOutExporter()
	case File:
		traceExporter, err = FileExporter(c.File)
	case None:
		return nil, nil
	default:
		err = fmt.Errorf("unknown exporter type: %s", c.Mode)
	}
	if err != nil {
		return nil, err
	}

	batchSpanProcessor := trace.NewBatchSpanProcessor(traceExporter)
	tracerProvider := trace.NewTracerProvider(
		trace.WithSpanProcessor(batchSpanProcessor),
		trace.WithResource(newResource(c)),
	)

	otel.SetTracerProvider(tracerProvider)

	otel.SetTextMapPropagator(
		propagation.NewCompositeTextMapPropagator(
			propagation.TraceContext{},
			propagation.Baggage{},
		),
	)

	return tracerProvider, nil
}

// newResource returns a resource describing this application.
func newResource(c Config) *resource.Resource {
	attrs := []attribute.KeyValue{}
	for k, v := range c.Attributes {
		attrs = append(attrs, attribute.KeyValue{Key: attribute.Key(k), Value: attribute.StringValue(v)})
	}

	r, _ := resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(semconv.SchemaURL, attrs...),
	)
	return r
}

// SumoExporter -
func SumoExporter(endpoint string) (trace.SpanExporter, error) {
	ctx := context.Background()

	otlpOptions := []otlptracegrpc.Option{
		otlptracegrpc.WithInsecure(),
		otlptracegrpc.WithEndpoint(endpoint),
	}

	client := otlptracegrpc.NewClient(otlpOptions...)

	exporter, err := otlptrace.New(ctx, client)
	if err != nil {
		return nil, err
	}

	return exporter, nil
}

// StdOutExporter -
func StdOutExporter() (trace.SpanExporter, error) {
	exporter, err := stdouttrace.New(stdouttrace.WithPrettyPrint())
	if err != nil {
		return nil, err
	}
	return exporter, nil
}

// FileExporter returns a file exporter.
func FileExporter(w io.Writer) (trace.SpanExporter, error) {
	exporter, err := stdouttrace.New(
		stdouttrace.WithWriter(w),
		stdouttrace.WithPrettyPrint(), // Use human readable output.
	)
	if err != nil {
		return nil, err
	}

	return exporter, nil
}
