package telemetry

import (
	"net/http"

	"github.com/gorilla/mux"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"
)

// Middleware - wrapper to support urfave/negroni style middleware
type Middleware struct {
	muxMW mux.MiddlewareFunc
}

// NewMiddleware -
func NewMiddleware(name string, opts ...otelmux.Option) *Middleware {

	return &Middleware{
		muxMW: otelmux.Middleware(name, opts...),
	}
}

// ServeHTTP -
func (mw *Middleware) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	mw.muxMW.Middleware(next).ServeHTTP(rw, r)
}
