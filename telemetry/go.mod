module gitlab.com/msts-public/general/gomods/telemetry

go 1.16

replace (
	github.com/envoyproxy/go-control-plane => github.com/envoyproxy/go-control-plane v0.9.10-0.20210907150352-cf90f659a021
	github.com/golang/protobuf => github.com/golang/protobuf v1.5.2
	github.com/google/go-cmp => github.com/google/go-cmp v0.5.7
	go.opentelemetry.io/otel => go.opentelemetry.io/otel v1.4.1
	golang.org/x/crypto => golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	golang.org/x/net => golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2
	golang.org/x/sys => golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e
	golang.org/x/tools => golang.org/x/tools v0.1.7
	golang.org/x/xerrors => golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
	google.golang.org/genproto => google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013
	google.golang.org/grpc => google.golang.org/grpc v1.44.0
	google.golang.org/protobuf => google.golang.org/protobuf v1.27.1
)

require (
	github.com/gorilla/mux v1.8.0
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.0
	go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux v0.25.0
	go.opentelemetry.io/otel v1.4.1
	go.opentelemetry.io/otel/exporters/otlp/otlptrace v1.4.1
	go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc v1.4.1
	go.opentelemetry.io/otel/exporters/stdout/stdouttrace v1.4.1
	go.opentelemetry.io/otel/sdk v1.4.1
	go.opentelemetry.io/otel/trace v1.4.1
)
