package telemetry

import (
	"github.com/rs/zerolog"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

// TraceHook - hook to add error message to trace
// Note: when used in api middleware, the otelmux module will set the status based on the response code with
//       empty description, overwriting the status description set here
type TraceHook struct {
	Span trace.Span
}

// Run -
func (h TraceHook) Run(e *zerolog.Event, level zerolog.Level, msg string) {
	if level >= zerolog.ErrorLevel {
		h.Span.SetAttributes(attribute.KeyValue{Key: attribute.Key(level.String()), Value: attribute.StringValue(msg)})
		h.Span.SetStatus(codes.Error, msg)
	}
}
