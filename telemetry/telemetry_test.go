package telemetry

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
	"go.opentelemetry.io/otel"
)

func TestTelemetry(t *testing.T) {
	require := require.New(t)

	testCases := map[string]struct {
		mode string
		file string
	}{
		"none": {
			mode: None,
		},
		"stdout": {
			mode: StdOut,
		},
		"file": {
			mode: File,
			file: "test.trc",
		},
		"sumo": {
			mode: Sumo,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			var fh *os.File

			if tc.file != "" {
				fh, err := os.Create(tc.file)
				require.NoError(err, "failed creating file")
				defer fh.Close()
			}

			conf := Config{
				Mode:       tc.mode,
				File:       fh,
				Attributes: map[string]string{"test": "test value"},
			}

			tr, err := NewTracer(conf)
			require.NoError(err, "failed new tracer")
			if tc.mode != None {
				require.NotNil(tr, "tracer should not be nil")
			} else {
				require.Nil(tr, "tracer should be nil")
			}
		})
		if tc.file != "" {
			os.Remove(tc.file)
		}
	}
}

func TestLogHook(t *testing.T) {
	require := require.New(t)

	// divert stdout to variable
	old := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	outC := make(chan string)
	go func() {
		var buf bytes.Buffer
		io.Copy(&buf, r)
		outC <- buf.String()
	}()

	conf := Config{
		Mode: StdOut,
	}

	tr, err := NewTracer(conf)
	require.NoError(err, "failed new tracer")
	otel.SetTracerProvider(tr)
	defer tr.Shutdown(context.Background())

	ctx := context.Background()
	l := zerolog.New(os.Stdout).With().Timestamp().Logger()
	ctx = l.WithContext(ctx)

	_, span := otel.Tracer("test").Start(ctx, "testHook")
	th := TraceHook{Span: span}
	l = l.Hook(th)

	l.Error().Err(fmt.Errorf("test error")).Msg("something went wrong")

	span.End()

	w.Close()
	os.Stdout = old
	out := <-outC

	require.Contains(out, "something went wrong", "trace should have the logged error message")
}
