module gitlab.com/msts-public/general/gomods/alerter

go 1.16

replace (
	github.com/pkg/errors => github.com/pkg/errors v0.9.1
	github.com/stretchr/testify => github.com/stretchr/testify v1.7.0
	golang.org/x/crypto => golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	golang.org/x/net => golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2
	golang.org/x/sys => golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e
	golang.org/x/tools => golang.org/x/tools v0.1.7
	golang.org/x/xerrors => golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
)

require (
	github.com/opsgenie/opsgenie-go-sdk-v2 v1.2.8
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/msts-public/general/gomods/env v0.1.1
	gitlab.com/msts-public/general/gomods/logger v0.1.4
)
