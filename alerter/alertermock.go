package alerter

import (
	"github.com/rs/zerolog"
)

// MockAlerter -
type MockAlerter struct {
	*OpsGenieAlerter
	NumAlerts map[zerolog.Level]int
}

// NewMockAlerter -
func NewMockAlerter(c Config) (*MockAlerter, error) {
	oga, err := NewOpsGenieAlerter(c)
	if err != nil {
		return nil, err
	}

	oga.config.Enabled = true
	oga.config.Mocked = true

	ma := &MockAlerter{
		OpsGenieAlerter: oga,
		NumAlerts:       map[zerolog.Level]int{},
	}

	return ma, nil
}

// WriteLevel -
func (ma *MockAlerter) WriteLevel(level zerolog.Level, p []byte) (n int, err error) {
	// only do this in testing as it is not safe in concurrent processes, can cause "fatal error: concurrent map writes"
	if len(ma.config.Tags) > 0 {
		isTest := false
		for _, tag := range ma.config.Tags {
			if tag == "alertertest" {
				isTest = true
				break
			}
		}
		if isTest {
			if _, ok := ma.NumAlerts[level]; ok {
				ma.NumAlerts[level]++
			} else {
				ma.NumAlerts[level] = 1
			}
		}
	}

	if level < zerolog.ErrorLevel {
		return len(p), nil
	}

	return ma.Write(p)
}
