# alerter

Module for integrating with Opsgenie for alerting via zerolog logging

The alerter conforms to the zerolog.LevelWriter interface, so can be passed to zerologs function, e.g. MultiLevelWriter, to be a log message destination

The alerter only messages on Error or higher levels of logging.

The alerter converts the log message into an OpsGenie message and send to OpsGenie

### Setup

```golang
import "gitlab.com/msts-public/general/gomods/alerter"

func main() {
	config = alerter.Config{
		APIKey:      "123...40",
		Team:        "OpsGenieTeam",
		Environment: "development",
		Hostname:    os.Getenv("HOSTNAME"),
		Process:     "Test",
		Tags:        []string{"dev", "test"} ,
	}

	alert, err := alerter.NewAlerter(config)
}

```

### Config

```golang
type Config struct {
	APIKey      string             // OpsGenie API Key
	Team        string             // Team alerts are sent to in OpsGenie
	AppName     string             // Application name - identify where the alert came from
	Environment string             // Application environment - identify where the alert came from
	Hostname    string             // Application host - identify where the alert came from
	Process     string             // Application process - identify where the alert came from
	Enabled     bool               // Is alerting enambled
	Mocked      bool               // Is alerting mocked, ie alert is printed to StdErr not sent to OpsGenie
	Tags        []string           // Tags to add to all alerts
	VisibleTo   []alert.Responder  // Teams or users the alert is also visible to
}

```

### Usage

To use alerter as a destination in a zerolog multilevel logger, simply add it to the list of writers:

```golang
	multi := zerolog.MultilevelWriter(os.StdErr, alert)
	l := zerolog.New(multi).With().Timestamp().Logger()

```

### Supported contextual logging values to be included in alert details

- Str("priority", "P1") - sets the priority level, if not set or given unrecognised value, defaults to P1
- Str("package", "mypackage") - sets a 'package' value in alert Details
- Str("function", "myfunc") - sets a 'function' value in alert Details
- Err(err) - sets an 'error' value in the alert Details  
- Dict("vals", zerolog.Dict) - sets a 'vals' value in alert Details with a list of key-value pairs

```golang
	func myFunc(log zerolog.Logger) error {
		l := log.With().Str("function", "myFunc").Logger()
		...

		if err != nil {
			l.Error().Err(err).Str("priority","P3").Dict("vals", zerolog.Dict().Str("key1","val1")).Msg("Something went wrong")
		}
	}

```
### References:

- github.com/opsgenie/opsgenie-go-sdk-v2
- https://godoc.org/github.com/opsgenie/opsgenie-go-sdk-v2
- https://docs.opsgenie.com/docs/opsgenie-go-api-v2
- github.com/rs/zerolog
