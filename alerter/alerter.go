package alerter

import (
	"encoding/json"
	"fmt"

	"github.com/opsgenie/opsgenie-go-sdk-v2/alert"
	"github.com/opsgenie/opsgenie-go-sdk-v2/client"
	"github.com/rs/zerolog"
)

// Config -
type Config struct {
	APIKey      string
	Team        string
	AppName     string
	Environment string
	Hostname    string
	Process     string
	Enabled     bool
	Mocked      bool
	Tags        []string
	VisibleTo   []alert.Responder
}

// alert priorities map
var priority = map[string]alert.Priority{
	"P1": alert.P1,
	"P2": alert.P2,
	"P3": alert.P3,
	"P4": alert.P4,
	"P5": alert.P5,
}

// LogEvent - log event struct for sending to OpsGenie
type LogEvent struct {
	Timestamp string                 `json:"time,omitempty"`
	Msg       string                 `json:"message,omitempty"`
	Priority  string                 `json:"priority,omitempty"`
	Err       string                 `json:"error,omitempty"`
	Caller    string                 `json:"caller,omitempty"`
	Package   string                 `json:"package,omitempty"`
	Function  string                 `json:"function,omitempty"`
	Tags      []string               `json:"tags,omitempty"`
	Array     []map[string]string    `json:"array,omitempty"`
	Vals      map[string]interface{} `json:"vals,omitempty"`
}

// Alerter is an interface that defines an Alert sending mechanism
//  As Alerter is to be implemented as a writer for logger, it needs to have the Write and WriteLevel
//  functions defined.
type Alerter interface {
	IsEnabled() bool
	Write(p []byte) (n int, err error)
	WriteLevel(level zerolog.Level, p []byte) (n int, err error)
}

// NewAlerter -
func NewAlerter(c Config) (Alerter, error) {
	if c.Mocked {
		return NewMockAlerter(c)
	}
	return NewOpsGenieAlerter(c)
}

// OpsGenieAlerter is an implementation of the Alerter interface
// that uses OpsGenie to send alert.
type OpsGenieAlerter struct {
	config        Config
	teamRecipient []alert.Responder
	alertClient   *alert.Client
}

// NewOpsGenieAlerter creates a new OpsGenieAlerter, setting the API Key for
// the OpsGenie integration and other settings.
func NewOpsGenieAlerter(c Config) (*OpsGenieAlerter, error) {
	// Set the ApiKey
	clientConfig := &client.Config{
		ApiKey: c.APIKey,
	}

	alertclient, err := alert.NewClient(clientConfig)
	if err != nil {
		return nil, fmt.Errorf("failed new opsgenie alert client: %v", err)
	}

	teams := []alert.Responder{
		{Type: alert.TeamResponder, Name: c.Team},
	}

	oga := &OpsGenieAlerter{
		config:        c,
		teamRecipient: teams,
		alertClient:   alertclient,
	}

	// if hostname is not set, default to the provided process
	if oga.config.Hostname == "" {
		oga.config.Hostname = oga.config.Process
	}

	return oga, nil
}

func (a *OpsGenieAlerter) Write(p []byte) (n int, err error) {
	if !a.config.Enabled && !a.config.Mocked {
		return len(p), nil
	}

	logEvent := LogEvent{}
	if err := json.Unmarshal(p, &logEvent); err != nil {
		return len(p), err
	}

	// if unset, default priority to P1
	if logEvent.Priority == "" {
		logEvent.Priority = "P1"
	}

	_, err = a.Alert(logEvent)
	return len(p), err
}

// WriteLevel -
func (a *OpsGenieAlerter) WriteLevel(level zerolog.Level, p []byte) (n int, err error) {
	if level < zerolog.ErrorLevel {
		return len(p), nil
	}
	return a.Write(p)
}

// IsEnabled -
func (a *OpsGenieAlerter) IsEnabled() bool {
	return a.config.Enabled
}

// Alert sends an OpsGenie alert.
func (a *OpsGenieAlerter) Alert(message LogEvent) (*alert.CreateAlertRequest, error) {

	tags := a.config.Tags

	if message.Tags != nil {
		tags = append(tags, message.Tags...)
	}

	details := map[string]string{}
	if message.Timestamp != "" {
		details["timestamp"] = message.Timestamp
	}

	if message.Function != "" {
		details["function"] = message.Function
	}

	if message.Package != "" {
		details["package"] = message.Package
	}

	if message.Err != "" {
		details["error"] = message.Err
	}

	for k, v := range message.Vals {
		details[k] = fmt.Sprintf("%+v", v)
	}

	if len(message.Array) > 0 {
		for _, m := range message.Array {
			for k, v := range m {
				if _, ok := details[k]; ok {
					details[k] = details[k] + "\n" + v
				} else {
					details[k] = v
				}
			}
		}
	}

	request := &alert.CreateAlertRequest{
		Message:     fmt.Sprintf("%s - %s - %s", a.config.AppName, a.config.Environment, message.Msg),
		Description: message.Msg,
		Responders:  a.teamRecipient,
		VisibleTo:   a.config.VisibleTo,
		Source:      a.config.Hostname,
		Entity:      a.config.Process,
		Priority:    priority[message.Priority],
		Tags:        tags,
		Details:     details,
	}

	if a.config.Mocked {
		fmt.Printf("--- Alert: %+v ---\n", request)
		return request, nil
	}

	_, err := a.alertClient.Create(nil, request)
	return request, err
}
