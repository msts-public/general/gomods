// +build !test_integration

package alerter

import (
	"fmt"
	"testing"
	"time"

	"github.com/opsgenie/opsgenie-go-sdk-v2/alert"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"

	"gitlab.com/msts-public/general/gomods/env"
	"gitlab.com/msts-public/general/gomods/logger"
)

func TestNewAlerter(t *testing.T) {
	require := require.New(t)

	config := Config{
		APIKey:  "testOpsGenieAPIKey-itMustBe40charsLong..",
		Enabled: false,
		Process: "test",
	}
	a, err := NewAlerter(config)
	require.NoError(err)
	require.NotNil(a)
	require.IsType(&OpsGenieAlerter{}, a, "Type should be OpsGenieAlerter")

	config.Mocked = true
	a, err = NewAlerter(config)
	require.NoError(err)
	require.IsType(&MockAlerter{}, a, "Type should be MockAlerter")
}

func TestAlert(t *testing.T) {
	require := require.New(t)

	config := Config{
		APIKey:      "testOpsGenieAPIKey-itMustBe40charsLong..",
		Mocked:      true,
		Environment: "test",
		AppName:     "APP_NAME",
		Process:     "alertertest",
		Hostname:    "testhost",
	}
	config.Tags = []string{config.Environment, config.Process}

	alrtr, err := NewAlerter(config)
	require.NoError(err, "Should get new alerter")

	require.IsType(&MockAlerter{}, alrtr, "Type should be MockAlerter")

	o, ok := alrtr.(*MockAlerter)
	require.True(ok)

	require.NotNil(alrtr)
	require.NotNil(o.alertClient)
	require.NotNil(o.teamRecipient)

	now := time.Now().String()

	testCases := map[string]struct {
		event   LogEvent
		request *alert.CreateAlertRequest
	}{
		"basic log event": {
			event: LogEvent{
				Timestamp: now,
				Msg:       "test alerter message",
				Priority:  "P1",
				Err:       "test alerter error",
				Caller:    "test caller",
				Package:   "testpackage",
				Function:  "testfunction",
			},
			request: &alert.CreateAlertRequest{
				Message:     fmt.Sprintf("%s - %s - %s", config.AppName, config.Environment, "test alerter message"),
				Description: "test alerter message",
				Responders:  o.teamRecipient,
				VisibleTo:   o.config.VisibleTo,
				Entity:      config.Process,
				Source:      config.Hostname,
				Priority:    priority["P1"],
				Tags:        []string{config.Environment, config.Process}, // default tags
				Details: map[string]string{
					"timestamp": now,
					"package":   "testpackage",
					"function":  "testfunction",
					"error":     "test alerter error",
				},
			},
		},
		"basic log event has no timestamp": {
			event: LogEvent{
				Msg:      "test alerter message",
				Priority: "P1",
				Err:      "test alerter error",
				Caller:   "test caller",
				Package:  "testpackage",
				Function: "testfunction",
			},
			request: &alert.CreateAlertRequest{
				Message:     fmt.Sprintf("%s - %s - %s", config.AppName, config.Environment, "test alerter message"),
				Description: "test alerter message",
				Responders:  o.teamRecipient,
				VisibleTo:   o.config.VisibleTo,
				Entity:      config.Process,
				Source:      config.Hostname,
				Priority:    priority["P1"],
				Tags:        []string{config.Environment, config.Process},
				Details: map[string]string{
					"package":  "testpackage",
					"function": "testfunction",
					"error":    "test alerter error",
				},
			},
		},
		"basic log event has no package": {
			event: LogEvent{
				Timestamp: now,
				Msg:       "test alerter message",
				Priority:  "P1",
				Err:       "test alerter error",
				Caller:    "test caller",
				Function:  "testfunction",
			},
			request: &alert.CreateAlertRequest{
				Message:     fmt.Sprintf("%s - %s - %s", config.AppName, config.Environment, "test alerter message"),
				Description: "test alerter message",
				Responders:  o.teamRecipient,
				VisibleTo:   o.config.VisibleTo,
				Entity:      config.Process,
				Source:      config.Hostname,
				Priority:    priority["P1"],
				Tags:        []string{config.Environment, config.Process},
				Details: map[string]string{
					"timestamp": now,
					"function":  "testfunction",
					"error":     "test alerter error",
				},
			},
		},
		"basic log event has no function": {
			event: LogEvent{
				Timestamp: now,
				Msg:       "test alerter message",
				Priority:  "P1",
				Err:       "test alerter error",
				Caller:    "test caller",
				Package:   "testpackage",
			},
			request: &alert.CreateAlertRequest{
				Message:     fmt.Sprintf("%s - %s - %s", config.AppName, config.Environment, "test alerter message"),
				Description: "test alerter message",
				Responders:  o.teamRecipient,
				VisibleTo:   o.config.VisibleTo,
				Entity:      config.Process,
				Source:      config.Hostname,
				Priority:    priority["P1"],
				Tags:        []string{config.Environment, config.Process}, // default tags
				Details: map[string]string{
					"timestamp": now,
					"package":   "testpackage",
					"error":     "test alerter error",
				},
			},
		},
		"basic log event has no error": {
			event: LogEvent{
				Timestamp: now,
				Msg:       "test alerter message",
				Priority:  "P1",
				Caller:    "test caller",
				Package:   "testpackage",
				Function:  "testfunction",
			},
			request: &alert.CreateAlertRequest{
				Message:     fmt.Sprintf("%s - %s - %s", config.AppName, config.Environment, "test alerter message"),
				Description: "test alerter message",
				Responders:  o.teamRecipient,
				VisibleTo:   o.config.VisibleTo,
				Entity:      config.Process,
				Source:      config.Hostname,
				Priority:    priority["P1"],
				Tags:        []string{config.Environment, config.Process}, // default tags
				Details: map[string]string{
					"timestamp": now,
					"package":   "testpackage",
					"function":  "testfunction",
				},
			},
		},
		"log event is has vals": {
			event: LogEvent{
				Timestamp: now,
				Msg:       "test alerter message",
				Priority:  "P1",
				Err:       "test alerter error",
				Caller:    "test caller",
				Package:   "testpackage",
				Function:  "testfunction",
				Vals: map[string]interface{}{
					"k1": "v1",
					"k2": "v2",
				},
			},
			request: &alert.CreateAlertRequest{
				Message:     fmt.Sprintf("%s - %s - %s", config.AppName, config.Environment, "test alerter message"),
				Description: "test alerter message",
				Responders:  o.teamRecipient,
				VisibleTo:   o.config.VisibleTo,
				Entity:      config.Process,
				Source:      config.Hostname,
				Priority:    priority["P1"],
				Tags:        []string{config.Environment, config.Process}, // default tags
				Details: map[string]string{
					"timestamp": now,
					"package":   "testpackage",
					"function":  "testfunction",
					"error":     "test alerter error",
					"k1":        "v1",
					"k2":        "v2",
				},
			},
		},
		"log event has array of key/value pairs": {
			event: LogEvent{
				Timestamp: now,
				Msg:       "test alerter message",
				Priority:  "P1",
				Err:       "test alerter error",
				Caller:    "test caller",
				Package:   "testpackage",
				Function:  "testfunction",
				Array: []map[string]string{
					{"k1": "v1"},
					{"k2": "v2"},
				},
			},
			request: &alert.CreateAlertRequest{
				Message:     fmt.Sprintf("%s - %s - %s", config.AppName, config.Environment, "test alerter message"),
				Description: "test alerter message",
				Responders:  o.teamRecipient,
				VisibleTo:   o.config.VisibleTo,
				Entity:      config.Process,
				Source:      config.Hostname,
				Priority:    priority["P1"],
				Tags:        []string{config.Environment, config.Process},
				Details: map[string]string{
					"timestamp": now,
					"package":   "testpackage",
					"function":  "testfunction",
					"error":     "test alerter error",
					"k1":        "v1",
					"k2":        "v2",
				},
			},
		},
		"log event has extra tags": {
			event: LogEvent{
				Timestamp: now,
				Msg:       "test alerter message",
				Priority:  "P1",
				Err:       "test alerter error",
				Caller:    "test caller",
				Package:   "testpackage",
				Function:  "testfunction",
				Tags:      []string{"T1", "T2"},
			},
			request: &alert.CreateAlertRequest{
				Message:     fmt.Sprintf("%s - %s - %s", config.AppName, config.Environment, "test alerter message"),
				Description: "test alerter message",
				Responders:  o.teamRecipient,
				VisibleTo:   o.config.VisibleTo,
				Entity:      config.Process,
				Source:      config.Hostname,
				Priority:    priority["P1"],
				Tags:        []string{config.Environment, config.Process, "T1", "T2"},
				Details: map[string]string{
					"timestamp": now,
					"package":   "testpackage",
					"function":  "testfunction",
					"error":     "test alerter error",
				},
			},
		},
	}

	for tn, tc := range testCases {
		t.Logf("---- running: %s ----", tn)
		t.Run(tn, func(tt *testing.T) {
			request, err := o.Alert(tc.event)
			require.NoError(err)
			require.NotNil(request)
			require.Equal(tc.request, request, "alert request should match expected")
		})
	}

}

func TestLogAlerter(t *testing.T) {
	require := require.New(t)

	config := Config{
		APIKey:      "testOpsGenieAPIKey-itMustBe40charsLong..",
		Mocked:      true,
		Environment: "test",
		AppName:     "APP_NAME",
		Process:     "alertertest",
		Hostname:    "testhost",
	}
	config.Tags = []string{config.Environment, config.Process}

	alrtr, err := NewAlerter(config)
	require.NoError(err, "Should get new alerter")

	require.IsType(&MockAlerter{}, alrtr, "Type should be MockAlerter")

	e := env.NewEnv(nil)
	l := logger.NewMultiLogger(e.GetMatch("LOG"), alrtr)
	// Note: should run test with -v option to manually inspect output
	l.Error().Str("priority", "P1").Err(fmt.Errorf("test fail error")).Array("tags", zerolog.Arr().Str("tag1").Str("tag2")).Msg("high priority error test message")
	require.Equal(1, alrtr.(*MockAlerter).NumAlerts[zerolog.ErrorLevel], "should have been called once with error level")

	l.Error().Str("priority", "P3").Err(fmt.Errorf("test fail error")).Msg("medium priority error test message")
	require.Equal(2, alrtr.(*MockAlerter).NumAlerts[zerolog.ErrorLevel], "should have been called twice with error level")

	l.Error().Str("priority", "P5").Err(fmt.Errorf("test fail error")).Msg("low priority error test message")
	require.Equal(3, alrtr.(*MockAlerter).NumAlerts[zerolog.ErrorLevel], "should have been called thrice with error level")

	l.Warn().Str("priority", "P3").Err(fmt.Errorf("test warn error")).Msg("warn test message")
	require.Equal(3, alrtr.(*MockAlerter).NumAlerts[zerolog.ErrorLevel], "should have been called thrice with error level")
	require.Equal(1, alrtr.(*MockAlerter).NumAlerts[zerolog.WarnLevel], "should have been called once with warn level")

	l.Info().Msg("info test message")
	require.Equal(3, alrtr.(*MockAlerter).NumAlerts[zerolog.ErrorLevel], "should have been called thrice with error level")
	require.Equal(1, alrtr.(*MockAlerter).NumAlerts[zerolog.WarnLevel], "should have been called once with warn level")
	require.Equal(1, alrtr.(*MockAlerter).NumAlerts[zerolog.InfoLevel], "should have been called once with info level")

}
