module gitlab.com/msts-public/general/gomods/logger

go 1.16

replace (
	github.com/rs/zerolog => github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify => github.com/stretchr/testify v1.7.0
	golang.org/x/crypto => golang.org/x/crypto v0.0.0-20211215165025-cf75a172585e
	golang.org/x/net => golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d
	golang.org/x/sys => golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e
	golang.org/x/text => golang.org/x/text v0.3.6
	golang.org/x/tools => golang.org/x/tools v0.1.7
	golang.org/x/xerrors => golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
)

require (
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.0
)
