package logger

import (
	"io"
	"os"

	"github.com/rs/zerolog"
)

const (
	optionLevel  = "APP_LOG_LEVEL"
	optionFormat = "APP_PRETTY_LOGS"
	optionCaller = "APP_LOG_CALLER"
)

// Logger -
type Logger = zerolog.Logger

// Hook -
type Hook struct {
	PackageName string
}

// Run -
func (h Hook) Run(e *zerolog.Event, level zerolog.Level, msg string) {
	if level != zerolog.NoLevel {
		e.Str("package", h.PackageName)
	}
}

// NewLogger returns a logger
func NewLogger(opts map[string]string) Logger {

	InitLogger(opts)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()
	if val := opts[optionCaller]; val == "1" {
		l = l.With().Caller().Logger()
	}

	if val := opts[optionFormat]; val != "0" {
		l = l.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	}

	return l
}

// InitLogger initializes logger
func InitLogger(opts map[string]string) {

	// set default log level
	zeroLogLevel := zerolog.InfoLevel

	logLevel := opts[optionLevel]
	switch logLevel {
	case "debug":
		zeroLogLevel = zerolog.DebugLevel
	case "warn":
		zeroLogLevel = zerolog.WarnLevel
	case "error":
		zeroLogLevel = zerolog.ErrorLevel
	default:
		zeroLogLevel = zerolog.InfoLevel
	}

	zerolog.SetGlobalLevel(zeroLogLevel)
}

// NewMultiLogger - returns logger with multiple outputs
func NewMultiLogger(opts map[string]string, writers ...io.Writer) Logger {

	InitLogger(opts)

	if val := opts[optionFormat]; val != "0" {
		writers = append(writers, zerolog.ConsoleWriter{Out: os.Stdout})
	} else {
		writers = append(writers, os.Stderr)
	}

	multi := zerolog.MultiLevelWriter(writers...)
	l := zerolog.New(multi).With().Timestamp().Logger()

	if val := opts[optionCaller]; val == "1" {
		l = l.With().Caller().Logger()
	}

	return l
}
