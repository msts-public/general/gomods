# logger

Module with helper functions for setting up a zerolog logger

### Setup

```golang
import "gitlab.com/msts-public/general/gomods/logger"

func main() {
  l := logger.NewLogger(logOptions)

  l2 := logger.NewMultiLogger(logOptions, writer)
}

```

### Supported options

- optionLevel  = "APP_LOG_LEVEL" - set the default logging level
- optionFormat = "APP_PRETTY_LOGS" - output 'human friendly' logs
- optionCaller = "APP_LOG_CALLER" - include details of where the log event was called from

### References:

- github.com/rs/zerolog

