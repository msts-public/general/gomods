// +build !test_fixtures,!test_integration

package logger

import (
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
)

func TestLogger(t *testing.T) {

	// environment
	opts1 := map[string]string{optionLevel: "error"}
	opts2 := map[string]string{optionFormat: "1", optionCaller: "1", optionLevel: "warn"}
	opts3 := map[string]string{optionLevel: "debug", optionFormat: "0"}

	l := NewLogger(opts1)
	require.NotNil(t, l, "New Logger is not nil")

	l.Debug().Msg("Test debug")

	l = NewLogger(opts2)
	require.NotNil(t, l, "New Logger is not nil")

	tl := &testLogger{}

	// default multilogger
	ml := NewMultiLogger(nil, tl)
	require.NotNil(t, ml, "New MultiLogger is not nil")

	// multilogger with options
	mlo := NewMultiLogger(opts2, tl)
	require.NotNil(t, mlo, "New MultiLogger is not nil")

	mld := NewMultiLogger(opts3, tl)
	require.NotNil(t, mld, "New MultiLogger is not nil")
}

type testLogger struct{}

// Write -
func (tl *testLogger) Write(p []byte) (n int, err error) {
	return len(p), nil
}

// WriteLevel -
func (tl *testLogger) WriteLevel(level zerolog.Level, p []byte) (n int, err error) {
	return len(p), nil
}
