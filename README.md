# gomods

Go modules

Import as

```golang
import "gitlab.com/msts-enterprise/general/gomods/<module>"
```

For each module to be made available, a tag should be created on main branch for the module with SEM versioning in the form `module/v1.2.3`, e.g. `alerter/v0.1.0`. If there are multiple modules updated in a single merge, a tag should be created for each module. See below references for more details.

Current modules:

- alerter - log to opsgenie for raising alerts
- env - manage loading of environment variable
- logger - manage logging with zerolog logger
- repo - auto-generate basic CRUD statements for database tables
- schema - validate json schema
- telemetry - helper functions for integrating with OpenTelemetry

More details can be found in each module's README

### Development

Branch from `develop`, will be merged back to `develop` following review.

To release merge `develop` to `main` and add the relevant `module/v1.2.3` tag.

During development, the in-progress module can be temporarily referenced from a project with `replace` in the `go.mod` file. eg:

```
replace gitlab.com/msts-public/general/gomods/schema => ../gomods/schema
```

Do not commit this change in your project, instead commit an update to the new version number in `go.mod` and then run `go mod tidy` to update `go.sum`.

### References:

- https://blog.golang.org/publishing-go-modules
- https://golang.org/doc/modules/managing-source#multiple-module-source
