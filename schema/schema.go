package schema

import (
	"fmt"
	"net/url"

	"github.com/rs/zerolog"
	"github.com/xeipuuv/gojsonschema"
)

// package constants
const (
	packageName = "schema"

	ErrorCodeInvalidURI       = "invalid_schema_uri"
	ErrorCodeSchemaNotLoaded  = "schema_not_loaded"
	ErrorCodeInvalidSchemaDef = "invalid_schema_definition"
	ErrorCodeInvalidData      = "invalid_data"
)

// Config -
type Config struct {
	Logger    zerolog.Logger
	SchemaURI string
	Defs      []string
}

// Schema -
type Schema struct {
	config            Config
	logger            zerolog.Logger
	schemaDefinitions map[string]*gojsonschema.Schema
	activeSchema      string
}

// NewSchema - returns a new schema validator which loads and compiles the schema file supplied in the config
func NewSchema(c Config) (*Schema, error) {
	var err error
	l := c.Logger.With().Str("package", packageName).Logger()

	// get a new schema loader and set it up
	sl := gojsonschema.NewSchemaLoader()
	sl.Validate = true

	// parse the schema URI
	uri, err := url.Parse(c.SchemaURI)
	if err != nil {
		l.Warn().Err(err).Str("schemaURI", c.SchemaURI).Msg("Invalid schema URI")
		return nil, NewError(ErrorCodeInvalidURI, err)
	}

	// If scheme is missing in the schema URI, prepend with 'file:///'
	if uri.Scheme == "" {
		uri, err = url.Parse("file:///" + c.SchemaURI)
		if err != nil {
			l.Warn().Err(err).Str("schemaURI", c.SchemaURI).Msg("Invalid schema URI")
			return nil, NewError(ErrorCodeInvalidURI, err)
		}
	}

	file := uri.String()

	// no definition supplied, defaults to root
	if len(c.Defs) == 0 {
		c.Defs = append(c.Defs, "root")
	}

	// load and compile schema
	sds := map[string]*gojsonschema.Schema{}
	for _, def := range c.Defs {
		var src string

		// determine source
		if def == "root" {
			// schema-path/schema-file.schema.json
			src = file
		} else {
			// schema-path/schema-file.schema.json#/$def/object/sub-object
			src = fmt.Sprintf("%s#/$def/%s", file, def)
		}

		// get compiled schema
		loader := gojsonschema.NewReferenceLoader(src)
		sds[def], err = sl.Compile(loader)
		if err != nil {
			l.Warn().Err(err).Str("schema", file).Str("definition", def).Msg("Error compiling schema")
			return nil, NewError(ErrorCodeSchemaNotLoaded, err)
		}
	}

	s := &Schema{
		config:            c,
		logger:            l,
		schemaDefinitions: sds,
		activeSchema:      c.Defs[0], // defaults to the first definition supplied (root if not supplied)
	}

	return s, nil
}

// Init -
func (s *Schema) Init(l zerolog.Logger) error {

	// replace initial logger with runtime specific instance
	s.logger = l.With().Str("package", packageName).Logger()

	return nil
}

// Validate - validate data against the schema returning schema validation errors
func (s *Schema) Validate(data interface{}) error {

	l := s.logger.With().Str("function", "Validate").Logger()

	if s.activeSchema == "" {
		return NewError(ErrorCodeInvalidSchemaDef, fmt.Errorf("no active schema definition is set"))
	}

	var dataLoader gojsonschema.JSONLoader
	switch data.(type) {
	case nil:
		return NewValidationError(ErrorCodeInvalidData, "(root)", "nil", "data for validation cannot be nil", "$")
	case string:
		dataLoader = gojsonschema.NewStringLoader(data.(string))
	default:
		dataLoader = gojsonschema.NewGoLoader(data)
	}

	// validate data
	result, err := s.schemaDefinitions[s.activeSchema].Validate(dataLoader)
	if err != nil {
		l.Warn().Err(err).Interface("loader", dataLoader).Interface("result", result)
		// error occurs when data cannot be decoded (e.g invalid json structure), returning as schema validation error
		return NewValidationError(ErrorCodeInvalidData, "(root)", "", err.Error(), "$")
	}

	if result.Valid() {
		l.Debug().Interface("data", data).Msg("Validated JSON schema ok")
		return nil
	}

	return s.processResult(result)
}

// SetSchema - sets the active schema definition for validation if definitions are supplied in the config and is not root.
func (s *Schema) SetSchema(def string) error {
	if s.activeSchema == "root" {
		return nil
	}

	if _, ok := s.schemaDefinitions[def]; !ok {
		return NewError(ErrorCodeInvalidSchemaDef, fmt.Errorf("schema definition '%s' not found", def))
	}

	s.activeSchema = def
	return nil
}

// processResult - process validation errors filtering out non-user-friendly errors that refer to the conditionals in the
// schema that may not be understood by the end-users. These are usually accompanied by a more specific error. If the
// errors are all filtered ones, these errors will be returned.
func (s *Schema) processResult(result *gojsonschema.Result) error {

	l := s.logger.With().Str("function", "processResult").Logger()
	ec := NewValidationErrorCollection()
	ecSkipped := NewValidationErrorCollection()

	for _, err := range result.Errors() {
		errType := err.Type()
		switch errType {
		case "number_any_of", "number_one_of", "number_all_of", "number_not", "condition_then", "condition_else":
			l.Debug().Str("type", errType).Msg("skipping error type")
			ecSkipped.Add(NewSchemaJSONError(err))
			continue
		default:
			ec.Add(NewSchemaJSONError(err))
		}
	}

	l.Debug().Msgf("got %d processed errors", len(ec))

	if len(ec) == 0 {
		l.Debug().Msg("all errors were skipped, returning skipped errors")
		return ecSkipped
	}

	return ec
}
