module gitlab.com/msts-public/general/gomods/schema

go 1.16

replace (
	github.com/stretchr/testify => github.com/stretchr/testify v1.7.0
	golang.org/x/crypto => golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	golang.org/x/net => golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2
	golang.org/x/sys => golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e
	golang.org/x/tools => golang.org/x/tools v0.1.7
	golang.org/x/xerrors => golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
)

require (
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.0
	github.com/xeipuuv/gojsonschema v1.2.0
)
