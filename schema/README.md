# schema

Module for JSON Schema validation

### Setup

```golang
import (
    "fmt"
    "os"

    "github.com/rs/zerolog"

    "gitlab.com/msts-public/general/gomods/schema"
)

func main() {
    l := zerolog.New(os.Stdout).With().Timestamp().Logger()

    config := schema.Config{
        SchemaURI: "/full/path/test.schema.json",
        Logger:    l,
        Defs:      []string{"request","response"}
    }

    s, err := schema.NewSchema(config)
    if err != nil {
        fmt.Println(err)
        return
    }

    // Validates the data against the schema
    s.SetSchema("request")
    err = s.Validate(`{"first_name": "John"}`)
    if err != nil {
        fmt.Println(err)
    }
}
```

### Config

```golang
type Config struct {
	Logger    zerolog.Logger // Zero logger instance
	SchemaURI string         // URI of schema definition. If scheme is not specified, it will default to using 'file' scheme
	Defs      []string       // Definitions within the schema. If empty, will default to "root"
}
```

### References:

- github.com/rs/zerolog
- github.com/xeipuuv/gojsonschema