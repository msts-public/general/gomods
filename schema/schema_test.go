package schema

import (
	"fmt"
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
)

func TestSchema(t *testing.T) {

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()
	wd, _ := os.Getwd()
	schemaURI := fmt.Sprintf("%s/test.schema.json", wd)

	defConfig := Config{
		SchemaURI: schemaURI,
		Logger:    l,
	}

	testCase := map[string]struct {
		config       Config
		data         interface{}
		err          func() error
		setSchemaDef string
		setSchemaErr func() error
		schemaErr    func() error
	}{
		"Validation Success": {
			config: defConfig,
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
				"age": 20,
				"proof_of_age": {
					"id_type": "ssn",
					"id_number": "A8693170"
				}
			}
			`,
		},
		"Validation failed with reference schema": {
			config: defConfig,
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
				"age": 20,
				"proof_of_age": {
					"id_type": "ssn",
					"id_number": "A8693170"
				},
				"contacts": {
					"phone": 123456789,
					"email": ["john.doe@example.com"]
				}
			}
			`,
			err: func() error {
				vec := NewValidationErrorCollection()
				vec.Add(ValidationError{
					Property: "phone",
					Type:     "invalid_type",
					Detail:   "Invalid type. Expected: string, given: integer",
					Value:    "123456789",
					Path:     "$.contacts.phone",
				})
				return vec
			},
		},
		"Validation failed with definition": {
			config: Config{
				SchemaURI: schemaURI,
				Defs:      []string{"occupation/category"},
			},
			data: `
			{
				"industry": "invalid",
				"title": "Software Developer"
			}
			`,
			err: func() error {
				vec := NewValidationErrorCollection()
				vec.Add(ValidationError{
					Property: "industry",
					Type:     "invalid_value",
					Value:    "invalid",
					Detail:   "Must be one of the following: \"Banking\", \"Medical\", \"Construction\", \"IT\"",
					Path:     "$.industry",
				})
				return vec
			},
		},
		"Validation failed with set schema": {
			config: Config{
				SchemaURI: schemaURI,
				Defs:      []string{"person", "occupation/category"},
			},
			data: `
			{
				"industry": "invalid",
				"title": "Software Developer"
			}
			`,
			setSchemaDef: "occupation/category",
			err: func() error {
				vec := NewValidationErrorCollection()
				vec.Add(ValidationError{
					Property: "industry",
					Type:     "invalid_value",
					Value:    "invalid",
					Detail:   "Must be one of the following: \"Banking\", \"Medical\", \"Construction\", \"IT\"",
					Path:     "$.industry",
				})
				return vec
			},
		},
		"Validation failed with invalid data": {
			config: Config{
				SchemaURI: schemaURI,
			},
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
			`,
			err: func() error {
				return ValidationError{
					Property: "(root)",
					Type:     "invalid_data",
					Detail:   "unexpected EOF",
					Path:     "$",
				}
			},
		},
		"Validation failed due to unknown schema definition": {
			config: Config{
				SchemaURI: schemaURI,
				Defs:      []string{"person", "occupation/category"},
			},
			setSchemaDef: "nonexistent/definition",
			setSchemaErr: func() error {
				return NewError(ErrorCodeInvalidSchemaDef, fmt.Errorf("schema definition 'nonexistent/definition' not found"))
			},
		},
		"Validation failed with nil data": {
			config: Config{
				SchemaURI: schemaURI,
			},
			data: nil,
			err: func() error {
				return ValidationError{
					Property: "(root)",
					Type:     "invalid_data",
					Detail:   "data for validation cannot be nil",
					Value:    "nil",
					Path:     "$",
				}
			},
		},
		"Non existing schema definition": {
			config: Config{
				SchemaURI: schemaURI,
				Defs:      []string{"non-existing-def"},
			},
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
				"age": 20,
				"proof_of_age": {
					"id_type": "ssn",
					"id_number": "A8693170"
				}
			}
			`,
			schemaErr: func() error {
				return Error{
					"schema_not_loaded",
					"Object has no key 'non-existing-def'",
				}
			},
		},
		"Invalid schema URI": {
			config: Config{
				SchemaURI: "%",
			},
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
				"age": 20,
				"proof_of_age": {
					"id_type": "ssn",
					"id_number": "A8693170"
				}
			}
			`,
			schemaErr: func() error {
				return Error{
					"invalid_schema_uri",
					`parse "%": invalid URL escape "%"`,
				}
			},
		},
	}

	for tn, tc := range testCase {
		t.Run(tn, func(t *testing.T) {
			s, err := NewSchema(tc.config)
			if tc.schemaErr != nil {
				require.Equal(t, tc.schemaErr(), err, "error expected")
				return
			} else {
				require.NoError(t, err, "should not return an error")
			}

			// setSchemaDef supplied, test SetSchema
			if tc.setSchemaDef != "" {
				err = s.SetSchema(tc.setSchemaDef)
				if tc.setSchemaErr != nil {
					require.Equal(t, tc.setSchemaErr(), err, "error expected")
					return
				} else {
					require.NoError(t, err, "should not return an error")
				}
			}

			err = s.Validate(tc.data)
			if tc.err != nil {
				require.Equal(t, tc.err(), err, "error expected")
			} else {
				require.NoError(t, err, "should not return an error")
			}
		})
	}
}
