package schema

import (
	"fmt"
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
	"github.com/xeipuuv/gojsonschema"
)

func TestError(t *testing.T) {
	require := require.New(t)

	se := NewError("error_type", fmt.Errorf("error message"))
	require.Equal("error_type", se.Type, "type should be as expected")
	require.Equal("error message", se.Detail, "detail should be as expected")
	require.Equal("error message", se.Error(), "error should be as expected")
}

func TestValidationError(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()
	wd, _ := os.Getwd()
	schemaURI := fmt.Sprintf("%s/test.schema.json", wd)

	s, err := NewSchema(Config{
		SchemaURI: schemaURI,
		Logger:    l,
	})

	require.NoError(err, "should not return an error")

	testCase := map[string]struct {
		data          string
		validationErr ValidationError
	}{
		"Missing required field": {
			data: "{}",
			validationErr: ValidationError{
				Type:     "required",
				Property: "first_name",
				Value:    "",
				Detail:   "first_name is required",
				Path:     "$.first_name",
			},
		},
		"Field with invalid value - array_min_items": {
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
				"age": 20,
				"proof_of_age": {
					"id_type": "ssn",
					"id_number": "A8693170"
				},
				"parents": []
			}
					`,
			validationErr: ValidationError{
				Type:     "invalid_value",
				Property: "parents",
				Value:    "[]",
				Detail:   "Array must have at least 1 item",
				Path:     "$.parents",
			},
		},
		"Field with invalid type": {
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
				"age": "20",
				"proof_of_age": {
					"id_type": "ssn",
					"id_number": "A8693170"
				}
			}
					`,
			validationErr: ValidationError{
				Type:     "invalid_type",
				Property: "age",
				Value:    "20",
				Detail:   "Invalid type. Expected: integer, given: string",
				Path:     "$.age",
			},
		},
		"Field with invalid enum value": {
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
				"age": 20,
				"proof_of_age": {
					"id_type": "ssn",
					"id_number": "A8693170"
				},
				"title": "Engr"
			}
					`,
			validationErr: ValidationError{
				Type:     "invalid_value",
				Property: "title",
				Value:    "Engr",
				Detail:   `Must be one of the following: "Dr", "Prof", "Mr", "Mrs", "Ms"`,
				Path:     "$.title",
			},
		},
		"Field with validation error in array": {
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
				"age": 20,
				"proof_of_age": {
					"id_type": "ssn",
					"id_number": "A8693170"
				},
				"parents": [
					"24a78917-da7d-42f1-b1c8-8f9895f48e54",
					"invalid-uuid"
				]
			}
					`,
			validationErr: ValidationError{
				Type:     "invalid_value",
				Property: "parents[1]",
				Value:    "invalid-uuid",
				Detail:   "Does not match format 'uuid'",
				Path:     "$.parents[1]",
			},
		},
		"Field with validation error in sub property": {
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
				"age": 20,
				"proof_of_age": {
					"id_type": "ssn",
					"id_number": "A8693170"
				},
				"primary_contact": {
					"phone": "1"
				}
			}
					`,
			validationErr: ValidationError{
				Type:     "invalid_value",
				Property: "phone",
				Value:    "1",
				Detail:   "String length must be greater than or equal to 2",
				Path:     "$.primary_contact.phone",
			},
		},
		"Field with validation error in sub property array": {
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
				"age": 20,
				"proof_of_age": {
					"id_type": "ssn",
					"id_number": "A8693170"
				},
				"primary_contact": {
					"email": []
				}
			}
					`,
			validationErr: ValidationError{
				Type:     "invalid_value",
				Property: "email",
				Value:    "[]",
				Detail:   "Array must have at least 1 item",
				Path:     "$.primary_contact.email",
			},
		},
		"Field with validation error in sub property array with invalid value": {
			data: `
			{
				"first_name": "John",
				"last_name": "Doe",
				"age": 20,
				"proof_of_age": {
					"id_type": "ssn",
					"id_number": "A8693170"
				},
				"primary_contact": {
					"email": [
						"test@example.com",
						"notavalidemail"
					]
				}
			}
					`,
			validationErr: ValidationError{
				Type:     "invalid_value",
				Property: "email[1]",
				Value:    "notavalidemail",
				Detail:   "Does not match format 'email'",
				Path:     "$.primary_contact.email[1]",
			},
		},
	}

	for tn, tc := range testCase {
		t.Run(tn, func(t *testing.T) {
			loader := gojsonschema.NewStringLoader(tc.data)

			result, err := s.schemaDefinitions["root"].Validate(loader)
			require.NoError(err, "should not return an error")

			var validationErr gojsonschema.ResultError
			if len(result.Errors()) > 0 {
				validationErr = result.Errors()[0]
			}

			t.Logf("validation error: %s", validationErr)

			ve := NewSchemaJSONError(validationErr)
			require.Equal(tc.validationErr, ve, "validation error should be as expected")
		})
	}
}

func TestValidationErrorCollection(t *testing.T) {
	require := require.New(t)

	ve := NewValidationError("error_type", "field", "value", "error details", "$.field")
	vec := NewValidationErrorCollection()
	vec.Add(ve)

	require.Len(vec, 1, "collection length should be as expected")
	require.Equal(vec[0], ve, "collection item should be as expected")
	require.Equal(vec.Error(), "Errors:\n - field - error details\n", "error string should be as expected")
}
