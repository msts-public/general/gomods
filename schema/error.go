package schema

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/xeipuuv/gojsonschema"
)

var (
	reField = regexp.MustCompile(`(?m)^.+\.`)
	reArray = regexp.MustCompile(`(?m)\.(\d+)(\.)?`)
)

// Error - non-schema validation errors
type Error struct {
	Type   string
	Detail string
}

// NewError - general schema error
func NewError(typ string, err error) Error {
	return Error{
		Type:   typ,
		Detail: err.Error(),
	}
}

// Error - returns a formatted error string for the schema error
func (e Error) Error() string {
	return e.Detail
}

// ValidationError - schema validation errors
type ValidationError struct {
	Type     string
	Property string
	Value    string
	Detail   string
	Path     string
	err      gojsonschema.ResultError
}

// NewValidationError -
func NewValidationError(typ, property, value, detail, path string) ValidationError {
	return ValidationError{
		Type:     typ,
		Property: property,
		Value:    value,
		Detail:   detail,
		Path:     path,
	}
}

// NewSchemaJSONError - JSON schema validation errors
func NewSchemaJSONError(err gojsonschema.ResultError) ValidationError {
	ve := ValidationError{err: err}

	ve.setPath()
	ve.setField()
	ve.setType()
	ve.setDetail()
	ve.setValue()
	ve.err = nil

	return ve
}

// Error - returns a formatted error string for the schema validation error
func (ve ValidationError) Error() string {
	switch ve.Type {
	case "required":
		return fmt.Sprintf("'%s' is %s", ve.Property, ve.Type)

	case "invalid_value":
		return fmt.Sprintf("'%s' %s; got >%s<", ve.Property, ve.Detail, ve.Value)

	default:
		return fmt.Sprintf("%s - %s", ve.Property, ve.Detail)
	}
}

// setPath - sets the path of the validation error with the JSONPath of the relevant property where validation error occurred
func (ve *ValidationError) setPath() {
	var field string

	ve.Path = "$"

	switch ve.err.Type() {
	case "required":
		field = ve.err.Details()["property"].(string)
	default:
		field = ve.err.Field()
	}

	// not sure if it's possible for the field to be empty, but to be safe the path is set to "$"
	if field == "" {
		return
	}

	// reformat fields with array index and prefix with "$." (e.g contacts.0.type -> $.contacts[0].type, contacts.0 -> $.contacts[0])
	ve.Path = ve.Path + "." + reArray.ReplaceAllString(field, "[$1]$2")

}

// setField - sets the property of the validation error with the relevant property where validation error occurred
func (ve *ValidationError) setField() {
	ve.Property = reField.ReplaceAllString(ve.Path, "")
}

// setValue - sets the value of the validation error with the relevant data value where validation error occurred
func (ve *ValidationError) setValue() {
	switch ve.err.Type() {
	case "required":
		// set this to empty as returned value is that of the parent not the required field
		ve.Value = ""
	default:
		ve.Value = fmt.Sprintf("%v", ve.err.Value())
	}
}

// setType - sets the type of the validation error with the type of error
func (ve *ValidationError) setType() {
	switch ve.err.Type() {
	case "number_gte", "number_gt", "number_lte", "number_lt", "string_gte", "string_gt", "string_lte", "string_lt",
		"enum", "format", "pattern", "const", "array_min_items", "array_max_items":
		ve.Type = "invalid_value"
	default:
		ve.Type = ve.err.Type()
	}
}

// setDetail - sets the detail of the validation error with the reformatted errors returned from the validation
func (ve *ValidationError) setDetail() {
	ve.Detail = ""

	switch ve.err.Type() {
	case "number_gte", "number_gt", "number_lte", "number_lt", "format", "pattern", "array_min_items", "array_max_items":
		ve.Detail = ve.err.String()
		if strings.Contains(ve.Detail, " 1 items") {
			ve.Detail = strings.ReplaceAll(ve.Detail, " 1 items", " 1 item")
		}
	default:
		ve.Detail = ve.err.Description()
	}

	// clean up message a bit so we don't have property repeated
	if strings.Contains(ve.Detail, ve.err.Field()+": ") {
		ve.Detail = strings.ReplaceAll(ve.Detail, ve.err.Field()+": ", "")
	}
	if strings.Contains(ve.Detail, ve.err.Field()+" must") {
		ve.Detail = strings.ReplaceAll(ve.Detail, ve.err.Field()+" must", "Must")
	}
	if strings.Contains(ve.Detail, ve.err.Field()+" does") {
		ve.Detail = strings.ReplaceAll(ve.Detail, ve.err.Field()+" does", "Does")
	}
}

// ValidationErrorCollection - collection of ValidationError types
type ValidationErrorCollection []ValidationError

// NewValidationErrorCollection - returns a collection of ValidationError types
func NewValidationErrorCollection() ValidationErrorCollection {
	return ValidationErrorCollection{}
}

// Add - add error to schema validation error collection
func (vec *ValidationErrorCollection) Add(ve ValidationError) {
	*vec = append(*vec, ve)
}

// Error - returns a formatted error string from the schema validation error collection
func (vec ValidationErrorCollection) Error() string {
	b := strings.Builder{}

	for i, ve := range vec {
		if i == 0 {
			b.WriteString("Errors:\n")
		}
		b.WriteString(" - " + ve.Error() + "\n")
	}

	return b.String()
}
