# errors

Module for standardising error formats

- Defines a standard format for errors
- Defines a collection type for reporting multiple errors
- Defines methods for converting database and json schema errors to a standard format
- Defines methods to convert errors to an api response format


