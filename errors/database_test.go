package errors

import (
	"database/sql"
	"fmt"
	"testing"

	"github.com/lib/pq"
	"github.com/stretchr/testify/require"
)

type scanRow struct {
	Value string `db:"val"`
}

func makePqError(code pq.ErrorCode) error {
	err := pq.Error{
		Code:    code,
		Message: "an error message",
		Detail:  "error detail",
		Table:   "table_name",
		Column:  "column_name",
	}
	if code[0:2] == "23" && code != "23502" {
		err.Detail = "error detail: (column)=(12345)"
	}
	return &err
}

func TestDatabaseErrors(t *testing.T) {
	require := require.New(t)

	testCases := []struct {
		name  string
		pqErr error
		err   error
	}{
		{
			name:  "no rows",
			pqErr: sql.ErrNoRows,
			err:   ErrNoRows("test"),
		},
		{
			name:  "data excpetion",
			pqErr: makePqError("22000"),
			err:   makeError(TitleDatabaseInvalid, "data exception", "test", "", "", `an error message`, false),
		},
		{
			name:  "division by zero",
			pqErr: makePqError("22012"),
			err:   makeError(TitleDatabaseInvalid, "data exception", "test", "", "", `an error message`, false),
		},
		{
			name:  "integrity constraint",
			pqErr: makePqError("23000"),
			err:   makeError(TitleDatabaseInvalid, "constraint violation", "table_name", "column_name", "12345", `an error message`, false),
		},
		{
			name:  "unique constraint",
			pqErr: makePqError("23505"),
			err:   makeError(TitleDatabaseConflict, "constraint violation", "table_name", "column_name", "12345", `an error message`, false),
		},
		{
			name:  "not null",
			pqErr: makePqError("23502"),
			err:   makeError(TitleDatabaseInvalid, "constraint violation", "table_name", "column_name", ">null<", `an error message`, false),
		},
		{
			name:  "syntax error",
			pqErr: makePqError("42601"),
			err:   makeError(TitleDatabaseQuery, "syntax error", "test", "", "", `an error message`, false),
		},
		{
			name:  "access error",
			pqErr: makePqError("28P01"),
			err:   makeError(TitleDatabaseAccess, "authorizition exception", "database", "", "", `Access denied`, false),
		},
		{
			name:  "system error",
			pqErr: makePqError("58000"),
			err:   makeError(TitleDatabaseService, "system error", "database", "", "", `Unable to process request at this time`, true),
		},
		{
			name:  "unhandled code",
			pqErr: makePqError("XX000"),
			err:   makeError(TitleDatabaseService, "unhandled error", "test", "", "", `Unable to process request at this time`, true),
		},
		{
			name:  "struct scan error",
			pqErr: fmt.Errorf("could not find name TestColumn in map"),
			err:   makeError(TitleDatabaseInvalid, "missing parameter", "test", "TestColumn", "", "Invalid request - missing required parameter", false),
		},
		{
			name:  "non pqerror",
			pqErr: fmt.Errorf("not a database error"),
			err:   makeError(TitleDatabaseService, "unknown", "test", "", "", "Unable to process request at this time\nUnexpected error type: >not a database error<", false),
		},
	}

	for _, tc := range testCases {
		t.Logf("----- running: %s -----", tc.name)
		dberr := NewFromDatabaseError(tc.pqErr, "test")
		require.Equal(tc.err, dberr, "got expected error")
	}
}
