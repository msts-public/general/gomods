// +build !test_integration

package errors

func makeError(title, etype, context, property, value, detail string, system bool) Error {
	err := Error{
		Title:    title,
		Type:     etype,
		Context:  context,
		Property: property,
		Value:    value,
		Detail:   detail,
		Fatal:    false,
	}

	if system {
		err.systemError = true
	}

	return err
}
