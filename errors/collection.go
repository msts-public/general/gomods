package errors

import (
	"strings"
)

// Collection - holds a collection of errors
type Collection []error

// NewCollection returns a new Error Collection variable.
func NewCollection() Collection {
	var c Collection
	return c
}

// Error implements the error interface for Err.
func (c Collection) Error() string {
	b := strings.Builder{}

	for i, err := range c {
		if i == 0 {
			b.WriteString("Errors:\n")
		}
		b.WriteString(" - " + err.Error() + "\n")
	}

	return b.String()
}

// Add adds an Error or Collection to the Errors slice.
func (c *Collection) Add(err error) {
	switch err.(type) {
	case Error:
		*c = append(*c, err)
	case Collection:
		*c = append(*c, err.(Collection)...)
	default:
		*c = append(*c, NewError(TitleSystem, "legacy", "", "", err.Error(), nil))
	}
}

// AsResponseError - returns an error for an api response
func (c Collection) AsResponseError(titleCodeMap ErrorStatusCode) *ResponseError {
	re := &ResponseError{
		Errors: []Error{},
	}

	for i, e := range c {
		ee := e.(Error)

		if i == 0 {
			re.Code = titleCodeMap[ee.Title]
			re.Title = ee.Title
		}

		re.Errors = append(re.Errors, Error{
			Type:     ee.Type,
			Property: ee.Property,
			Context:  ee.Context,
			Value:    ee.Value,
			Detail:   ee.Detail,
		})
	}

	return re
}

// IsFatal -
func (c Collection) IsFatal() bool {
	if len(c) == 0 {
		return false
	}
	return c[0].(Error).Fatal
}
