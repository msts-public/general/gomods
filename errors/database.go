package errors

import (
	"database/sql"
	"fmt"
	"net"
	"net/http"
	"regexp"

	"github.com/lib/pq"
)

// Database errors -
const (
	TitleDatabaseAccess   string = "Access Denied"
	TitleDatabaseConflict string = "Conflict Error"
	TitleDatabaseInvalid  string = "Invalid Request"
	TitleDatabaseNoData   string = "Data Not Found"
	TitleDatabaseQuery    string = "Query Error"
	TitleDatabaseService  string = "Service Error"

	MessageNoRows string = "Query returned no records"
	NilRecord     string = "Nil record supplied"
)

// DBErrorCodeMap - Database error to response code mapping
var DBErrorCodeMap = ErrorStatusCode{
	TitleDatabaseInvalid: http.StatusBadRequest,
	TitleDatabaseQuery:   http.StatusBadRequest,
	TitleDatabaseNoData:  http.StatusNotFound,
	TitleDatabaseService: http.StatusServiceUnavailable,
	TitleDatabaseAccess:  http.StatusUnauthorized,
}

// NewDatabaseError -
func NewDatabaseError(errTitle, context, message string) Error {
	return Error{
		Title:   errTitle,
		Context: context,
		Detail:  message,
	}
}

// ErrNoRows -
func ErrNoRows(context string) Error {
	return Error{
		Title:   TitleDatabaseNoData,
		Context: context,
		Detail:  MessageNoRows,
	}
}

// IsErrNoRows -
func IsErrNoRows(err error) bool {
	if err == sql.ErrNoRows {
		return true
	}

	switch err.(type) {
	case Error:
		dbe := err.(Error)
		if dbe.Title == TitleDatabaseNoData {
			return true
		}
	case *pq.Error:
		pqe := err.(*pq.Error)
		if pqe.Code == "02000" {
			return true
		}
	}
	return false
}

var constraintRE = regexp.MustCompile(`\((.+)\)=\((.+)\)`)

// ErrConstraintViolation -
func ErrConstraintViolation(pqerr *pq.Error) Error {
	de := Error{
		Title:    TitleDatabaseInvalid,
		Type:     "constraint violation",
		Detail:   pqerr.Message,
		Context:  pqerr.Table,
		Property: pqerr.Column,
	}

	if pqerr.Code == "23505" {
		de.Title = TitleDatabaseConflict
	}
	if pqerr.Code == "23502" {
		de.Value = ">null<"
	}

	match := constraintRE.FindAllStringSubmatch(pqerr.Detail, 1)

	if len(match) > 0 {
		if de.Property == "" && len(match[0]) > 1 {
			de.Property = match[0][1]
		}
		if len(match[0]) > 2 {
			de.Value = match[0][2]
		}
	}

	return de
}

var missingRE = regexp.MustCompile(`^could not find name (\w+) in map`)
var scanRE = regexp.MustCompile(`^missing destination name (\w+) in`)

// NewFromDatabaseError - convert a pq.Error to a Error
func NewFromDatabaseError(err error, defaultContext string) Error {

	dberr := Error{Context: defaultContext}

	if err == sql.ErrNoRows {
		return ErrNoRows(defaultContext)
	}

	switch et := err.(type) {
	case *net.OpError:
		dberr.Title = TitleDatabaseService
		dberr.Property = "database"
		dberr.Detail = "Connection failed"
		dberr.systemError = true
	case *pq.Error:
		if pqerr, ok := err.(*pq.Error); ok {
			dberr.Detail = pqerr.Message + "\n" + pqerr.Detail

			switch pqerr.Code.Class() {
			case "22": // data exception
				dberr.Title = TitleDatabaseInvalid
				dberr.Type = "data exception"
				dberr.Detail = pqerr.Message
			case "23": // integrity exception
				dberr = ErrConstraintViolation(pqerr)
			case "25", "42": // transaction exception, syntax error
				dberr.Title = TitleDatabaseQuery
				dberr.Detail = pqerr.Message
				if pqerr.Code.Class() == "42" {
					dberr.Type = "syntax error"
				} else {
					dberr.Type = "data exception"
				}
			case "28": // authorizition exception
				dberr.Title = TitleDatabaseAccess
				dberr.Type = "authorizition exception"
				dberr.Detail = "Access denied"
				dberr.Context = "database"
			case "53", "54", "58": // resource, limit, and system errors - should probably raise an alert if these ever occur
				dberr.Title = TitleDatabaseService
				dberr.Type = "system error"
				dberr.Detail = "Unable to process request at this time"
				dberr.Context = "database"
				dberr.systemError = true
			default:
				fmt.Printf("--- unhandled database code: %s", pqerr.Code)
				dberr.Title = TitleDatabaseService
				dberr.Type = "unhandled error"
				dberr.Detail = "Unable to process request at this time"
				dberr.systemError = true
			}
		} else {
			fmt.Printf("--- shouldn't get here")
			dberr.Title = TitleDatabaseService
			dberr.Detail = "Unable to process request at this time\n" + err.Error()
			dberr.systemError = true
		}

	default:
		// TODO: add handling for other possible request and struct scan errors
		// handle missing parameter error - expect format 'cound not find name xxx in map...'
		if missingRE.MatchString(et.Error()) {
			fmt.Printf("--- matches missing parameter error: %s\n", et)
			dberr.Title = TitleDatabaseInvalid
			dberr.Type = "missing parameter"
			dberr.Property = missingRE.FindStringSubmatch(et.Error())[1]
			dberr.Detail = fmt.Sprintf("Invalid request - missing required parameter")

		} else if scanRE.MatchString(et.Error()) {
			fmt.Printf("--- matches struct scan error: %s\n", et)
			dberr.Title = TitleSystem
			dberr.Type = "struct scan error"
			dberr.Property = scanRE.FindStringSubmatch(et.Error())[1]
			dberr.Detail = fmt.Sprintf(et.Error())

		} else {
			fmt.Printf("--- not a pqerror or struct scan error: %s\n", et)
			dberr.Title = TitleDatabaseService
			dberr.Type = "unknown"
			dberr.Detail = fmt.Sprintf("Unable to process request at this time\nUnexpected error type: >%s<", et)
		}
	}

	return dberr
}
