module gitlab.com/msts-public/general/gomods/errors

go 1.16

require (
	github.com/lib/pq v1.10.2
	github.com/stretchr/testify v1.7.0
	github.com/xeipuuv/gojsonschema v1.2.0
)
