package errors

import (
	"fmt"
	"strings"

	"github.com/xeipuuv/gojsonschema"
)

// NewFromJSONSchemaError -
func NewFromJSONSchemaError(err gojsonschema.ResultError, context string) Error {
	return Error{
		Title:    SchemaValidation,
		Type:     getType(err),
		Property: getField(err),
		Context:  context,
		Value:    getValue(err),
		Detail:   getDetail(err),
	}
}

// InvalidSchema -
func InvalidSchema(err error, context string) error {
	return Error{
		Title:    SchemaValidation,
		Type:     "invalid_schema",
		Context:  context,
		Property: "(root)",
		Detail:   err.Error(),
	}
}

func getField(err gojsonschema.ResultError) string {
	switch err.Type() {
	case "required":
		return err.Details()["property"].(string)
	default:
		return err.Field()
	}
}

func getValue(err gojsonschema.ResultError) string {
	switch err.Type() {
	case "required":
		// set this to empty as returned value is that of the parent not the required field
		return ""
	default:
		return fmt.Sprintf("%v", err.Value())
	}
}

func getType(err gojsonschema.ResultError) string {
	switch err.Type() {
	case "number_gte", "number_gt", "number_lte", "number_lt", "string_gte", "string_gt", "string_lte", "string_lt",
		"enum", "format", "pattern", "const":
		return "invalid_value"
	case "array_min_items", "array_max_items":
		return "required"
	default:
		return err.Type()
	}
}

func getDetail(err gojsonschema.ResultError) string {
	detail := ""

	switch err.Type() {
	case "number_gte", "number_gt", "number_lte", "number_lt", "format", "pattern", "array_min_items", "array_max_items":
		detail = err.String()
		if strings.Contains(detail, " 1 items") {
			detail = strings.ReplaceAll(detail, " 1 items", " 1 item")
		}
	default:
		detail = err.Description()
	}

	// clean up message a bit so we don't have property repeated
	if strings.Contains(detail, err.Field()+": ") {
		detail = strings.ReplaceAll(detail, err.Field()+": ", "")
	}
	if strings.Contains(detail, err.Field()+" must") {
		detail = strings.ReplaceAll(detail, err.Field()+" must", "Must")
	}
	if strings.Contains(detail, err.Field()+" does") {
		detail = strings.ReplaceAll(detail, err.Field()+" does", "Does")
	}

	return detail
}
