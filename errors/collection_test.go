// +build !test_integration

package errors

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCollection(t *testing.T) {
	require := require.New(t)

	c1 := NewCollection()
	c2 := NewCollection()

	c1.Add(makeError(TitleBadRequest, "test", "mock", "add", "test add first error", "1", false))
	require.Len(c1, 1)
	require.Equal("Errors:\n - Bad Request: mock.add - 1 >\"test add first error\"<\n", c1.Error())

	c1.Add(makeError(TitleBadRequest, "test", "mock", "add", "test add second error", "2", false))
	require.Len(c1, 2)
	require.Equal("Errors:\n - Bad Request: mock.add - 1 >\"test add first error\"<\n - Bad Request: mock.add - 2 >\"test add second error\"<\n", c1.Error())

	c2.Add(makeError(TitleBadRequest, "test", "mock", "add", "test add third error", "3", false))
	require.Len(c2, 1)
	require.Equal("Errors:\n - Bad Request: mock.add - 3 >\"test add third error\"<\n", c2.Error())

	c2.Add(c1)
	require.Len(c2, 3)
	require.Equal("Errors:\n"+
		" - Bad Request: mock.add - 3 >\"test add third error\"<\n"+
		" - Bad Request: mock.add - 1 >\"test add first error\"<\n"+
		" - Bad Request: mock.add - 2 >\"test add second error\"<\n", c2.Error())
}
