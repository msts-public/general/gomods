package errors

import (
	"encoding/json"
	"net/http"
)

// package constants
const (
	SchemaValidation = "Schema Validation Error"
)

// JSONErrorCodeMap - JSON error to response code mapping
var JSONErrorCodeMap = ErrorStatusCode{
	TitleBadRequest:  http.StatusBadRequest,
	SchemaValidation: http.StatusBadRequest,
}

// NewFromJSONError -
func NewFromJSONError(err error, context string) Error {
	ne := Error{
		Title:   TitleBadRequest,
		Context: context,
	}

	switch err.(type) {
	case *json.InvalidUnmarshalError:
		je := err.(*json.InvalidUnmarshalError)
		ne.Type = "invalid_target"
		ne.Property = je.Type.Name()
		ne.Detail = je.Error()
	case *json.MarshalerError:
		// here for completeness - can be returned from custom MarshalJSON and MarshalText functions
		//  not currently using any custom functions
		je := err.(*json.MarshalerError)
		ne.Property = je.Type.Name()
		ne.Detail = je.Err.Error()
	case *json.SyntaxError:
		je := err.(*json.SyntaxError)
		ne.Type = "syntax_error"
		ne.Detail = je.Error()
	case *json.UnmarshalTypeError:
		je := err.(*json.UnmarshalTypeError)
		ne.Type = "type_error"
		ne.Property = je.Field
		ne.Value = je.Value
		ne.Detail = je.Error()
	case *json.UnsupportedTypeError:
		je := err.(*json.UnsupportedTypeError)
		ne.Type = "type_error"
		ne.Property = je.Type.Name()
		ne.Value = je.Type.String()
		ne.Detail = je.Error()
	case *json.UnsupportedValueError:
		// here for completeness - not sure what could return this
		je := err.(*json.UnsupportedValueError)
		ne.Type = "value_error"
		ne.Value = je.Str
		ne.Detail = je.Error()
	default:
		ne.Detail = err.Error()
	}

	return ne
}
