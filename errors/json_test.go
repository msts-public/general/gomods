package errors

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
)

type testJSON struct {
	Number int         `json:"number"`
	Text   string      `json:"text"`
	Other  interface{} `json:"other"`
}

func TestJSONErrors(t *testing.T) {
	require := require.New(t)

	testCases := []struct {
		name   string
		dataB  []byte
		dataJ  *testJSON
		badArg bool
		err    error
	}{
		{
			name:  "test marshal no error",
			dataJ: &testJSON{Number: 123, Text: "abc", Other: "zyx"},
		},
		{
			name:  "test unmarshal no error",
			dataB: []byte(`{"number": 123, "text": "abc", "other": "2020-01-01T13:14:15Z"}`),
		},
		{
			name:  "test unmarshal syntax error",
			dataB: []byte(`{"number": 12a, "text": "abc"}`),
			err:   makeError(TitleBadRequest, "syntax_error", "test", "", "", `invalid character 'a' after object key:value pair`, false),
		},
		{
			name:  "test unmarshal syntax error - end quote",
			dataB: []byte(`{"number": 12, "text": "abc}`),
			err:   makeError(TitleBadRequest, "syntax_error", "test", "", "", `unexpected end of JSON input`, false),
		},
		{
			name:  "test unmarshal syntax error - start quote",
			dataB: []byte(`{"number": 12, "text": bc"}`),
			err:   makeError(TitleBadRequest, "syntax_error", "test", "", "", `invalid character 'b' looking for beginning of value`, false),
		},
		{
			name:  "test unmarshal type error int",
			dataB: []byte(`{"number": "12a", "text": "abc"}`),
			err:   makeError(TitleBadRequest, "type_error", "test", "number", "string", `json: cannot unmarshal string into Go struct field testJSON.number of type int`, false),
		},
		{
			name:  "test unmarshal type error string",
			dataB: []byte(`{"number": 12, "text": 123}`),
			err:   makeError(TitleBadRequest, "type_error", "test", "text", "number", `json: cannot unmarshal number into Go struct field testJSON.text of type string`, false),
		},
		{
			name:   "test unmarshal type error string",
			dataB:  []byte(`{"number": 123, "text": "abc"}`),
			badArg: true,
			err:    makeError(TitleBadRequest, "invalid_target", "test", "testJSON", "", `json: Unmarshal(non-pointer errors.testJSON)`, false),
		},
		{
			name:  "test marshal unsupported type error",
			dataJ: &testJSON{Number: 123, Text: "abc", Other: func() {}},
			err:   makeError(TitleBadRequest, "type_error", "test", "", "func()", `json: unsupported type: func()`, false),
		},
	}

	for _, tc := range testCases {
		var err error

		if tc.dataJ == nil {
			if tc.badArg {
				// deliberate error, ignore warning about non-pointer passed to Unmarshal()
				tj := testJSON{}
				err = json.Unmarshal(tc.dataB, tj)
			} else {
				tj := &testJSON{}
				err = json.Unmarshal(tc.dataB, tj)
			}
			if err != nil {
				err = NewFromJSONError(err, "test")
			}
		} else {
			_, err = json.Marshal(tc.dataJ)
			if err != nil {
				err = NewFromJSONError(err, "test")
			}
		}

		if tc.err == nil {
			require.NoError(err)
		} else {
			require.Error(err)
			require.Equal(tc.err, err)
		}
	}
}
