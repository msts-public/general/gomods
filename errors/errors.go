package errors

import (
	"fmt"
)

// Error - Intended to be end user friendly error text messages, used in APIs etc
type Error struct {
	Title    string `json:"-"`
	Type     string `json:"type,omitempty"`
	Property string `json:"property,omitempty"`
	Value    string `json:"value,omitempty"`
	Context  string `json:"context,omitempty"`
	Detail   string `json:"detail"`
	// Fatal - Internal flag to indicate all processing should stop when other errors may continue
	Fatal       bool `json:"-"` // default false
	systemError bool
}

// Error -
func (e Error) Error() string {
	switch e.Title {
	case SchemaValidation:
		switch e.Type {
		case "required":
			return fmt.Sprintf("%s: '%s' is %s", e.Title, e.Property, e.Type)

		case "number_gte", "number_gt", "number_lte", "number_lt":
			return fmt.Sprintf("%s: '%s' %s; got >%s<", e.Title, e.Property, e.Detail, e.Value)

		default:
			return fmt.Sprintf("%s: %s - %s", e.Title, e.Property, e.Detail)
		}
	default:
		if e.Context == "" {
			return fmt.Sprintf("%s: %s", e.Title, e.Detail)
		} else if e.Property == "" {
			return fmt.Sprintf("%s: %s - %s", e.Title, e.Context, e.Detail)
		}
		if e.Value != "" {
			return fmt.Sprintf("%s: %s.%s - %s >%q<", e.Title, e.Context, e.Property, e.Detail, e.Value)
		}
		return fmt.Sprintf("%s: %s.%s - %s", e.Title, e.Context, e.Property, e.Detail)
	}
}

// NewError -
func NewError(errTitle, errType, errContext, errProperty, errDetail string, errValue interface{}) Error {
	me := Error{
		Title:    errTitle,
		Type:     errType,
		Context:  errContext,
		Property: errProperty,
		Detail:   errDetail,
	}

	if errValue != nil {
		me.Value = fmt.Sprintf("%+v", errValue)
	}

	return me
}

// NewConflictError -
func NewConflictError(errContext, errProperty, errDetail string, errValue interface{}) Error {
	return NewError(TitleConflict, "", errContext, errProperty, errDetail, errValue)
}

// NewDataConflictError -
func NewDataConflictError(errContext, errProperty, errDetail string, errValue interface{}) Error {
	return NewError(TitleDatabaseConflict, "", errContext, errProperty, errDetail, errValue)
}

// NewNoDataError -
func NewNoDataError(errContext, errProperty, errDetail string, errValue interface{}) Error {
	return NewError(TitleDatabaseNoData, "", errContext, errProperty, errDetail, errValue)
}

// NewSchemaError -
func NewSchemaError(errType, errContext, errProperty, errDetail string, errValue interface{}) Error {
	return NewError(SchemaValidation, errType, errContext, errProperty, errDetail, errValue)
}

// NewSystemError -
func NewSystemError(errContext, errProperty, errDetail string, errValue interface{}) Error {
	e := NewError(TitleSystem, "", errContext, errProperty, errDetail, errValue)
	e.systemError = true
	return e
}

// SetFatal -
func (e *Error) SetFatal() {
	e.Fatal = true
}

// IsFatal -
func (e Error) IsFatal() bool {
	return e.Fatal
}

// IsSystemError -
func (e Error) IsSystemError() bool {
	return e.systemError
}

// GetErrorTitle -
func GetErrorTitle(err error) string {
	if err == nil {
		return ""
	}

	switch et := err.(type) {
	case Error:
		return et.Title
	case Collection:
		if len(et) == 0 {
			return ""
		}
		return et[0].(Error).Title
	default:
		return err.Error()
	}
}

// Unauthorized -
func Unauthorized(context string) Error {
	return Error{
		Title:   TitleUnauthorized,
		Context: context,
		Detail:  "Access denied",
	}
}

// UnauthorizedWithDetail -
func UnauthorizedWithDetail(context, property, detail string) Error {
	return Error{
		Title:    TitleUnauthorized,
		Context:  context,
		Property: property,
		Detail:   detail,
	}
}

// Forbidden -
func Forbidden(context string) Error {
	return Error{
		Title:   TitleForbidden,
		Context: context,
		Detail:  "Access denied",
	}
}

// NotFound -
func NotFound(context string) Error {
	return Error{
		Title:   TitleNotFound,
		Context: context,
		Detail:  "Resource not found",
	}
}

// MethodNotAllowed -
func MethodNotAllowed(context, method string) Error {
	return Error{
		Title:   TitleMethodNotAllowed,
		Context: context,
		Detail:  TitleMethodNotAllowed,
		Value:   method,
	}
}

// MethodNotAllowedReason -
func MethodNotAllowedReason(context, method, detail string) Error {
	return Error{
		Title:   TitleMethodNotAllowed,
		Context: context,
		Detail:  detail,
		Value:   method,
	}
}

// InternalServerError -
func InternalServerError(context string) Error {
	return Error{
		Title:   TitleInternalServerError,
		Context: context,
		Detail:  "Server Error",
	}
}

// ServiceUnavailableError -
func ServiceUnavailableError(context string) Error {
	return Error{
		Title:   TitleServiceUnavailable,
		Context: context,
		Detail:  "Service Temporarily Unavailable",
	}
}

// MissingContext -
func MissingContext(context string) Error {
	return Error{
		Title:    TitleInternalServerError,
		Context:  context,
		Property: context,
		Detail:   "context empty",
	}
}
