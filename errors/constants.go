package errors

import (
	"net/http"
)

// package constants - standard error responses
const (
	TitleBadRequest          = "Bad Request"
	TitleConflict            = "Conflict Error"
	TitleForbidden           = "Forbidden"
	TitleInternalServerError = "Internal Server Error"
	TitleMethodNotAllowed    = "Method Not Allowed"
	TitleNotFound            = "Not Found"
	TitleServiceUnavailable  = "Service Unavailable"
	TitleSystem              = "System Error"
	TitleUnauthorized        = "Unauthorized"
)

// StdErrorCodeMap - Standard error to response code mapping
var StdErrorCodeMap = ErrorStatusCode{
	TitleBadRequest:          http.StatusBadRequest,
	TitleConflict:            http.StatusConflict,
	TitleForbidden:           http.StatusForbidden,
	TitleInternalServerError: http.StatusInternalServerError,
	TitleMethodNotAllowed:    http.StatusMethodNotAllowed,
	TitleNotFound:            http.StatusNotFound,
	TitleServiceUnavailable:  http.StatusServiceUnavailable,
	TitleSystem:              http.StatusServiceUnavailable,
	TitleUnauthorized:        http.StatusUnauthorized,
}

// ErrorConstants -
type ErrorConstants map[string]string

// Add - add an error constant
func (ec ErrorConstants) Add(name, value string) {
	ec[name] = value
}

// Merge - merge a set of error constants
func (ec ErrorConstants) Merge(constants map[string]string) {
	for k, v := range constants {
		ec[k] = v
	}
}

// ErrorStatusCode - map an error constant to an error status code
type ErrorStatusCode map[string]int

// Add - add an error status code
func (esc ErrorStatusCode) Add(name string, value int) {
	esc[name] = value
}

// Merge - add a set of error status codes
func (esc ErrorStatusCode) Merge(constants map[string]int) {
	for k, v := range constants {
		esc[k] = v
	}
}
