package errors

import (
	"fmt"
	"net/http"
	"strings"
)

// ResponseError -
type ResponseError struct {
	Code   int     `json:"code"`
	Title  string  `json:"title"`
	Detail string  `json:"detail,omitempty"` // TODO: possibly remove - keeping for backward compatibility during transition
	Errors []Error `json:"errors,omitempty"`
}

// Response -
type Response struct {
	Error *ResponseError `json:"error"`
}

func (re ResponseError) Error() string {
	b := strings.Builder{}

	b.WriteString(fmt.Sprintf("%d - %s:\n", re.Code, re.Title))

	if len(re.Errors) == 0 {
		b.WriteString(fmt.Sprintf(" - %s", re.Detail))
	} else {
		for _, err := range re.Errors {
			b.WriteString(" - ")
			if err.Type != "" {
				b.WriteString(fmt.Sprintf("%s: ", err.Type))
			}
			if err.Property != "" {
				b.WriteString(fmt.Sprintf("%s ", err.Property))
			}
			if err.Value != "" {
				b.WriteString(fmt.Sprintf("(%s) ", err.Value))
			}
			b.WriteString(fmt.Sprintf(" => %s", err.Detail))
		}
	}

	return b.String()
}

// AsResponseError -
func (e Error) AsResponseError(titleCodeMap ErrorStatusCode) *ResponseError {
	var code int
	if _, ok := titleCodeMap[e.Title]; ok {
		code = titleCodeMap[e.Title]
	} else {
		code = http.StatusServiceUnavailable
	}

	return &ResponseError{
		Code:  code,
		Title: e.Title,
		Errors: []Error{
			{
				Type:     e.Type,
				Property: e.Property,
				Context:  e.Context,
				Value:    e.Value,
				Detail:   e.Detail,
			},
		},
	}
}

// AsResponse -
func (e Error) AsResponse(titleCodeMap ErrorStatusCode) *Response {
	return &Response{
		Error: e.AsResponseError(titleCodeMap),
	}
}
