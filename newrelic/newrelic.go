package newrelic

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/gorilla/mux"
	"github.com/newrelic/go-agent/v3/integrations/nrgorilla"
	"github.com/newrelic/go-agent/v3/newrelic"
	"github.com/rs/zerolog"
)

// Config -
type Config struct {
	APIKey  string
	AppName string
	Enabled bool
	Debug   bool
}

// NewRelic -
type NewRelic struct {
	logger zerolog.Logger
	config Config
	app    *newrelic.Application
	txn    *newrelic.Transaction
}

// NewNewRelic -
func NewNewRelic(l zerolog.Logger, c Config) (*NewRelic, error) {

	nr := &NewRelic{
		logger: l.With().Str("package", "newrelic").Logger(),
		config: c,
	}

	err := nr.Init()
	if err != nil {
		l.Warn().Err(err).Msg("Failed init")
		return nr, err
	}

	return nr, nil
}

// Init -
func (nr *NewRelic) Init() error {

	// logger
	l := nr.logger.With().Str("function", "Init").Logger()

	// new relic application
	newRelicApp, err := newrelic.NewApplication(
		newrelic.ConfigAppName(nr.config.AppName),
		newrelic.ConfigLicense(nr.config.APIKey),
		newrelic.ConfigLogger(&logshim{l: nr.logger, debug: nr.config.Debug}),
		newrelic.ConfigEnabled(nr.config.Enabled),
	)
	if err != nil {
		l.Warn().Err(err).Msg("Failed new newrelic agent")
		return err
	}
	if err := newRelicApp.WaitForConnection(30 * time.Second); err != nil {
		l.Warn().Err(err).Msg("Failed connecting to newrelic")
		return err
	}

	nr.app = newRelicApp

	return nil
}

// StartTransaction begins a Transaction.
func (nr *NewRelic) StartTransaction(name string) *newrelic.Transaction {
	nr.txn = nr.app.StartTransaction(name)
	return nr.txn
}

// EndTransaction ends a started Transaction
func (nr *NewRelic) EndTransaction() {
	if nr.txn == nil {
		nr.logger.Warn().Msg("Cannot end transaction if no transaction started")
		return
	}
	nr.txn.End()
}

// AddAttribute to the started transaction
func (nr *NewRelic) AddAttribute(key string, value interface{}) {
	if nr.txn == nil {
		nr.logger.Warn().Msg("Cannot add attribute if no transaction started")
		return
	}
	nr.txn.AddAttribute(key, value)
}

// NoticeError records an error on the transaction
func (nr *NewRelic) NoticeError(err error) {
	if err == nil {
		return
	}
	if nr.txn == nil {
		nr.logger.Warn().Msg("Cannot notice error if no transaction started")
		return
	}
	nr.txn.NoticeError(err)
}

// InstrumentRoutes instruments requests through the provided mux.Router.
func (nr *NewRelic) InstrumentRoutes(r *mux.Router) *mux.Router {
	return nrgorilla.InstrumentRoutes(r, nr.app)
}

// Shutdown flushes data to New Relic's servers and stops all
// agent-related goroutines managing this application.
func (nr *NewRelic) Shutdown() {
	nr.app.Shutdown(10 * time.Second)
}

// This logging wrapping is based on the following:
// https://github.com/newrelic/go-agent/blob/master/internal/logger/logger.go
// https://github.com/newrelic/go-agent/blob/master/_integrations/nrlogxi/v1/nrlogxi.go

type logshim struct {
	l     zerolog.Logger
	debug bool
}

func (ls *logshim) Error(msg string, context map[string]interface{}) {
	ls.l.Warn().Msg(ls.fire("error", msg, context))
}
func (ls *logshim) Warn(msg string, context map[string]interface{}) {
	ls.l.Warn().Msg(ls.fire("warn", msg, context))
}
func (ls *logshim) Info(msg string, context map[string]interface{}) {
	ls.l.Info().Msg(ls.fire("info", msg, context))
}
func (ls *logshim) Debug(msg string, context map[string]interface{}) {
	if ls.debug {
		ls.l.Info().Msg(ls.fire("debug", msg, context))
	} else {
		ls.l.Debug().Msg(ls.fire("debug", msg, context))
	}
}
func (ls *logshim) DebugEnabled() bool {
	if ls.debug {
		return true
	}
	return ls.l.Debug().Enabled()
}

func (ls *logshim) fire(level, msg string, ctx map[string]interface{}) string {
	js, err := json.Marshal(struct {
		Level   string                 `json:"level"`
		Event   string                 `json:"msg"`
		Context map[string]interface{} `json:"context"`
	}{
		level,
		msg,
		ctx,
	})
	if err != nil {
		ls.l.Warn().Err(err).Msg("unable to marshal log entry")
		return fmt.Sprintf("unable to marshal log entry: %v", err)
	}
	return string(js)
}
