# newrelic

Module for integrating with NewRelic and Gorilla Mux

### Setup

```golang
import "gitlab.com/msts-public/general/gomods/newrelic"

func main() {
	config = newrelic.Config{
		APIKey:  "123...40",
		AppName: "MyApp",
		Enabled: true,
		Debug:   false,
	}

	nr, err := newrelic.NewNewRelic(config)
}

```

### Config

```golang
type Config struct {
	APIKey   string  // NewRelic API Key
	AppName  string  // Application name - identity to use for new relic stats
	Enabled  bool    // Is new relic integration enabled
	Debug    bool    // Is new relic debug logging enabled
}

```

### References:

- github.com/newrelic/go-agent
- https://godoc.org/github.com/newrelic/go-agent/v3/newrelic
- https://pkg.go.dev/github.com/newrelic/go-agent/v3/integrations/nrgorilla
