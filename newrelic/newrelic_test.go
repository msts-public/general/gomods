// +build !test_fixtures,!test_integration

package newrelic

import (
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
)

func TestNewRelic(t *testing.T) {

	// environment
	config := Config{
		APIKey:  "testApiKeyWhichMustBe40CharactersLong...",
		AppName: "test new relic",
		Debug:   true,
		Enabled: false,
	}

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	nr, err := NewNewRelic(l, config)
	require.NoError(t, err, "should not return an error")
	require.NotNil(t, nr, "should not be nil")
}
