module gitlab.com/msts-public/general/gomods/newrelic

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/newrelic/go-agent/v3 v3.14.0
	github.com/newrelic/go-agent/v3/integrations/nrgorilla v1.1.1
	github.com/rs/zerolog v1.25.0
	github.com/stretchr/testify v1.7.0
)
