// +build !test_integration

package repo

import (
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
)

func TestSQL(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	r := &Base{
		Logger:    l,
		TableName: "test_table",
	}

	err := r.Init(&TestTable{})
	require.NoError(err, "should not error")

	s1 := r.GetByIDSQL()
	require.NotEmpty(s1)
	s2 := r.GetByParamSQL()
	require.NotEmpty(s2)
	s3 := r.GetCreateSQL()
	require.NotEmpty(s3)
	s4 := r.GetUpdateSQL()
	require.NotEmpty(s4)
	s5 := r.GetDeleteSQL()
	require.NotEmpty(s5)
	s6 := r.GetRemoveSQL()
	require.NotEmpty(s6)
}
