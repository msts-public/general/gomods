package repo

// getByIDSQL is the standard fetch by primary key SQL template
var getByIDSQL = `
	SELECT %s
	FROM %q
	WHERE id = $1
	AND %s IS NULL
	%s
`

// getByAltIDSQL is the standard fetch by alternate unique key SQL template
var getByAltIDSQL = `
	SELECT %s
	FROM %q
	WHERE %s = $1
	AND %s IS NULL
	%s
`

// Deprecated: prefer use of getByParamSQL for this type of query
// getByIDsSQL is the standard SQL template for fetching a set of record by their primary keys
var getByIDsSQL = `
	SELECT %s
	FROM %q
	WHERE id IN (SELECT unnest($1::uuid[]))
	AND %s IS NULL
`

// getByParamSQL is the base SQL template for fetching by parameters, this forms the base for the query provided to and built up by sqlFromParamsAndOperator
var getByParamSQL = `
	SELECT %s
	FROM %q
	WHERE %s IS NULL
`

// createRecordSQL is the base SQL template for creating a record
var createRecordSQL = `
	INSERT INTO %q (%s)
	VALUES (%s)
	RETURNING %s
`

// updateRecordSQL is the base SQL template for updating a record
var updateRecordSQL = `
	UPDATE %q
	SET %s
	WHERE id = :id
	RETURNING %s
`

// deleteRecordSQL is the base SQL template for logically deleting a record
var deleteRecordSQL = `
	UPDATE %q
	SET %s = :%s
	WHERE id = :id
	AND %s IS NULL
`

// removeRecordSQL is the base SQL template for deleting a record
var removeRecordSQL = `
	DELETE FROM %q
	WHERE id = :id
`

// countSQL is the base SQL template for counting record. This is also passed to sqlFromParamsAndOperator for counting by params
var countSQL = `
	SELECT count(0) FROM %q
	WHERE %s IS NULL
`

// GetByIDSQL generates the SQL statement for fetching records by primary key
func (r *Base) GetByIDSQL() string {
	if r.VarGetByIDSQL == "" {
		r.VarGetByIDSQL = r.GenerateStatement(StatementSelectByID, r.ColumnMap)
	}
	return r.VarGetByIDSQL
}

// GetByAltIDSQL generates the SQL statement for fetching records by unique key
func (r *Base) GetByAltIDSQL() string {
	if r.VarGetByAltIDSQL == "" {
		r.VarGetByAltIDSQL = r.GenerateStatement(StatementSelectByAltID, r.ColumnMap)
	}
	return r.VarGetByAltIDSQL
}

// GetByParamSQL generates the base SQL statement for fetching records by provided parameters
func (r *Base) GetByParamSQL() string {
	if r.VarGetByParamSQL == "" {
		r.VarGetByParamSQL = r.GenerateStatement(StatementSelectByParam, r.ColumnMap)
	}
	return r.VarGetByParamSQL
}

// GetCreateSQL generates the SQL statement for creating a record
func (r *Base) GetCreateSQL() string {
	if r.VarCreateRecordSQL == "" {
		r.VarCreateRecordSQL = r.GenerateStatement(StatementCreate, r.ColumnMap)
	}
	return r.VarCreateRecordSQL
}

// GetUpdateSQL generates the SQL statement for updating a record
func (r *Base) GetUpdateSQL() string {
	if r.VarUpdateRecordSQL == "" {
		r.VarUpdateRecordSQL = r.GenerateStatement(StatementUpdate, r.ColumnMap)
	}
	return r.VarUpdateRecordSQL
}

// GetDeleteSQL generates the SQL statement for logically deleting a record
func (r *Base) GetDeleteSQL() string {
	if r.VarDeleteRecordSQL == "" {
		r.VarDeleteRecordSQL = r.GenerateStatement(StatementDelete, nil)
	}
	return r.VarDeleteRecordSQL
}

// GetRemoveSQL generates the SQL statement for deleting a record
func (r *Base) GetRemoveSQL() string {
	if r.VarRemoveRecordSQL == "" {
		r.VarRemoveRecordSQL = r.GenerateStatement(StatementRemove, nil)
	}
	return r.VarRemoveRecordSQL
}
