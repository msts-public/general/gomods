// Package repo provide a mechanism for accessing data in a single database table
package repo

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"
)

// package constants
const (
	StatementCreate               string = "create"
	StatementUpdate               string = "update"
	StatementDelete               string = "delete"
	StatementRemove               string = "remove"
	StatementSelectByID           string = "select by id"
	StatementSelectByIDs          string = "select by ids"
	StatementSelectByAltID        string = "select by alt id"
	StatementSelectByParam        string = "select by param"
	StatementSelectForUpdate      string = "select for update"
	StatementSelectByAltForUpdate string = "select by alt for update"
	StatementCount                string = "select count"

	ColumnsCreate string = "create"
	ColumnsUpdate string = "update"
	ColumnsReturn string = "return"

	ForUpdateNowait string = " for no key update nowait"
	ForUpdateSkip   string = " for no key update skip locked"
)

// Base is the base set of properties for a repository
type Base struct {
	// Services
	Logger      zerolog.Logger
	Tx          *sqlx.Tx
	TableName   string
	OrderByFunc func() string
	LockOption  string

	// cached prepared statements
	getByIDStmt          *sqlx.Stmt
	getByIDUpdateStmt    *sqlx.Stmt
	getByAltIDStmt       *sqlx.Stmt
	getByAltIDUpdateStmt *sqlx.Stmt
	createStmt           *sqlx.NamedStmt
	updateStmt           *sqlx.NamedStmt
	deleteStmt           *sqlx.NamedStmt
	removeStmt           *sqlx.NamedStmt
	countStmt            *sqlx.Stmt
	countByParamSQL      string

	// bare SQL statements
	VarGetByIDSQL      string
	VarGetByAltIDSQL   string
	VarGetByParamSQL   string
	VarCreateRecordSQL string
	VarUpdateRecordSQL string
	VarDeleteRecordSQL string
	VarRemoveRecordSQL string

	// lists of table column names to be used in statments
	CreateColumns Columns
	UpdateColumns Columns
	ReturnColumns Columns
	allColumns    Columns
	ColumnMap     map[string]Columns

	Filters             ColumnFilters     // columns to exclude from lists
	LogicalDeleteColumn string            // name of column used for logical deletes
	AltIDColumn         string            // alternative unique key field name
	customQueries       map[string]string // set of custom queries for returning table data
}

// Init initialises the repository
func (r *Base) Init(rec interface{}) error {
	if r.TableName == "" {
		return NewRepoError("repo", "TableName must be defined for repo", nil)
	}
	if r.LogicalDeleteColumn == "" {
		r.LogicalDeleteColumn = "deleted_at"
	}
	if r.AltIDColumn == "" {
		r.AltIDColumn = "api_id"
	}
	if r.LockOption == "" {
		r.LockOption = ForUpdateNowait
	}

	r.allColumns = GetColumns(rec)
	for _, c := range r.allColumns {
		if _, ok := r.Filters.Create[c]; !ok {
			r.CreateColumns = append(r.CreateColumns, c)
		}
		if _, ok := r.Filters.Update[c]; !ok {
			r.UpdateColumns = append(r.UpdateColumns, c)
		}
		if _, ok := r.Filters.Return[c]; !ok {
			r.ReturnColumns = append(r.ReturnColumns, c)
		}
	}

	r.ColumnMap = map[string]Columns{
		ColumnsCreate: r.CreateColumns,
		ColumnsUpdate: r.UpdateColumns,
		ColumnsReturn: r.ReturnColumns,
	}

	r.customQueries = map[string]string{}

	return nil
}

// InitTx initialises the repository with a new DB tx
func (r *Base) InitTx(tx *sqlx.Tx) error {
	if tx != nil {
		r.Tx = tx
	}

	if r.Tx == nil {
		return NewRepoError(r.TableName, "Cannot initialise repo without transaction", nil)
	}

	// clear prepared
	r.getByIDStmt = nil
	r.getByAltIDStmt = nil
	r.getByIDUpdateStmt = nil
	r.getByAltIDUpdateStmt = nil
	r.createStmt = nil
	r.updateStmt = nil
	r.deleteStmt = nil
	r.removeStmt = nil
	r.countStmt = nil

	return nil
}

// AddCustom adds a custom query to rebp. As any custom query is intended to return a record
//   (or array of records) of repo table type, this allows the column list to be specified
//    by '%s' and substituted in from parsed table columns as in standard queries
func (r *Base) AddCustom(name, stmt, alias string) error {
	if r.customQueries == nil {
		return NewRepoError("repo", "Repo must be initialised before adding custom queries", nil)
	}

	if strings.Contains(strings.ToLower(stmt), "select %s") {
		if alias != "" {
			r.customQueries[name] = fmt.Sprintf(stmt, r.ReturnColumns.ToPrefixedList(alias+"."))
		} else {
			r.customQueries[name] = fmt.Sprintf(stmt, r.ReturnColumns.ToList())
		}
	} else {
		r.customQueries[name] = stmt
	}

	return nil
}

// GetRowByID fetches a table row by the primary key value
func (r *Base) GetRowByID(querySQL string, recID interface{}, forUpdate bool) (*sqlx.Row, error) {
	l := r.Logger.With().Str("function", "GetRowByID").Logger()

	l.Trace().Str("id", fmt.Sprintf("%v", recID)).Msg("Fetching row")

	if r.Tx == nil {
		l.Warn().Msg("Transaction not initialized")
		return nil, NewRepoError(r.TableName, "Transaction not initialized", nil)
	}

	var stmt *sqlx.Stmt
	var err error

	// check for cached statement
	if forUpdate {
		l.Trace().Msg("Fetching for update")
		stmt = r.getByIDUpdateStmt
		querySQL = querySQL + r.LockOption
	} else {
		stmt = r.getByIDStmt
	}

	// prepare statement
	if stmt == nil {
		stmt, err = r.Tx.Preparex(querySQL)
		if err != nil {
			l.Warn().Err(err).Msg("Failed preparing query SQL")
			l.Trace().Msgf("Query >%s<", querySQL)
			l.Trace().Msgf("ID >%s<", recID)
			return nil, NewFromDatabaseError(err, r.TableName)
		}
	}

	// cache statement
	if forUpdate {
		r.getByIDUpdateStmt = stmt
	} else {
		r.getByIDStmt = stmt
	}

	return stmt.QueryRowx(recID), nil
}

// GetRecByID fetches a table record by the primary key value
func (r *Base) GetRecByID(querySQL string, recID interface{}, rec interface{}, forUpdate bool) error {
	l := r.Logger.With().Str("function", "GetRecByID").Logger()

	l.Trace().Str("id", fmt.Sprintf("%v", recID)).Msg("Fetching record")

	if r.Tx == nil {
		l.Warn().Msg("Transaction not initialized")
		return NewRepoError(r.TableName, "Transaction not initialized", nil)
	}

	var stmt *sqlx.Stmt
	var err error

	// check for cached statement
	if forUpdate {
		l.Trace().Msg("Fetching for update")
		stmt = r.getByIDUpdateStmt
		querySQL = querySQL + r.LockOption
	} else {
		stmt = r.getByIDStmt
	}

	// prepare statement
	if stmt == nil {
		stmt, err = r.Tx.Preparex(querySQL)
		if err != nil {
			l.Warn().Err(err).Msg("Failed preparing query SQL")
			l.Trace().Msgf("Query >%s<", querySQL)
			l.Trace().Msgf("ID >%s<", recID)
			return NewFromDatabaseError(err, r.TableName)
		}
	}

	// cache statement
	if forUpdate {
		r.getByIDUpdateStmt = stmt
	} else {
		r.getByIDStmt = stmt
	}

	err = stmt.QueryRowx(recID).StructScan(rec)
	if err != nil {
		if err == sql.ErrNoRows {
			return ErrNoRows(r.TableName)
		}

		l.Warn().Err(err).Msg("Failed executing query")
		l.Trace().Msgf("statement: >%s<", querySQL)
		l.Trace().Msgf("id: >%v<", recID)

		rec = nil

		return NewFromDatabaseError(err, r.TableName)
	}

	l.Trace().Msg("Record fetched")

	return nil
}

// GetRecByAltID fetchs a table record by the alternate unique key value
func (r *Base) GetRecByAltID(querySQL string, recID interface{}, rec interface{}, forUpdate bool) error {
	l := r.Logger.With().Str("function", "GetRecByAltID").Logger()

	l.Trace().Str("id", fmt.Sprintf("%v", recID)).Msg("Fetching record")

	if r.Tx == nil {
		l.Warn().Msg("Transaction not initialized")
		return NewRepoError(r.TableName, "Transaction not initialized", nil)
	}

	var stmt *sqlx.Stmt
	var err error

	// check for cached statement
	if forUpdate {
		l.Trace().Msg("Fetching for update")
		stmt = r.getByAltIDUpdateStmt
		querySQL = querySQL + r.LockOption
	} else {
		stmt = r.getByAltIDStmt
	}

	// prepare statement
	if stmt == nil {
		stmt, err = r.Tx.Preparex(querySQL)
		if err != nil {
			l.Warn().Err(err).Msg("Failed preparing query SQL")
			l.Trace().Msgf("Query >%s<", querySQL)
			l.Trace().Msgf("API_ID >%s<", recID)
			return NewFromDatabaseError(err, r.TableName)
		}
	}

	// cache statement
	if forUpdate {
		r.getByAltIDUpdateStmt = stmt
	} else {
		r.getByAltIDStmt = stmt
	}

	err = stmt.QueryRowx(recID).StructScan(rec)
	if err != nil {
		if err == sql.ErrNoRows {
			return ErrNoRows(r.TableName)
		}

		l.Warn().Err(err).Msg("Failed executing query")
		l.Trace().Msgf("statement: >%s<", querySQL)
		l.Trace().Msgf("id: >%v<", recID)

		rec = nil

		return NewFromDatabaseError(err, r.TableName)
	}

	l.Trace().Msg("Record fetched")

	return nil
}

// GetRowsByParam - fetch set of table rows filtered by the provided parameters and options
func (r *Base) GetRowsByParam(
	querySQL string,
	params []Restriction,
	options *Options,
) (rows *sqlx.Rows, err error) {

	l := r.Logger.With().Str("function", "GetRowsByParam").Logger()

	if r.Tx == nil {
		l.Warn().Msg("Transaction not initialized")
		return nil, NewRepoError(r.TableName, "Transaction not initialized", nil)
	}

	// params
	querySQL, queryParams, err := r.sqlFromParamsAndOperator(querySQL, params, options)
	if err != nil {
		l.Warn().Err(err).Msg("Failed generating query")
		return nil, err
	}

	l.Trace().Msgf("Query >%s<", querySQL)
	l.Trace().Msgf("Parameters >%+v<", queryParams)

	rows, err = r.Tx.NamedQuery(querySQL, queryParams)
	if err != nil {
		l.Warn().Err(err).Msg("Failed querying row")
		return nil, NewFromDatabaseError(err, r.TableName)
	}

	return rows, nil
}

// GetRowsCustom fetches a set of table rows filtered by the custom query
func (r *Base) GetRowsCustom(queryName string, params map[string]interface{}) (*sqlx.Rows, error) {
	l := r.Logger.With().Str("function", "GetRowsCustom").Logger()

	if r.Tx == nil {
		l.Warn().Msg("Transaction not initialized")
		return nil, NewRepoError(r.TableName, "Transaction not initialized", nil)
	}

	if r.customQueries == nil {
		l.Warn().Msg("No custom queries defined")
		return nil, NewRepoError(r.TableName, "No custom queries defined", nil)
	}

	querySQL, ok := r.customQueries[queryName]
	if !ok {
		l.Warn().Str("name", queryName).Msg("Custom query not defined")
		return nil, NewRepoError(r.TableName, "Custom query not defined", queryName)
	}

	rows, err := r.Tx.NamedQuery(querySQL, params)
	if err != nil {
		l.Warn().Err(err).Msg("Failed querying row")
		return nil, NewFromDatabaseError(err, r.TableName)
	}

	return rows, nil
}

// CreateRec creates a record in the database from the provided record struct
func (r *Base) CreateRec(createSQL string, rec interface{}) error {
	l := r.Logger.With().Str("function", "CreateRec").Logger()

	if r.Tx == nil {
		l.Warn().Msg("Transaction not initialized")
		return NewRepoError(r.TableName, "Transaction not initialized", nil)
	}

	var stmt *sqlx.NamedStmt
	var err error

	if r.createStmt != nil {
		stmt = r.createStmt
	} else {
		stmt, err = r.Tx.PrepareNamed(createSQL)
		if err != nil {
			l.Warn().Err(err).Str("statement", createSQL).Msg("Failed preparing create SQL")
			return NewFromDatabaseError(err, r.TableName)
		}
		r.createStmt = stmt
	}

	err = stmt.QueryRowx(rec).StructScan(rec)
	if err != nil {
		l.Warn().Err(err).Msgf("Failed executing create")
		return NewFromDatabaseError(err, r.TableName)
	}

	return nil
}

// UpdateRec updates a record in the database from the provided record struct
func (r *Base) UpdateRec(updateSQL string, rec interface{}) error {
	l := r.Logger.With().Str("function", "UpdateRec").Logger()

	if r.Tx == nil {
		l.Warn().Msg("Transaction not initialized")
		return NewRepoError(r.TableName, "Transaction not initialized", nil)
	}

	var stmt *sqlx.NamedStmt
	var err error

	if r.updateStmt != nil {
		stmt = r.updateStmt
	} else {
		stmt, err = r.Tx.PrepareNamed(updateSQL)
		if err != nil {
			l.Warn().Err(err).Str("statement", updateSQL).Msg("Failed preparing update SQL")
			return NewFromDatabaseError(err, r.TableName)
		}
		r.updateStmt = stmt
	}

	err = stmt.QueryRowx(rec).StructScan(rec)
	if err != nil {
		l.Warn().Err(err).Msg("Failed executing update")
		return NewFromDatabaseError(err, r.TableName)
	}

	return nil
}

// DeleteRec logically deletes a record in the database for the given primary key value
func (r *Base) DeleteRec(deleteSQL string, recID interface{}) error {
	l := r.Logger.With().Str("function", "DeleteRec").Logger()

	l.Trace().Msgf("Delete record ID >%s<", recID)

	if r.Tx == nil {
		l.Warn().Msg("Transaction not initialized")
		return NewRepoError(r.TableName, "Transaction not initialized", nil)
	}

	var stmt *sqlx.NamedStmt
	var err error

	if r.deleteStmt != nil {
		stmt = r.deleteStmt
	} else {
		stmt, err = r.Tx.PrepareNamed(deleteSQL)
		if err != nil {
			l.Warn().Err(err).Msg("Failed preparing delete SQL")
			return NewFromDatabaseError(err, r.TableName)
		}
		r.deleteStmt = stmt
	}

	params := map[string]interface{}{
		"id":                  recID,
		r.LogicalDeleteColumn: time.Now().UTC(),
	}

	res, err := stmt.Exec(params)
	if err != nil {
		l.Warn().Err(err).Msg("Failed executing delete")
		return NewFromDatabaseError(err, r.TableName)
	}

	// rows affected
	raf, err := res.RowsAffected()
	if err != nil {
		l.Warn().Err(err).Msg("Failed executing rows affected")
		return NewFromDatabaseError(err, r.TableName)
	}

	// expect a single row
	if raf == 0 {
		return ErrNoRows(r.TableName)
	}
	if raf != 1 {
		return NewRepoError(r.TableName, "Expecting to delete exactly one row", fmt.Sprintf("deleted: %d", raf))
	}

	l.Trace().Msgf("Deleted >%d< records", raf)

	return nil
}

// RemoveRec deletes a record in the database for the given primary key value
func (r *Base) RemoveRec(removeSQL string, recID interface{}) error {

	l := r.Logger.With().Str("function", "RemoveRec").Logger()

	l.Trace().Msgf("Remove record ID >%s<", recID)

	if r.Tx == nil {
		l.Warn().Msg("Transaction not initialized")
		return NewRepoError(r.TableName, "Transaction not initialized", nil)
	}

	var stmt *sqlx.NamedStmt
	var err error

	if r.removeStmt != nil {
		stmt = r.removeStmt
	} else {
		stmt, err = r.Tx.PrepareNamed(removeSQL)
		if err != nil {
			l.Warn().Err(err).Msg("Failed preparing remove SQL")
			return NewFromDatabaseError(err, r.TableName)
		}
		r.removeStmt = stmt
	}

	params := map[string]interface{}{
		"id": recID,
	}

	res, err := stmt.Exec(params)
	if err != nil {
		l.Warn().Err(err).Msg("Failed executing remove")
		return NewFromDatabaseError(err, r.TableName)
	}

	// rows affected
	raf, err := res.RowsAffected()
	if err != nil {
		l.Warn().Err(err).Msg("Failed executing rows affected")
		return NewFromDatabaseError(err, r.TableName)
	}

	// expect a single row
	if raf == 0 {
		return ErrNoRows(r.TableName)
	}
	if raf != 1 {
		return NewRepoError(r.TableName, "Expecting to remove exactly one row", fmt.Sprintf("removed: %d", raf))
	}

	l.Trace().Msgf("Removed >%d< records", raf)

	return nil
}

// Count returns a count of the number of records in the table
func (r *Base) Count() (int, error) {

	l := r.Logger.With().Str("function", "Count").Logger()

	if r.Tx == nil {
		l.Warn().Msg("Transaction not initialized")
		return 0, NewRepoError(r.TableName, "Transaction not initialized", nil)
	}

	l.Trace().Msgf("Fetching Count() for >%v<", r.TableName)

	var stmt *sqlx.Stmt
	var err error

	if r.countStmt != nil {
		stmt = r.countStmt
	} else {
		stmt, err = r.Tx.Preparex(r.GenerateStatement(StatementCount, nil))
		if err != nil {
			l.Warn().Err(err).Msg("Failed preparing count SQL")
			return 0, NewFromDatabaseError(err, r.TableName)
		}
		r.countStmt = stmt
	}

	var count int64

	err = stmt.Get(&count)

	if err != nil {
		return int(count), NewFromDatabaseError(err, r.TableName)
	}

	return int(count), nil
}

// CountByParam returns a count of the number of records in the table that match the given params
func (r *Base) CountByParam(params []Restriction) (int, error) {
	l := r.Logger.With().Str("function", "CountByParam").Logger()

	if r.Tx == nil {
		l.Warn().Msg("Transaction not initialized")
		return 0, NewRepoError(r.TableName, "Transaction not initialized", nil)
	}

	var err error
	var querySQL string
	var queryParams map[string]interface{}

	if r.countByParamSQL != "" {
		querySQL = r.countByParamSQL
	} else {
		querySQL = r.GenerateStatement(StatementCount, nil)
		r.countByParamSQL = querySQL
	}

	if params != nil {
		querySQL, queryParams, err = r.sqlFromParamsAndOperator(querySQL, params, nil)
		if err != nil {
			l.Warn().Err(err).Msg("Failed building statement")
			return 0, err
		}
	}

	nStmt, err := r.Tx.PrepareNamed(querySQL)
	if err != nil {
		l.Warn().Err(err).Msg("Failed preparing statement")
		return 0, NewFromDatabaseError(err, r.TableName)
	}

	var count int64

	err = nStmt.Get(&count, queryParams)
	if err != nil {
		l.Warn().Err(err).Msg("Failed getting count")
		return int(count), NewFromDatabaseError(err, r.TableName)
	}

	return int(count), nil
}

// OrderBy returns the default order by clause for queries
func (r *Base) OrderBy() string {
	return "created_at desc"
}

// GenerateStatement generates the standard SQL statements
// statements requiring non-standard column lists must be explicitly generated by the user
func (r *Base) GenerateStatement(statementType string, columns map[string]Columns) (stmt string) {

	switch statementType {

	case StatementSelectByID:
		stmt = fmt.Sprintf(getByIDSQL, columns[ColumnsReturn].ToList(), r.TableName, r.LogicalDeleteColumn, "")
	case StatementSelectByAltID:
		stmt = fmt.Sprintf(getByAltIDSQL, columns[ColumnsReturn].ToList(), r.TableName, r.AltIDColumn, r.LogicalDeleteColumn, "")
	case StatementSelectByIDs:
		stmt = fmt.Sprintf(getByIDsSQL, columns[ColumnsReturn].ToList(), r.TableName, r.LogicalDeleteColumn)
	case StatementSelectForUpdate:
		stmt = fmt.Sprintf(getByIDSQL, columns[ColumnsReturn].ToList(), r.TableName, r.LogicalDeleteColumn, r.LockOption)
	case StatementSelectByParam:
		stmt = fmt.Sprintf(getByParamSQL, columns[ColumnsReturn].ToList(), r.TableName, r.LogicalDeleteColumn)
	case StatementSelectByAltForUpdate:
		stmt = fmt.Sprintf(getByAltIDSQL, columns[ColumnsReturn].ToList(), r.TableName, r.AltIDColumn, r.LogicalDeleteColumn, r.LockOption)

	case StatementCreate:
		stmt = fmt.Sprintf(createRecordSQL, r.TableName, columns[ColumnsCreate].ToList(), columns[ColumnsCreate].ToPrefixedList(":"), columns[ColumnsReturn].ToList())
	case StatementUpdate:
		stmt = fmt.Sprintf(updateRecordSQL, r.TableName, columns[ColumnsUpdate].ToUpdateList(), columns[ColumnsReturn].ToList())

	case StatementDelete:
		stmt = fmt.Sprintf(deleteRecordSQL, r.TableName, r.LogicalDeleteColumn, r.LogicalDeleteColumn, r.LogicalDeleteColumn)
	case StatementRemove:
		stmt = fmt.Sprintf(removeRecordSQL, r.TableName)

	case StatementCount:
		stmt = fmt.Sprintf(countSQL, r.TableName, r.LogicalDeleteColumn)

	default:
		stmt = ""
	}

	r.Logger.Trace().Msg(stmt)
	return stmt
}

// GetColumnList - Colunm list getter
func (r *Base) GetColumnList(name string) []string {
	return r.ColumnMap[name]
}

// hasColumn - checks if a given column name is a known column
func (r *Base) hasColumn(name string) bool {
	for _, col := range r.allColumns {
		if strings.ToLower(name) == col {
			return true
		}
	}
	return false
}

// SetLockOption allows the default lock option to be set on the repo.
// This will clear any cached 'for update' statemens. Valid option are "skip", "nowait", "default"
func (r *Base) SetLockOption(lock string) error {
	if v, ok := updateOptions[lock]; !ok {
		return NewRepoError(r.TableName, "invalid lock option", lock)
	} else {
		r.LockOption = v
	}

	r.getByIDUpdateStmt = nil
	r.getByAltIDUpdateStmt = nil

	return nil
}
