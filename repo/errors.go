package repo

import (
	"database/sql"
	"fmt"
	"net"
	"regexp"
	"strings"

	"github.com/jackc/pgconn"
	"github.com/lib/pq"
)

// Database errors -
const (
	TitleAccess    string = "Access Denied"
	TitleIntegrity string = "Data Integrity Error"
	TitleInvalid   string = "Invalid Request"
	TitleNoData    string = "Data Not Found"
	TitleData      string = "Data Exception"
	TitleDatabase  string = "Database Error"
	TitleQuery     string = "Query Error"
	TitleRepo      string = "Repo Setup Error"
	TitleSyntax    string = "Syntax Error"
	TitleService   string = "Service Not Available"
	TitleSystem    string = "System Error"

	QueryNoRows      string = "Query returned no records"
	NilRecord        string = "Nil record supplied"
	ErrInvalidColumn string = "Unrecognised column name for table"
)

type matcher struct {
	RegEx *regexp.Regexp
	Title string
}

// TODO ongoing: add handling for other possible request and struct scan errors
var errMatcher = map[string]matcher{
	"missing": {RegEx: regexp.MustCompile(`could not find name (\w+) in map`), Title: TitleSyntax},   // record set/struct fields mismatch
	"scan":    {RegEx: regexp.MustCompile(`missing destination name (\w+) in`), Title: TitleInvalid}, // record set/struct fields mismatch
	"convert": {RegEx: regexp.MustCompile(`cannot convert`), Title: TitleSyntax},                     // implicit type conversion
	"binds":   {RegEx: regexp.MustCompile(`expected \d+ arguments, got \d+`), Title: TitleInvalid},   // incorrect number of bind variables
	"pgundef": {RegEx: regexp.MustCompile(`cannot encode status undefined`), Title: TitleInvalid},    // pgtype field with Status: undefined
}

// Error - struct for error details
type Error struct {
	Title   string
	Context string
	Detail  string
	Value   interface{}
}

// Error returns error details as strinG
func (e Error) Error() string {
	var b strings.Builder

	_, _ = b.WriteString(e.Title)
	if e.Context != "" {
		_, _ = b.WriteString(" for '" + e.Context + "'")
	}
	_, _ = b.WriteString(": " + e.Detail)
	if e.Value != nil {
		_, _ = b.WriteString(fmt.Sprintf(" >%+v<", e.Value))
	}

	return b.String()
}

// NewAccessError returns a generic error wrapped in standard Error struct
func NewAccessError(err error) *Error {
	return &Error{
		Title:  TitleAccess,
		Detail: err.Error(),
	}
}

// NewDatabaseError provides standard format for database error details
func NewDatabaseError(context, message string, value interface{}) *Error {
	return &Error{
		Title:   TitleDatabase,
		Context: context,
		Detail:  message,
		Value:   value,
	}
}

// NewRepoError provides standard format for non-database errors
func NewRepoError(context, message string, value interface{}) *Error {
	return &Error{
		Title:   TitleRepo,
		Context: context,
		Detail:  message,
		Value:   value,
	}
}

// ErrNoRows provides standard for the database no rows found error
func ErrNoRows(context string) *Error {
	return &Error{
		Title:   TitleNoData,
		Context: context,
		Detail:  QueryNoRows,
	}
}

// IsErrNoRows checks if a given error is a database no rows found error
func IsErrNoRows(err error) bool {
	if err == nil {
		return false
	}

	if err == sql.ErrNoRows {
		return true
	}

	switch ve := err.(type) {
	case Error:
		if ve.Title == TitleNoData {
			return true
		}
	case *Error:
		if ve.Title == TitleNoData {
			return true
		}
	case *pq.Error:
		if ve.Code == "02000" {
			return true
		}
	default:
		fmt.Printf("--- got unhandled err type %T %+v\n", ve, ve)
	}
	return false
}

// NewFromDatabaseError converts a pq.Error to a Error
func NewFromDatabaseError(err error, defaultContext string) *Error {
	dberr := &Error{
		Context: defaultContext,
	}

	if err == sql.ErrNoRows {
		return ErrNoRows(defaultContext)
	} else if err == sql.ErrTxDone {
		return NewDatabaseError("tx", err.Error(), nil)
	} else if err == sql.ErrConnDone {
		return NewDatabaseError("connection", err.Error(), nil)
	}

	switch et := err.(type) {
	case *net.OpError:
		dberr.Title = TitleService
		dberr.Detail = "Connection failed"
	case *pgconn.PgError:
		dberr.Detail = strings.TrimSpace(et.Message + "\n" + et.Detail)
		dberr.Context = getErrTableContext(et, defaultContext)

		class := getPgErrorClass(et.Code)
		switch class {

		case "authorization":
			dberr.Title = TitleAccess
			dberr.Context = class
		case "integrity":
			dberr.Title = TitleIntegrity
		case "data_exception":
			dberr.Title = TitleData
		case "no_data":
			dberr.Title = TitleNoData
		case "service_error":
			dberr.Title = TitleService
			dberr.Context = "database"
		case "syntax_error":
			dberr.Title = TitleSyntax
		case "system_error":
			dberr.Title = TitleSystem
			dberr.Context = "database"
		case "transaction_state":
			dberr.Title = TitleService
			dberr.Context = "database"
		case "success", "warning":
			// not an error - should never get here
			return nil

		default:
			fmt.Printf("--- unhandled database code: %s", et.Code)
			dberr.Title = TitleService
			dberr.Detail = "Unable to process request at this time"
		}

	case *pq.Error:
		if pqerr, ok := err.(*pq.Error); ok {
			dberr.Detail = pqerr.Message + "\n" + pqerr.Detail

			switch pqerr.Code.Class() {
			case "22": // data exception
				dberr.Title = TitleInvalid
				dberr.Detail = pqerr.Message
			case "23": // integrity exception
				dberr = pqErrConstraintViolation(pqerr)
			case "25", "42": // transaction exception, syntax error
				dberr.Title = TitleSyntax
				dberr.Detail = pqerr.Message
			case "28": // authorizition exception
				dberr.Title = TitleAccess
				dberr.Detail = "Access denied"
				dberr.Context = "database"
			case "53", "54", "58": // resource, limit, and system errors - should probably raise an alert if these ever occur
				dberr.Title = TitleService
				dberr.Detail = "Unable to process request at this time"
				dberr.Context = "database"
			default:
				fmt.Printf("--- unhandled database code: %s\n", pqerr.Code)
				dberr.Title = TitleService
				dberr.Detail = "Unable to process request at this time"
			}
		} else {
			fmt.Printf("--- shouldn't get here\n")
			dberr.Title = TitleService
			dberr.Detail = "Unable to process request at this time\n" + err.Error()
		}

	default:
		foundMatch := false
		for retype, matcher := range errMatcher {
			if matcher.RegEx.MatchString(et.Error()) {
				dberr.Title = matcher.Title
				foundMatch = true

				switch retype {
				case "missing":
					dberr.Value = matcher.RegEx.FindStringSubmatch(et.Error())[1]
					dberr.Detail = "invalid parameter"
				case "scan":
					dberr.Value = matcher.RegEx.FindStringSubmatch(et.Error())[1]
					dberr.Detail = et.Error()
				default:
					dberr.Detail = et.Error()
				}
				break
			}
		}
		if !foundMatch {
			fmt.Printf("--- not a recognised error: %T - %s\n", et, et)
			dberr.Title = TitleSystem
			dberr.Detail = fmt.Sprintf("Unable to process request at this time\nUnexpected error type: >%T<: %+v", et, et)
		}
	}

	return dberr
}

// getPgErrorClass returns a class name for the postgres codes
func getPgErrorClass(code string) string {
	var class string

	switch code[0:2] {
	case "00": // success - not a error
		class = "success"
	case "01": // warnings - not an error
		class = "warning"
	case "02": // no data
		class = "no_data"
	case "03": // sql statement not complete
		class = "syntax_error"
	case "08": // connection
		class = "service_error"
	case "09": // trigger exception
		class = "data_exception"
	case "0A": // feature not supported
		class = "service_error"
	case "0B": // invalid transaction initiation
		class = "service_error"
	case "0F": // locator exception
		class = "service_error"
	case "0L": // invalid grantor
		class = "syntax_error"
	case "0P": // invalid role
		class = "syntax_error"
	case "20": // case not found
		class = "syntax_error"
	case "21": // cardinality
		class = "integrity"
	case "22": // data exception
		class = "data_exception"
	case "23": // integrity constraint
		class = "integrity"
	case "24": // invalid cursor state
		class = "service_error"
	case "25": // transaction state
		class = "transaction_state"
	case "26": // invalid statement name
		class = "syntax_error"
	case "27": // trigger data change violation
		class = "data_exception"
	case "28": // auth spec
		class = "authorization"
	case "2D": // transaction termination
		class = "transaction_state"
	case "2F": // sql routine exception
		class = "data_exception"
	case "34": // invalid cursor name
		class = "syntax_error"
	case "3B": // savepoint
		class = "transaction_state"
	case "3D": // invalid catalog name
		class = "syntax_error"
	case "3F": // invalid schema name
		class = "syntax_error"
	case "40": // transaction rollback
		class = "transaction_state"
	case "42": // syntax error/access violation
		class = "syntax_error"
	case "44": // with check option violation
		class = "service_error"
	case "53": // insufficient resources
		class = "service_error"
	case "54": // program limit exceeded
		class = "service_error"
	case "57": // operator intevention
		class = "service_error"
	case "P0": // pl/pgsql error
		switch code {
		case "P0002":
			class = "no_data"
		case "P0003":
			class = "data_exception"
		default:
			class = "syntax_error"
		}
	default:
		class = "system_error"
	}

	return class
}

var constraintRE = regexp.MustCompile(`\((.+)\)=\((.+)\)`)

// getErrTableContext - check postgres error message for usable error context information
func getErrTableContext(pgerr *pgconn.PgError, defaultCtx string) string {
	ctx := strings.Trim(fmt.Sprintf("%s.%s", pgerr.TableName, pgerr.ColumnName), ".")
	if ctx == "" {
		return defaultCtx
	}
	return ctx
}

// pqErrConstraintViolation handles extracting constraint name informatior from postgres errors
func pqErrConstraintViolation(pqerr *pq.Error) *Error {
	de := &Error{
		Title:   TitleInvalid,
		Detail:  pqerr.Message,
		Context: pqerr.Table,
	}

	property := pqerr.Column

	if pqerr.Code == "23505" {
		de.Title = TitleIntegrity
	}
	if pqerr.Code == "23502" {
		de.Value = ">null<"
	}

	match := constraintRE.FindAllStringSubmatch(pqerr.Detail, 1)

	if len(match) > 0 {
		if property == "" && len(match[0]) > 1 {
			property = match[0][1]
		}
		if len(match[0]) > 2 {
			de.Value = match[0][2]
		}
	}

	if pqerr.Column != "" {
		de.Context = de.Context + "." + property
	}

	return de
}
