package repo

import (
	"fmt"
	"reflect"
	"strings"
)

// Columns is an array of string representing the column names for a table
type Columns []string

// ToList returns Colunms as a comma separated list in a string
func (c Columns) ToList() string {
	return strings.Join(c, ", ")
}

// ToPrefixedList returns Colunms as a comma separated list in string, with each item prepended with the given prefix
func (c Columns) ToPrefixedList(prefix string) string {
	colArray := make([]string, len(c))
	for i, col := range c {
		colArray[i] = fmt.Sprintf("%s%s", prefix, strings.TrimSpace(col))
	}
	return strings.Join(colArray, ", ")
}

// ToUpdateList formats Colunms for a SET clause of a UPDATE statement with each column set to a bind variable of the same name
func (c Columns) ToUpdateList() string {
	colArray := make([]string, len(c))
	for i, col := range c {
		colArray[i] = fmt.Sprintf("%s = :%s", col, strings.TrimSpace(col))
	}
	return strings.Join(colArray, ",\n\t  ")
}

// GetColumns extracts column names from a given struct based on `db` tags
func GetColumns(rec interface{}) Columns {
	c := Columns{}

	if rec == nil {
		return c
	}

	rt := reflect.TypeOf(rec)
	rv := reflect.ValueOf(rec)

	if rt.Kind() == reflect.Ptr {
		rt = rt.Elem()
	}
	if rt.Kind() != reflect.Struct {
		return c
	}

	if rv.Kind() == reflect.Ptr {
		rv = rv.Elem()
	}

	for i := 0; i < rt.NumField(); i++ {
		col := rt.Field(i).Tag.Get("db")
		if rt.Field(i).Anonymous {
			// handle embedded fields
			cols := GetColumns(rv.Field(i).Interface())
			c = append(c, cols...)
		} else if col != "" {
			c = append(c, col)
		}
	}
	return c
}
