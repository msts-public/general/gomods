// +build !test_integration

package repo

import (
	"database/sql"
	"fmt"
	"net"
	"testing"

	"github.com/jackc/pgconn"
	"github.com/stretchr/testify/require"
)

func TestError(t *testing.T) {
	require := require.New(t)

	e1 := NewAccessError(fmt.Errorf("test error"))
	require.Error(e1, "e1 is an error type")
	require.Equal(TitleAccess, e1.Title, "should have expected title")
	require.Equal("test error", e1.Detail, "should have expected detail")
	require.Equal(TitleAccess+": test error", e1.Error(), "should get expected error")

	e2 := NewDatabaseError("table", "some error", "x")
	require.Error(e2, "e2 is an error type")
	require.Equal(TitleDatabase, e2.Title, "should have expected title")
	require.Equal("some error", e2.Detail, "should have expected detail")
	require.Equal("table", e2.Context, "should have expected context")
	require.Equal("x", e2.Value, "should have expected value")
	require.Equal(TitleDatabase+" for 'table': some error >x<", e2.Error(), "should get expected error")

	e3 := NewRepoError("table", "config error", 2)
	require.Error(e3, "e3 is an error type")
	require.Equal(TitleRepo, e3.Title, "should have expected title")
	require.Equal("config error", e3.Detail, "should have expected detail")
	require.Equal("table", e3.Context, "should have expected context")
	require.Equal(2, e3.Value, "should have expected value")
	require.Equal(TitleRepo+" for 'table': config error >2<", e3.Error(), "should get expected error")

	e4 := ErrNoRows("table")
	require.Error(e4, "e4 is an error type")
	require.Equal(TitleNoData, e4.Title, "should have expected title")
	require.Equal(QueryNoRows, e4.Detail, "should have expected detail")
	require.Equal("table", e4.Context, "should have expected context")
	require.Equal(TitleNoData+" for 'table': "+QueryNoRows, e4.Error(), "should get expected error")
}

func TestPgError(t *testing.T) {
	testCases := map[string]string{
		"authorization":     "auth",
		"integrity error":   "integ",
		"data error":        "data",
		"no data error":     "noData",
		"service error":     "service",
		"system error":      "system",
		"syntax error":      "syntax",
		"transaction error": "tx",
		"non error":         "ok",
	}

	for tn, tc := range testCases {
		t.Logf("---- running: %s ----", tn)
		err := NewFromDatabaseError(pgErrs[tc], "default")
		require.Equal(t, repoErrs[tc], err, "should get expected repo error for pg error")
	}
}

var pgErrs = map[string]*pgconn.PgError{
	"ok":      {Code: "00000", Message: "Success"},
	"noData":  {Code: "02000", Message: "No data found", TableName: "empty_table"},
	"service": {Code: "08000", Message: "Connection not available"},
	"data":    {Code: "22012", Message: "Division by zero"},
	"integ":   {Code: "23000", Message: "Primary key violation", TableName: "pk_table", ColumnName: "pk_column"},
	"tx":      {Code: "25000", Message: "Invalid transaction state"},
	"auth":    {Code: "28P01", Message: "Invalid password"},
	"syntax":  {Code: "42601", Message: "Syntax error in query"},
	"system":  {Code: "58000", Message: "System error"},
}

var miscErrs = map[string]error{
	"sql":     sql.ErrNoRows,
	"net":     &net.OpError{},
	"miss":    fmt.Errorf("could not find name some_name in map"),
	"scan":    fmt.Errorf("missing destination name some_name in struct"),
	"bind":    fmt.Errorf("expected 5 arguments, got 4"),
	"convert": fmt.Errorf("cannot convert 5 to Text"),
}

var repoErrs = map[string]*Error{
	"ok":      nil,
	"noData":  {Title: TitleNoData, Detail: "No data found", Context: "empty_table"},
	"service": {Title: TitleService, Detail: "Connection not available", Context: "database"},
	"data":    {Title: TitleData, Detail: "Division by zero", Context: "default"},
	"integ":   {Title: TitleIntegrity, Detail: "Primary key violation", Context: "pk_table.pk_column"},
	"tx":      {Title: TitleService, Detail: "Invalid transaction state", Context: "database"},
	"auth":    {Title: TitleAccess, Detail: "Invalid password", Context: "authorization"},
	"syntax":  {Title: TitleSyntax, Detail: "Syntax error in query", Context: "default"},
	"system":  {Title: TitleSystem, Detail: "System error", Context: "database"},
	"sql":     {Title: TitleNoData, Detail: "Query returned no records", Context: "default"},
	"net":     {Title: TitleService, Detail: "Connection failed", Context: "default"},
	"miss":    {Title: TitleSyntax, Detail: "invalid parameter", Context: "default", Value: "some_name"},
	"scan":    {Title: TitleInvalid, Detail: "missing destination name some_name in struct", Context: "default", Value: "some_name"},
	"bind":    {Title: TitleInvalid, Detail: "expected 5 arguments, got 4", Context: "default"},
	"convert": {Title: TitleSyntax, Detail: "cannot convert 5 to Text", Context: "default"},
}

func TestNonPgError(t *testing.T) {
	testCases := map[string]string{
		"sql.NoData":    "sql",
		"network error": "net",
		"missing error": "miss",
		"scan error":    "scan",
		"bind error":    "bind",
		"convert error": "convert",
	}

	for tn, tc := range testCases {
		t.Logf("---- running: %s ----", tn)
		err := NewFromDatabaseError(miscErrs[tc], "default")
		require.Equal(t, repoErrs[tc], err, "should get expected repo error for pg error")
		t.Logf("-- got error: %+v", err)
	}
}
