package repo

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/jackc/pgtype"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/rs/zerolog"
)

// BaseMock - Base properties for all repo's
type BaseMock struct {
	Logger    zerolog.Logger
	TableName string
}

// Init - satisfy interface requirements
func (r *BaseMock) Init() error {
	return nil
}

// InitTx - satisfy interface requirements
func (r *BaseMock) InitTx(tx *sqlx.Tx) error {
	return nil
}

// GetRowsByParam - satisfy interface requirements
func (r *BaseMock) GetRowsByParam(querySQL string, params []Restriction, options *Options) (rows *sqlx.Rows, err error) {
	return nil, nil
}

// GetByParamSQL -
func (r *BaseMock) GetByParamSQL() string {
	return "SELECT * FROM 1"
}

// CompareParams iterates over parameters and struct column to find matches, to mimic real query filtering.
// Not all possible options are necessarily covered, so some restrictions may not give the expected dataset.
func (r *BaseMock) CompareParams(params []Restriction, rec interface{}) (bool, error) {

	recElem := reflect.ValueOf(rec)
	if recElem.Kind() == reflect.Ptr {
		recElem = recElem.Elem()
	}

	// loop over restriction params
	for _, param := range params {
		foundCol := false
		// loop over the record fields to find a matching
		for i := 0; i < recElem.NumField(); i++ {
			var stringValue string
			var tp reflect.Type

			// Get the tag name
			valueField := recElem.Field(i)
			typeField := recElem.Type().Field(i)
			tag := typeField.Tag.Get("db")

			// if element is an embedded struct loop over the struct fields to determine if the
			// restriction column is in the embedded struct
			if valueField.Kind() == reflect.Struct && typeField.Anonymous {
				subElem := reflect.ValueOf(valueField.Interface())
				if subElem.Kind() == reflect.Ptr {
					subElem = subElem.Elem()
				}

				for j := 0; j < subElem.NumField(); j++ {
					valueField = subElem.Field(j)

					typeField = subElem.Type().Field(j)
					tag = typeField.Tag.Get("db")
					if param.Column == tag {
						// found matching column in sub-element, break out to get the value
						break
					}
				}
			}

			if param.Column == tag {
				foundCol = true

				switch valueField.Kind() {
				case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:

					stringValue = fmt.Sprintf("%010d", valueField.Int())

				case reflect.String:
					stringValue = valueField.String()

				case reflect.Bool:
					stringValue = fmt.Sprintf("%v", valueField.Interface())

				case reflect.Slice:
					ifaceValue := valueField.Interface()
					tp = reflect.TypeOf(ifaceValue)
					if tp.String() == "pq.StringArray" {
						stringValue = "|" + strings.Join(ifaceValue.(pq.StringArray), "|") + "|"
					}

				case reflect.Struct:
					iface := valueField.Interface()
					tp = reflect.TypeOf(iface)

					var ifaceValue string

					switch tp.String() {
					case "time.Time":
						ifaceValue = iface.(time.Time).UTC().Format(time.RFC3339)
					case "sql.NullString":
						ifaceValue = iface.(sql.NullString).String
					case "sql.NullTime":
						ifaceValue = iface.(sql.NullTime).Time.Format(time.RFC3339)
					case "pgtype.UUID":
						tmp := iface.(pgtype.UUID)
						tmp.AssignTo(&ifaceValue)
					default:
						ifaceValue = fmt.Sprintf("%+v", iface)
					}

					stringValue = ifaceValue

				case reflect.Map:
					iface := valueField.Interface()
					bVal, err := json.Marshal(iface)
					if err == nil {
						stringValue = string(bVal)
					}

				default:
					// no-op
				}

				// Convert the param field to a string
				paramString := ""
				paramField := reflect.ValueOf(param.Value)

				switch paramField.Kind() {
				case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:

					paramString = fmt.Sprintf("%010d", paramField.Int())

				case reflect.String:
					paramString = paramField.String()

					if tp != nil && tp.String() == "pq.StringArray" {
						// do matching here and set paramString to stringValue if matches, so it will match below
						if strings.Contains(stringValue, "|"+paramString+"|") {
							paramString = stringValue
						}
					}

				case reflect.Bool:
					paramString = fmt.Sprintf("%v", paramField.Interface())

				case reflect.Slice:

					switch vt := param.Value.(type) {
					case []byte:
						// assumption - an array of bytes is a marshalled json or similar, so just cast it as a string
						paramString = string(vt)
					case []string:
						// check for matching value here
						// - assumption: if value is array want to do an 'in' comparison, nothing else makes sense
						for _, arrayValue := range vt {
							if stringValue == arrayValue {
								paramString = arrayValue
								break
							}
						}
					default:
						// TODO: handle other array types
						paramString = ""
					}

				case reflect.Struct:
					iface := paramField.Interface()
					tp := reflect.TypeOf(iface)

					var ifaceValue string

					switch tp.String() {
					case "time.Time":
						ifaceValue = iface.(time.Time).UTC().Format(time.RFC3339)
					case "sql.NullString":
						ifaceValue = iface.(sql.NullString).String
					case "sql.NullTime":
						ifaceValue = iface.(sql.NullTime).Time.Format(time.RFC3339)
					case "pgtype.UUID":
						tmp := iface.(pgtype.UUID)
						tmp.AssignTo(&ifaceValue)
					default:
						ifaceValue = fmt.Sprintf("%+v", iface)
					}

					paramString = ifaceValue

				case reflect.Map:
					iface := valueField.Interface()
					bVal, err := json.Marshal(iface)
					if err == nil {
						paramString = string(bVal)
					}

				default:
					// no-op
				}

				// compare record and param using operator: TODO - Add more operators
				switch param.Operator {
				case OperatorNotEqualTo:
					if !(stringValue != paramString) {
						return false, nil
					}
				case OperatorLessThan:
					if !(stringValue < paramString) {
						return false, nil
					}
				case OperatorLessThanEqualTo:
					if !(stringValue <= paramString) {
						return false, nil
					}
				case OperatorGreaterThan:
					if !(stringValue > paramString) {
						return false, nil
					}
				case OperatorGreaterThanEqualTo:
					if !(stringValue >= paramString) {
						return false, nil
					}
				case OperatorIn:
					hasMatch := false
					for _, part := range strings.Split(paramString, ",") {
						if strings.TrimSpace(part) == stringValue {
							hasMatch = true
							break
						}
					}
					if !hasMatch {
						return false, nil
					}
				case OperatorNotIn:
					hasMatch := false
					for _, part := range strings.Split(paramString, ",") {
						if strings.TrimSpace(part) == stringValue {
							hasMatch = true
							break
						}
					}
					if hasMatch {
						return false, nil
					}
				default: // OperatorEqual
					if paramString != stringValue {
						return false, nil
					}
				}
			}
		}
		// return error if the restriction column is not in the table
		if !foundCol {
			return false, &Error{Title: TitleSyntax, Context: r.TableName, Detail: fmt.Sprintf(`column "%s" does not exist`, param.Column)}
		}
	}

	return true, nil
}

// SetLockOption - satisfy interface requirements
func (r *BaseMock) SetLockOption(lock string) error {
	return nil
}
