// +build test_integration

// for local testing against a real database to verify handling of real errors
package repo

import (
	"fmt"
	"os"
	"testing"
	"time"

	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/require"
)

type dbConnect struct {
	DBHost   string
	DBUser   string
	DBPass   string
	DBPort   string
	DBName   string
	DBSchema string
}

func setConnectString() string {
	host := os.Getenv("APP_DATABASE_HOST")
	user := os.Getenv("APP_DATABASE_USER")
	pass := os.Getenv("APP_DATABASE_PASS")
	port := os.Getenv("APP_DATABASE_PORT")
	dbname := os.Getenv("APP_DATABASE_NAME")
	schema := os.Getenv("APP_DATABASE_SCHEMA")

	if schema != "" {
		return fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable search_path=%s", user, pass, dbname, host, port, schema)
	}
	return fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", user, pass, dbname, host, port)
}

// assumes relevant env variables to connect to an existing postgres database are available
func TestError(t *testing.T) {
	require := require.New(t)

	cs := setConnectString()

	db, err := sqlx.Connect("pgx", cs)
	require.NoError(err, "failed new db")
	defer db.Close()

	_, err = db.Exec(`create temporary table if not exists test_table (
		id text constraint test_table_pk primary key,
		col_string text not null,
		col_int int not null,
		col_uuid uuid,
		created_at timestamp not null,
		updated_at timestamp not null,
		deleted_at timestamp)`)
	require.NoError(err, "failed creating temp table")

	// invalid sql
	_, err = db.Exec("invalid sql")
	require.Error(err)

	e2 := NewFromDatabaseError(err, "test")
	require.Equal(TitleSyntax, e2.Title)
	require.Equal("test", e2.Context)
	require.Regexp("syntax error at or near", e2.Detail)

	// invalid table name
	_, err = db.Query("select * from bad_table")
	require.Error(err)

	e2 = NewFromDatabaseError(err, "test")
	require.Equal(TitleSyntax, e2.Title)
	require.Equal("test", e2.Context)
	require.Regexp("relation .+ does not exist", e2.Detail)

	// no data
	tr := &TestTable{}
	err = db.Get(tr, "select * from test_table")
	require.Error(err)

	e2 = NewFromDatabaseError(err, "test")
	require.Equal(TitleNoData, e2.Title)
	require.Equal("test", e2.Context)
	require.Regexp("no records", e2.Detail)

	// invalid column name
	err = db.Get(tr, "select * from test_table where notacol = $1", 2)
	require.Error(err)

	e2 = NewFromDatabaseError(err, "test")
	require.Equal(TitleSyntax, e2.Title)
	require.Equal("test", e2.Context)
	require.Regexp("column .+ does not exist", e2.Detail)

	// mismatched parameter value and column types
	err = db.Get(tr, "select * from test_table where id = $1", 2)
	require.Error(err)

	e2 = NewFromDatabaseError(err, "test")
	require.Equal(TitleSyntax, e2.Title)
	require.Equal("test", e2.Context)
	require.Regexp("cannot convert 2 to Text", e2.Detail)

	// missing not null columns
	_, err = db.Exec("insert into test_table(id, col_string, col_int) values ($1, $2, $3)", "123", "col123", 123)
	require.Error(err)

	e2 = NewFromDatabaseError(err, "test")
	require.Equal(TitleIntegrity, e2.Title)
	require.Equal("test_table.created_at", e2.Context)
	require.Regexp("null value in column .+ violates not-null constraint", e2.Detail)

	// incorrect number of values
	now := time.Now().UTC()
	_, err = db.Exec(`insert into test_table(id, col_string, col_int, created_at, updated_at) values ($1, $2, $3, $4)`, "123", "col123", 123, now)
	require.Error(err)

	e2 = NewFromDatabaseError(err, "test")
	require.Equal(TitleSyntax, e2.Title)
	require.Equal("test", e2.Context)
	require.Regexp("INSERT has more target columns than expressions", e2.Detail)

	// missing bind
	_, err = db.Exec(`insert into test_table(id, col_string, col_int, created_at, updated_at) values ($1, $2, $3, $4, $5)`, "123", "col123", 123, now)
	require.Error(err)

	e2 = NewFromDatabaseError(err, "test")
	require.Equal(TitleInvalid, e2.Title)
	require.Equal("test", e2.Context)
	require.Regexp(`expected \d+ arguments, got \d+`, e2.Detail)

	// extra bind
	_, err = db.Exec(`insert into test_table(id, col_string, col_int, created_at, updated_at) values ($1, $2, $3, $4, $5)`, "123", "col123", 123, now, now, now)
	require.Error(err)

	e2 = NewFromDatabaseError(err, "test")
	require.Equal(TitleInvalid, e2.Title)
	require.Equal("test", e2.Context)
	require.Regexp(`expected \d+ arguments, got \d+`, e2.Detail)

	// dupicate record
	_, err = db.Exec(`insert into test_table(id, col_string, col_int, created_at, updated_at) values ($1, $2, $3, $4, $5)`, "123", "col123", 123, now, now)
	require.NoError(err)
	_, err = db.Exec(`insert into test_table(id, col_string, col_int, created_at, updated_at) values ($1, $2, $3, $4, $5)`, "123", "col123", 123, now, now)
	require.Error(err)

	e2 = NewFromDatabaseError(err, "test")
	require.Equal(TitleIntegrity, e2.Title)
	require.Equal("test_table", e2.Context)
	require.Regexp("Key .+ already exists", e2.Detail)
}
