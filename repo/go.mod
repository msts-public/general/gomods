module gitlab.com/msts-public/general/gomods/repo

go 1.16

replace (
	github.com/davecgh/go-spew => github.com/davecgh/go-spew v1.1.1
	github.com/jackc/pgconn => github.com/jackc/pgconn v1.11.0
	github.com/jackc/pgmock => github.com/jackc/pgmock v0.0.0-20210724152146-4ad1a8207f65
	github.com/jackc/pgproto3/v2 => github.com/jackc/pgproto3/v2 v2.2.0
	github.com/jackc/pgtype => github.com/jackc/pgtype v1.10.0
	github.com/jackc/pgx/v4 => github.com/jackc/pgx/v4 v4.15.0
	github.com/jackc/puddle => github.com/jackc/puddle v1.2.1
	github.com/lib/pq => github.com/lib/pq v1.10.4
	github.com/pkg/errors => github.com/pkg/errors v0.9.1
	github.com/rs/zerolog => github.com/rs/zerolog v1.26.1
	github.com/shopspring/decimal => github.com/shopspring/decimal v1.2.0
	github.com/stretchr/objx => github.com/stretchr/objx v0.2.0
	github.com/stretchr/testify => github.com/stretchr/testify v1.7.0
	go.uber.org/atomic => go.uber.org/atomic v1.6.0
	go.uber.org/multierr => go.uber.org/multierr v1.5.0
	go.uber.org/zap v1.13.0 => go.uber.org/zap v1.13.0
	golang.org/x/crypto => golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	golang.org/x/net => golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2
	golang.org/x/sys => golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e
	golang.org/x/tools => golang.org/x/tools v0.1.7
	golang.org/x/xerrors => golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
)

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/jackc/pgconn v1.11.0
	github.com/jackc/pgtype v1.10.0
	github.com/jackc/pgx/v4 v4.15.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.4
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/text v0.3.7 // indirect
)
