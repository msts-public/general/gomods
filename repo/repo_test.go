// +build !test_integration

package repo

import (
	"database/sql"
	"os"
	"regexp"
	"strings"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jackc/pgtype"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
)

var DefaultFilters = ColumnFilters{
	Create: map[string]*string{"deleted_at": nil, "serial_id": nil},
	Update: map[string]*string{"id": nil, "created_at": nil, "deleted_at": nil},
	Return: map[string]*string{"deleted_at": nil},
}

func dbMock() (*sqlx.DB, sqlmock.Sqlmock, error) {
	db, mock, err := sqlmock.New()
	if err != nil {
		return nil, nil, err
	}

	dbx := sqlx.NewDb(db, "sqlmock")
	return dbx, mock, nil
}

func TestInit(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	mock.ExpectBegin()
	mock.ExpectRollback()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	r := &Base{
		Logger:    l,
		Tx:        tx,
		TableName: tableName,
		Filters:   DefaultFilters,
	}

	err = r.Init(&TestTable{})
	require.NoError(err, "should not error")

	err = r.InitTx(tx)
	require.NoError(err, "should not error")

	tx.Rollback()
	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")

	colr := r.GetColumnList(ColumnsReturn)
	require.NotNil(colr)
	require.Len(colr, len(r.ReturnColumns))
	colb := r.GetColumnList("nonsense")
	require.Nil(colb)
}

func TestGenerateStatement(t *testing.T) {
	require := require.New(t)

	testCases := map[string]struct {
		lock   string
		expect *regexp.Regexp
	}{
		StatementCreate: {
			expect: regexp.MustCompile(`(?is)insert into "test_table" .+values.+returning.+`),
		},
		StatementUpdate: {
			expect: regexp.MustCompile(`(?is)update "test_table".+set .+returning.+`),
		},
		StatementDelete: {
			expect: regexp.MustCompile(`(?is)update "test_table".+set.+where id.+`),
		},
		StatementRemove: {
			expect: regexp.MustCompile(`(?is)delete from "test_table".+where id.+`),
		},
		StatementSelectByID: {
			expect: regexp.MustCompile(`(?is)select .+from "test_table".+where id = .+`),
		},
		StatementSelectByIDs: {
			expect: regexp.MustCompile(`(?is)select .+from "test_table".+where id in .+`),
		},
		StatementSelectByAltID: {
			expect: regexp.MustCompile(`(?is)select .+from "test_table".+where api_id = .+`),
		},
		StatementSelectByParam: {
			expect: regexp.MustCompile(`(?is)select .+from "test_table".+where deleted_at is null`),
		},
		StatementSelectForUpdate: {
			expect: regexp.MustCompile(`(?is)select .+from "test_table".+where id = .+for no key update nowait`),
		},
		StatementSelectByAltForUpdate: {
			expect: regexp.MustCompile(`(?is)select .+from "test_table".+where api_id = .+for no key update nowait`),
		},
		StatementSelectForUpdate + "Skip": {
			lock:   ForUpdateSkip,
			expect: regexp.MustCompile(`(?is)select .+from "test_table".+where id = .+for no key update skip locked`),
		},
		StatementSelectByAltForUpdate + "Skip": {
			lock:   ForUpdateSkip,
			expect: regexp.MustCompile(`(?is)select .+from "test_table".+where api_id = .+for no key update skip locked`),
		},
		StatementCount: {
			expect: regexp.MustCompile(`(?is)select count\(0\) from "test_table".+where deleted_at is null`),
		},
	}

	// test column init
	r := &Base{
		TableName: "test_table",
		Filters:   DefaultFilters,
	}

	err := r.Init(&TestTable{})
	require.NoError(err, "should not error")

	require.Len(r.ColumnMap, 3, "should have three column sets")
	require.Len(r.ColumnMap[ColumnsCreate], 6, "should have column set on insert")
	require.Len(r.ColumnMap[ColumnsUpdate], 4, "should have column set on update")
	require.Len(r.ColumnMap[ColumnsReturn], 6, "should have all columns from table except deleted at")

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			r := &Base{
				TableName: "test_table",
				Filters:   DefaultFilters,
			}
			if tc.lock != "" {
				r.LockOption = tc.lock
			}

			err := r.Init(&TestTable{})
			require.NoError(err, "should not error")

			stmt := tn
			if strings.Contains(stmt, "Skip") {
				stmt = strings.Replace(stmt, "Skip", "", 1)
			}

			sqlStmt := r.GenerateStatement(stmt, r.ColumnMap)
			require.Regexp(tc.expect, sqlStmt, "statement should match expected")
			t.Log(sqlStmt)
		})
	}
}

func TestGetRecByID(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	r := &Base{
		Logger:    l,
		TableName: tableName,
		Filters:   DefaultFilters,
	}

	r.Init(&TestTable{})

	mock.ExpectBegin()
	mock.ExpectPrepare(`(?is)select .+ from "` + tableName + `"(.+)`).ExpectQuery().
		WillReturnRows(sqlmock.NewRows(r.ReturnColumns).
			AddRow("122", "Name", 124, nil, "2020-09-01T12:13:14Z", "2020-09-01T12:13:14Z"))
	mock.ExpectPrepare(`(?is)select .+ from "` + tableName + `"(.+)for no key update nowait`).ExpectQuery().
		WillReturnRows(sqlmock.NewRows(r.ReturnColumns).
			AddRow("122", "Name", 124, nil, "2020-09-01T12:13:14Z", "2020-09-01T12:13:14Z"))
	mock.ExpectCommit()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	stmt := r.GenerateStatement(StatementSelectByID, r.ColumnMap)

	rec := &TestTable{}
	err = r.GetRecByID(stmt, "122", rec, false)
	require.Error(err, "should error for uninitialized tx")

	r.InitTx(tx)

	rec = &TestTable{}
	err = r.GetRecByID(stmt, "122", rec, false)
	require.NoError(err, "should not error")
	require.NotEmpty(rec, "should get a record back")

	rec = &TestTable{}
	err = r.GetRecByID(stmt, "122", rec, true)
	require.NoError(err, "should not error")
	require.NotEmpty(rec, "should get a record back")

	tx.Commit()

	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")
}

func TestGetRecByAltID(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	r := &Base{
		Logger:    l,
		TableName: tableName,
		Filters:   DefaultFilters,
	}

	r.Init(&TestTable{})

	mock.ExpectBegin()
	mock.ExpectPrepare(`(?is)select .+ from "` + tableName + `"(.+)`).ExpectQuery().
		WillReturnRows(sqlmock.NewRows(r.ReturnColumns).
			AddRow("122", "Name", 124, nil, "2020-09-01T12:13:14Z", "2020-09-01T12:13:14Z"))
	mock.ExpectPrepare(`(?is)select .+ from "` + tableName + `"(.+)for no key update nowait`).ExpectQuery().
		WillReturnRows(sqlmock.NewRows(r.ReturnColumns).
			AddRow("122", "Name", 124, nil, "2020-09-01T12:13:14Z", "2020-09-01T12:13:14Z"))
	mock.ExpectCommit()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	stmt := r.GenerateStatement(StatementSelectByAltID, r.ColumnMap)

	rec := &TestTable{}
	err = r.GetRecByAltID(stmt, "122", rec, false)
	require.Error(err, "should error for uninitialized tx")

	r.InitTx(tx)

	rec = &TestTable{}
	err = r.GetRecByAltID(stmt, "122", rec, false)
	require.NoError(err, "should not error")
	require.NotEmpty(rec, "should get a record back")

	rec = &TestTable{}
	err = r.GetRecByAltID(stmt, "122", rec, true)
	require.NoError(err, "should not error")
	require.NotEmpty(rec, "should get a record back")

	tx.Commit()

	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")
}

func TestGetRowByID(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	r := &Base{
		Logger:    l,
		TableName: tableName,
		Filters:   DefaultFilters,
	}
	r.Init(&TestTable{})

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	mock.ExpectBegin()
	mock.ExpectPrepare(`(?is)select .+ from "` + tableName + `"(.+)`).ExpectQuery().
		WillReturnRows(sqlmock.NewRows(r.ReturnColumns).
			AddRow("122", "Name", 124, nil, "2020-09-01T12:13:14Z", "2020-09-01T12:13:14Z"))
	mock.ExpectPrepare(`(?is)select .+ from "` + tableName + `"(.+)for no key update nowait`).ExpectQuery().
		WillReturnRows(sqlmock.NewRows(r.ReturnColumns).
			AddRow("123", "Name", 124, nil, "2020-09-01T12:13:14Z", "2020-09-01T12:13:14Z"))
	mock.ExpectPrepare(`(?is)select .+ from "` + tableName + `"(.+)for no key update skip locked`).ExpectQuery().
		WillReturnRows(sqlmock.NewRows(r.ReturnColumns).
			AddRow("124", "Name", 124, nil, "2020-09-01T12:13:14Z", "2020-09-01T12:13:14Z"))
	mock.ExpectCommit()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	stmt := r.GenerateStatement(StatementSelectByID, r.ColumnMap)

	row, err := r.GetRowByID(stmt, "122", false)
	require.Error(err, "should error for uninitialized tx")

	r.InitTx(tx)

	row, err = r.GetRowByID(stmt, "122", false)
	require.NoError(err, "should not error")
	require.NotEmpty(row, "should get a row back")

	rec := &TestTable{}
	err = row.StructScan(rec)
	require.NoError(err, "row should scan into struct")
	require.NotEmpty(rec, "should get a record back")

	row, err = r.GetRowByID(stmt, "123", true)
	require.NoError(err, "should not error")
	require.NotEmpty(row, "should get a row back")

	err = r.SetLockOption("skip")
	require.NoError(err, "should set lock option")

	row, err = r.GetRowByID(stmt, "124", true)
	require.NoError(err, "should not error")
	require.NotEmpty(row, "should get a row back")

	tx.Commit()

	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")
}

func TestGetRowsByParams(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	r := &Base{
		Logger:    l,
		TableName: tableName,
		Filters:   DefaultFilters,
	}
	r.Init(&TestTable{})

	mock.ExpectBegin()
	mock.ExpectQuery(`(?is)select .+ from "` + tableName + `"(.+)`).
		WillReturnRows(sqlmock.NewRows(r.ReturnColumns).
			AddRow("122", "Name", 124, nil, "2020-09-01T12:13:14Z", "2020-09-01T12:13:14Z"))
	mock.ExpectCommit()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	stmt := r.GenerateStatement(StatementSelectByParam, r.ColumnMap)

	rows, err := r.GetRowsByParam(stmt, nil, nil)
	require.Error(err, "should error for uninitialized tx")

	r.InitTx(tx)

	rows, err = r.GetRowsByParam(stmt, nil, nil)
	require.NoError(err, "should not error")
	require.NotEmpty(rows, "should get rows back")

	recs := []*TestTable{}

	for rows.Next() {
		rec := &TestTable{}
		err = rows.StructScan(rec)
		require.NoError(err, "should scan row to struct")
		recs = append(recs, rec)
	}
	require.Len(recs, 1, "should have one recond")

	tx.Commit()

	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")
}

func TestGetRowsCustom(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	r := &Base{
		Logger:    l,
		TableName: tableName,
		Filters:   DefaultFilters,
	}
	r.Init(&TestTable{})

	tests := []struct {
		sqlStr string
		name   string
		alias  string
	}{
		{
			sqlStr: "select %s from test_table",
			name:   "DummySQL1",
		},
		{
			sqlStr: "select %s from test_table where id = :id",
			name:   "DummySQL2",
			alias:  "t",
		},
	}

	for _, tc := range tests {
		err := r.AddCustom(tc.name, tc.sqlStr, tc.alias)
		require.NoError(err, "failed adding custom sql")
	}

	mock.ExpectBegin()
	mock.ExpectQuery(`(?is)^select .+ from ` + tableName + `$`).
		WillReturnRows(sqlmock.NewRows(r.ReturnColumns).
			AddRow("122", "Name", 124, nil, "2020-09-01T12:13:14Z", "2020-09-01T12:13:14Z"))
	mock.ExpectQuery(`(?is)select .+ from ` + tableName + `(.+) id = ?`).
		WillReturnError(sql.ErrNoRows)
	mock.ExpectCommit()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	rows, err := r.GetRowsCustom(tests[0].name, nil)
	require.Error(err, "should error for uninitialized tx")

	r.InitTx(tx)

	rows, err = r.GetRowsCustom(tests[0].name, nil)
	require.NoError(err, "should not error")
	require.NotEmpty(rows, "should get rows back")

	recs := []*TestTable{}

	for rows.Next() {
		rec := &TestTable{}
		err = rows.StructScan(rec)
		require.NoError(err, "should scan row to struct")
		recs = append(recs, rec)
	}
	require.Len(recs, 1, "should have one recond")

	rows, err = r.GetRowsCustom(tests[1].name, map[string]interface{}{"id": "abcdef"})
	require.Error(err, "should return error")

	tx.Commit()

	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")
}

func TestCreateRec(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	r := &Base{
		Logger:    l,
		TableName: tableName,
		Filters:   DefaultFilters,
	}
	r.Init(&TestTable{})

	mock.ExpectBegin()
	mock.ExpectPrepare(`(?is)insert into "` + tableName + `" .+values .+returning .+`).ExpectQuery().
		WillReturnRows(sqlmock.NewRows(r.ReturnColumns).
			AddRow("122", "Name", 124, nil, "2020-09-01T12:13:14Z", "2020-09-01T12:13:14Z"))
	mock.ExpectCommit()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	stmt := r.GenerateStatement(StatementCreate, r.ColumnMap)

	now := time.Now().UTC().Format(time.RFC3339)
	rec := &TestTable{
		ColStr:    "123",
		ColInt:    23,
		ColUUID:   pgtype.UUID{Status: pgtype.Null},
		CreatedAt: now,
		UpdatedAt: now,
	}
	err = r.CreateRec(stmt, rec)
	require.Error(err, "should error for uninitialized tx")

	r.InitTx(tx)

	err = r.CreateRec(stmt, rec)
	require.NoError(err, "should not error")

	tx.Commit()

	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")
}

func TestUpdateRec(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	r := &Base{
		Logger:    l,
		TableName: tableName,
		Filters:   DefaultFilters,
	}
	r.Init(&TestTable{})

	mock.ExpectBegin()
	mock.ExpectPrepare(`(?is)update "` + tableName + `".+set .+returning .+`).ExpectQuery().
		WillReturnRows(sqlmock.NewRows(r.ReturnColumns).
			AddRow("122", "Name", 124, nil, "2020-09-01T12:13:14Z", "2020-09-01T12:13:14Z"))
	mock.ExpectCommit()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	stmt := r.GenerateStatement(StatementUpdate, r.ColumnMap)

	now := time.Now().UTC().Format(time.RFC3339)
	rec := &TestTable{
		ColStr:    "123",
		ColInt:    23,
		ColUUID:   pgtype.UUID{Status: pgtype.Null},
		CreatedAt: now,
		UpdatedAt: now,
	}
	err = r.UpdateRec(stmt, rec)
	require.Error(err, "should error for uninitialized tx")

	r.InitTx(tx)

	err = r.UpdateRec(stmt, rec)
	require.NoError(err, "should not error")
	require.NotEmpty(rec.ID, "should get a record back")

	tx.Commit()

	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")
}

func TestDeleteRec(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	r := &Base{
		Logger:              l,
		TableName:           tableName,
		LogicalDeleteColumn: "deleted_at",
	}
	r.Init(&TestTable{})

	mock.ExpectBegin()
	mock.ExpectPrepare(`(?is)update "` + tableName + `".+set deleted_at = .+where .+deleted_at is null`).
		ExpectExec().WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	stmt := r.GenerateStatement(StatementDelete, r.ColumnMap)

	err = r.DeleteRec(stmt, "123")
	require.Error(err, "should error for uninitialized tx")

	r.InitTx(tx)

	err = r.DeleteRec(stmt, "123")
	require.NoError(err, "should not error")

	tx.Commit()

	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")
}

func TestRemoveRec(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	r := &Base{
		Logger:    l,
		TableName: tableName,
	}
	r.Init(&TestTable{})

	mock.ExpectBegin()
	mock.ExpectPrepare(`(?is)delete from "` + tableName + `".+where id .+`).
		ExpectExec().WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	stmt := r.GenerateStatement(StatementRemove, r.ColumnMap)

	err = r.RemoveRec(stmt, "i23")
	require.Error(err, "should error for uninitialized tx")

	r.InitTx(tx)

	err = r.RemoveRec(stmt, "i23")
	require.NoError(err, "should not error")

	tx.Commit()

	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")
}

func TestCount(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	r := &Base{
		Logger:    l,
		TableName: tableName,
		Filters:   DefaultFilters,
	}
	r.Init(&TestTable{})

	columns := []string{"count"}
	mock.ExpectBegin()
	mock.ExpectPrepare(`(?is)select count\(0\) from "` + tableName + `".+`).ExpectQuery().
		WillReturnRows(sqlmock.NewRows(columns).AddRow(1))
	mock.ExpectCommit()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	count, err := r.Count()
	require.Error(err, "should error for uninitialized tx")

	r.InitTx(tx)

	count, err = r.Count()
	require.NoError(err, "should not error")
	require.Equal(1, count, "should get expected count")
	tx.Commit()

	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")
}

func TestCountByParam(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	tableName := "test_table"

	db, mock, err := dbMock()
	require.NoError(err, "failed mock db")

	r := &Base{
		Logger:    l,
		TableName: tableName,
		Filters:   DefaultFilters,
	}
	r.Init(&TestTable{})

	columns := []string{"count"}
	mock.ExpectBegin()
	mock.ExpectPrepare(`(?is)select count\(0\) from "` + tableName + `"(.+)`).ExpectQuery().
		WillReturnRows(sqlmock.NewRows(columns).AddRow(1))
	mock.ExpectCommit()

	tx, err := db.Beginx()
	require.NoError(err, "failed new tx")

	count, err := r.CountByParam(nil)
	require.Error(err, "should error for uninitialized tx")

	r.InitTx(tx)

	count, err = r.CountByParam(nil) // TODO: test with params
	require.NoError(err, "should not error")
	require.Equal(1, count, "should get expected count")
	tx.Commit()

	require.NoError(mock.ExpectationsWereMet(), "all mock expectations should be met")
}
