// +build !test_integration

package repo

import (
	"database/sql"
	"fmt"
	"os"
	"regexp"
	"testing"
	"time"

	"github.com/lib/pq"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
)

type TestTableB struct {
	ColName  string         `db:"colname"`
	OtherCol int            `db:"othercol"`
	ColArray []string       `db:"colarray"`
	ColNull  sql.NullString `db:"colnull"`
	Col1     string         `db:"col1"`
	Col2     string         `db:"col2"`
	Col3     string         `db:"col3"`
}

func init() {
	zerolog.SetGlobalLevel(zerolog.WarnLevel)
}

func TestInClause(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	r := &Base{
		Logger:    l,
		Tx:        nil,
		TableName: "test",
	}

	testCases := map[string]struct {
		param   string
		joiner  string
		vals    interface{}
		expect  *regexp.Regexp
		qParams QueryParams
		err     error
	}{
		"in string": {
			param:   "colname",
			joiner:  "where",
			vals:    "abc ,def, ghi",
			expect:  regexp.MustCompile(`where colname IN \(:colname\d, :colname\d, :colname\d\)\n`),
			qParams: QueryParams{"colname0": "abc", "colname1": "def", "colname2": "ghi"},
		},
		"in []string": {
			param:   "colname",
			joiner:  "and",
			vals:    []string{"abc", "def"},
			expect:  regexp.MustCompile(`and colname IN \(:colname\d, :colname\d\)\n`),
			qParams: QueryParams{"colname0": "abc", "colname1": "def"},
		},
		"in pq.StringArray": {
			param:   "colname",
			joiner:  "and",
			vals:    pq.StringArray{"abc", "def"},
			expect:  regexp.MustCompile(`and colname IN \(:colname\d, :colname\d\)\n`),
			qParams: QueryParams{"colname0": "abc", "colname1": "def"},
		},
		"in []int": {
			param:   "colname",
			joiner:  "where",
			vals:    []int{123, 345},
			expect:  regexp.MustCompile(`where colname IN \(:colname\d, :colname\d\)\n`),
			qParams: QueryParams{"colname0": 123, "colname1": 345},
		},
		"in []int64": {
			param:   "colname",
			joiner:  "and",
			vals:    []int64{123, 345},
			expect:  regexp.MustCompile(`and colname IN \(:colname\d, :colname\d\)\n`),
			qParams: QueryParams{"colname0": int64(123), "colname1": int64(345)},
		},
		"in []int32": {
			param:   "colname",
			joiner:  "and",
			vals:    []int32{123, 345},
			expect:  regexp.MustCompile(`and colname IN \(:colname\d, :colname\d\)\n`),
			qParams: QueryParams{"colname0": int32(123), "colname1": int32(345)},
		},
		"in []int16": {
			param:   "colname",
			joiner:  "and",
			vals:    []int16{123, 345},
			expect:  regexp.MustCompile(`and colname IN \(:colname\d, :colname\d\)\n`),
			qParams: QueryParams{"colname0": int16(123), "colname1": int16(345)},
		},
		"in []int8": {
			param:   "colname",
			joiner:  "and",
			vals:    []int8{12, 34},
			expect:  regexp.MustCompile(`and colname IN \(:colname\d, :colname\d\)\n`),
			qParams: QueryParams{"colname0": int8(12), "colname1": int8(34)},
		},
		"empty params string": {
			param:  "colname",
			joiner: "where",
			vals:   "",
			err:    NewRepoError(r.TableName, "value for param is empty", "colname"),
		},
		"empty params array": {
			param:  "colname",
			joiner: "where",
			vals:   []string{},
			err:    NewRepoError(r.TableName, "value for param is empty", "colname"),
		},
		"empty params pq.StringArray": {
			param:  "colname",
			joiner: "where",
			vals:   pq.StringArray{},
			err:    NewRepoError(r.TableName, "value for param is empty", "colname"),
		},
	}

	for tn, tc := range testCases {
		t.Logf("---- running: %s ----", tn)

		clause, newParams, err := r.inClause("IN", tc.param, tc.joiner, tc.vals)
		if tc.err != nil {
			require.Errorf(err, "error should be returned when expected: %s %+v", clause, newParams)
			require.Equal(tc.err, err, "expected error should be returned")
			continue
		}
		require.NoError(err, "should not error when not expected")
		require.Regexp(tc.expect, clause, "should generate the expected clause")
		require.EqualValues(tc.qParams, newParams, "should return the expected query parameters")
	}
}

func TestBetweenClause(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	r := &Base{
		Logger:    l,
		Tx:        nil,
		TableName: "test",
	}

	now := time.Now().UTC()

	testCases := map[string]struct {
		param   string
		joiner  string
		vals    interface{}
		expect  *regexp.Regexp
		qParams QueryParams
		err     error
	}{
		"between string": {
			param:   "colname",
			joiner:  "where",
			vals:    "abc ,def",
			expect:  regexp.MustCompile(`where colname BETWEEN :colname_low AND :colname_high\n`),
			qParams: QueryParams{"colname_low": "abc", "colname_high": "def"},
		},
		"between []string": {
			param:   "colname",
			joiner:  "and",
			vals:    []string{"abc", "def"},
			expect:  regexp.MustCompile(`and colname BETWEEN :colname_low AND :colname_high\n`),
			qParams: QueryParams{"colname_low": "abc", "colname_high": "def"},
		},
		"between pq.StringArray": {
			param:   "colname",
			joiner:  "and",
			vals:    pq.StringArray{"abc", "def"},
			expect:  regexp.MustCompile(`and colname BETWEEN :colname_low AND :colname_high\n`),
			qParams: QueryParams{"colname_low": "abc", "colname_high": "def"},
		},
		"between []int": {
			param:   "colname",
			joiner:  "where",
			vals:    []int{123, 345},
			expect:  regexp.MustCompile(`where colname BETWEEN :colname_low AND :colname_high\n`),
			qParams: QueryParams{"colname_low": 123, "colname_high": 345},
		},
		"between []int64": {
			param:   "colname",
			joiner:  "and",
			vals:    []int64{123, 345},
			expect:  regexp.MustCompile(`and colname BETWEEN :colname_low AND :colname_high\n`),
			qParams: QueryParams{"colname_low": int64(123), "colname_high": int64(345)},
		},
		"between []int32": {
			param:   "colname",
			joiner:  "and",
			vals:    []int32{123, 345},
			expect:  regexp.MustCompile(`and colname BETWEEN :colname_low AND :colname_high\n`),
			qParams: QueryParams{"colname_low": int32(123), "colname_high": int32(345)},
		},
		"between []int16": {
			param:   "colname",
			joiner:  "and",
			vals:    []int16{123, 345},
			expect:  regexp.MustCompile(`and colname BETWEEN :colname_low AND :colname_high\n`),
			qParams: QueryParams{"colname_low": int16(123), "colname_high": int16(345)},
		},
		"between []int8": {
			param:   "colname",
			joiner:  "and",
			vals:    []int8{12, 34},
			expect:  regexp.MustCompile(`and colname BETWEEN :colname_low AND :colname_high\n`),
			qParams: QueryParams{"colname_low": int8(12), "colname_high": int8(34)},
		},
		"between time": {
			param:   "colname",
			joiner:  "and",
			vals:    []time.Time{now, now.AddDate(0, 0, 1)},
			expect:  regexp.MustCompile(`and colname BETWEEN :colname_low AND :colname_high\n`),
			qParams: QueryParams{"colname_low": now, "colname_high": now.AddDate(0, 0, 1)},
		},
		"empty params string": {
			param:  "colname",
			joiner: "where",
			vals:   "",
			err:    NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenStringHasTwoVals, "colname"), 0),
		},
		"too few params string": {
			param:  "colname",
			joiner: "where",
			vals:   "one",
			err:    NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenStringHasTwoVals, "colname"), 1),
		},
		"too many params string": {
			param:  "colname",
			joiner: "where",
			vals:   "one, two, three",
			err:    NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenStringHasTwoVals, "colname"), 3),
		},
		"empty params array": {
			param:  "colname",
			joiner: "where",
			vals:   []int{},
			err:    NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenArrayHasTwoVals, "colname"), 0),
		},
		"too few params array": {
			param:  "colname",
			joiner: "where",
			vals:   []int8{1},
			err:    NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenArrayHasTwoVals, "colname"), 1),
		},
		"too many params array": {
			param:  "colname",
			joiner: "where",
			vals:   []int16{1, 2, 3},
			err:    NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenArrayHasTwoVals, "colname"), 3),
		},
		"bad bounds string": {
			param:  "colname",
			joiner: "where",
			vals:   "zyx,abc",
			err:    NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%s < %s", "abc", "zyx")),
		},
		"bad bounds []string": {
			param:  "colname",
			joiner: "where",
			vals:   []string{"zyx", "abc"},
			err:    NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%s < %s", "abc", "zyx")),
		},
		"bad bounds []int": {
			param:  "colname",
			joiner: "where",
			vals:   []int{9, 1},
			err:    NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%d < %d", 1, 9)),
		},
		"bad bounds []time": {
			param:  "colname",
			joiner: "where",
			vals:   []time.Time{now.AddDate(0, 0, 1), now},
			err:    NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%v < %v", now, now.AddDate(0, 0, 1))),
		},
	}

	for tn, tc := range testCases {
		t.Logf("---- running: %s ----", tn)

		clause, newParams, err := r.betweenClause(tc.param, tc.joiner, tc.vals)
		if tc.err != nil {
			require.Error(err, "error should be returned when expected")
			require.Equal(tc.err, err, "expected error should be returned")
			continue
		}
		require.NoError(err, "should not error when not expected")
		require.Regexp(tc.expect, clause, "should generate the expected clause")
		require.EqualValues(tc.qParams, newParams, "should return the expected query parameters")
	}
}

func TestSQLFromParamsAndOperator(t *testing.T) {
	require := require.New(t)

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	r := &Base{
		Logger:    l,
		Tx:        nil,
		TableName: "blah",
	}

	r.Init(&TestTableB{})
	t.Logf("--- %+v", r.allColumns)

	initialQueryWhere := "SELECT * FROM blah WHERE 1 = 1"
	initialQueryNoWhere := "SELECT * FROM blah"
	initQueryWhereEsc := regexp.QuoteMeta(initialQueryWhere)
	initQueryNoWhereEsc := regexp.QuoteMeta(initialQueryNoWhere)

	testCases := map[string]struct {
		baseSQL string
		params  []Restriction
		options *Options
		expect  *regexp.Regexp
		qParams QueryParams
		err     error
	}{
		"single param with WHERE and default operator": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: "xyz"}},
			expect:  regexp.MustCompile(`(?is)` + initQueryWhereEsc + `\s+AND colname = :colname`),
			qParams: QueryParams{"colname": "xyz"},
		},
		"single param with no WHERE and default operator": {
			baseSQL: initialQueryNoWhere,
			params:  []Restriction{{Column: "colname", Value: "xyz"}},
			expect:  regexp.MustCompile(`(?is)` + initQueryNoWhereEsc + `\s+WHERE colname = :colname`),
			qParams: QueryParams{"colname": "xyz"},
		},
		"multiple params with no WHERE": {
			baseSQL: initialQueryNoWhere,
			params:  []Restriction{{Column: "colname", Value: "xyz"}, {Column: "othercol", Value: 100, Operator: OperatorLessThanEqualTo}},
			expect:  regexp.MustCompile(`(?is)` + initQueryNoWhereEsc + `\s+WHERE colname = :colname\s+AND othercol <= :othercol`),
			qParams: QueryParams{"colname": "xyz", "othercol": 100},
		},
		"single param with WHERE and default array operator": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: []string{"xyz", "wvu", "tsr"}}},
			expect:  regexp.MustCompile(`(?is)` + initQueryWhereEsc + `\s+AND colname IN \(:colname\d, :colname\d, :colname\d\)`),
			qParams: QueryParams{"colname0": "xyz", "colname1": "wvu", "colname2": "tsr"},
		},
		"single param with WHERE and between operator": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: []string{"abc", "xyz"}, Operator: OperatorBetween}},
			expect:  regexp.MustCompile(`(?is)` + initQueryWhereEsc + `\s+AND colname between :colname_low AND :colname_high`),
			qParams: QueryParams{"colname_low": "abc", "colname_high": "xyz"},
		},
		"single param with WHERE and unary operator": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: "xyz"}, {Column: "colnull", Operator: OperatorIsNull}},
			expect:  regexp.MustCompile(`(?is)` + initQueryWhereEsc + `\s+AND colname = :colname\s+AND colnull is null`),
			qParams: QueryParams{"colname": "xyz"},
		},
		"single param with WHERE and unary operator with unused value": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: "xyz"}, {Column: "colnull", Operator: OperatorIsNotNull, Value: "abc"}},
			expect:  regexp.MustCompile(`(?is)` + initQueryWhereEsc + `\s+AND colname = :colname\s+AND colnull is not null`),
			qParams: QueryParams{"colname": "xyz"},
		},
		"single param with WHERE and array operator": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: "xyz"}, {Column: "colarray", Value: 2, Operator: OperatorArrayContains}},
			expect:  regexp.MustCompile(`(?is)` + initQueryWhereEsc + `\s+AND colname = :colname\s+AND :colarray = any\(colarray\)`),
			qParams: QueryParams{"colname": "xyz", "colarray": 2},
		},
		"single param with WHERE and option sort asc": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: "xyz"}},
			options: &Options{OrderBy: []Ordering{{"col1", "asc"}, {Column: "col2"}, {"col3", "asc"}}},
			expect:  regexp.MustCompile(`(?is)` + initQueryWhereEsc + `\s+AND colname = :colname\s+order by col1 ASC, col2, col3 ASC`),
			qParams: QueryParams{"colname": "xyz"},
		},
		"single param with WHERE and option sort desc": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: "xyz"}},
			options: &Options{OrderBy: []Ordering{{"col1", "desc"}, {"col2", "desc"}, {"col3", "desc"}}},
			expect:  regexp.MustCompile(`(?is)` + initQueryWhereEsc + `\s+AND colname = :colname\s+order by col1 DESC, col2 DESC, col3 DESC`),
			qParams: QueryParams{"colname": "xyz"},
		},
		"single param with WHERE and option sort mixed": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: "xyz"}},
			options: &Options{OrderBy: []Ordering{{"col1", "desc"}, {"col2", "asc"}, {"col3", ""}}},
			expect:  regexp.MustCompile(`(?is)` + initQueryWhereEsc + `\s+AND colname = :colname\s+order by col1 desc, col2 asc, col3`),
			qParams: QueryParams{"colname": "xyz"},
		},
		"unsupported type for params": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: map[string]string{"col": "value"}}},
			err:     NewRepoError(r.TableName, "value type not supported", "map"),
		},
		"single param with WHERE and LIMIT and OFFSET options": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: "xyz"}},
			options: &Options{Limit: 1, Offset: 2},
			expect:  regexp.MustCompile(`(?is)` + initQueryWhereEsc + `\s+AND colname = :colname\s+LIMIT 1\s+OFFSET 2`),
			qParams: QueryParams{"colname": "xyz"},
		},
		"single param with WHERE and ForUpdate option": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: "xyz"}},
			options: &Options{ForUpdate: "skip"},
			expect:  regexp.MustCompile(`(?is)` + initQueryWhereEsc + `\s+AND colname = :colname\s+ for no key update skip locked`),
			qParams: QueryParams{"colname": "xyz"},
		},
		"invalid column for params": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "xCol", Value: "value"}},
			err:     NewRepoError(r.TableName, ErrInvalidColumn, "xCol"),
		},
		"invalid column for ordering": {
			baseSQL: initialQueryWhere,
			params:  []Restriction{{Column: "colname", Value: "value"}},
			options: &Options{OrderBy: []Ordering{{"colX", "desc"}}},
			err:     NewRepoError(r.TableName, ErrInvalidColumn, "colX"),
		},
	}

	for tn, tc := range testCases {
		t.Logf("---- running: %s ----", tn)

		sql, qparams, err := r.sqlFromParamsAndOperator(tc.baseSQL, tc.params, tc.options)
		if tc.err != nil {
			require.Error(err, "error should be returned when expected")
			require.Equal(tc.err, err, "expected error should be returned")
			continue
		}

		require.NoError(err, "error should not be returned when not expected")
		require.Regexp(tc.expect, sql, "should return the expected SQL")
		require.EqualValues(tc.qParams, qparams, "should return the expected query parameters")
	}
}
