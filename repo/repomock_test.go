// +build !test_integration

package repo

import (
	"database/sql"
	"os"
	"testing"
	"time"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
)

func TestCompareParams(t *testing.T) {

	l := zerolog.New(os.Stdout).With().Timestamp().Logger()

	bm := BaseMock{
		Logger: l,
	}

	type NestedCols struct {
		NString string `db:"n_string"`
		NInt    int    `db:"n_int"`
	}
	type TestRecord struct {
		AString     string         `db:"a_string"`
		ABool       bool           `db:"a_bool"`
		AnInt       int            `db:"an_int"`
		ANullString sql.NullString `db:"a_null_string"`
		ATime       time.Time      `db:"a_time"`
		ANullTime   sql.NullTime   `db:"a_null_time"`
		NestedCols
	}

	now := time.Now().UTC()

	tRec := &TestRecord{
		AString:     "aString",
		ABool:       false,
		AnInt:       50,
		ANullString: sql.NullString{Valid: true, String: "Sql Null String"},
		ATime:       now,
		ANullTime:   sql.NullTime{Valid: true, Time: now},
		NestedCols: NestedCols{
			NString: "nString",
			NInt:    75,
		},
	}

	testCases := map[string]struct {
		params []Restriction
		match  bool
	}{
		"string equality match": {
			params: []Restriction{{Column: "a_string", Value: "aString"}},
			match:  true,
		},
		"string equality non-match": {
			params: []Restriction{{Column: "a_string", Value: "bString", Operator: OperatorEqual}},
			match:  false,
		},
		"bool equality match": {
			params: []Restriction{{Column: "a_bool", Value: false}},
			match:  true,
		},
		"bool equality non-match": {
			params: []Restriction{{Column: "a_bool", Value: true, Operator: OperatorEqual}},
			match:  false,
		},
		"int equality match": {
			params: []Restriction{{Column: "an_int", Value: 50}},
			match:  true,
		},
		"int equality non-match": {
			params: []Restriction{{Column: "an_int", Value: 100}},
			match:  false,
		},
		"nullString equality match": {
			params: []Restriction{{Column: "a_null_string", Value: sql.NullString{Valid: true, String: "Sql Null String"}}},
			match:  true,
		},
		"nullString equality non-match": {
			params: []Restriction{{Column: "a_null_string", Value: sql.NullString{Valid: true, String: "Null Sql String"}}},
			match:  false,
		},
		"time equality match": {
			params: []Restriction{{Column: "a_time", Value: now}},
			match:  true,
		},
		"time equality non-match": {
			params: []Restriction{{Column: "a_time", Value: now.AddDate(0, 0, 1)}},
			match:  false,
		},
		"nullTime equality match": {
			params: []Restriction{{Column: "a_null_time", Value: now}},
			match:  true,
		},
		"nullTime equality non-match": {
			params: []Restriction{{Column: "a_null_time", Value: now.AddDate(0, 0, 1)}},
			match:  false,
		},

		"string less-than-equal match": {
			params: []Restriction{{Column: "a_string", Value: "bString", Operator: OperatorLessThanEqualTo}},
			match:  true,
		},
		"string less-than-equal non-match": {
			params: []Restriction{{Column: "a_string", Value: "aSting", Operator: OperatorLessThanEqualTo}},
			match:  false,
		},
		"int greater-than-equal match": {
			params: []Restriction{{Column: "an_int", Value: 9, Operator: OperatorGreaterThanEqualTo}},
			match:  true,
		},
		"int greater-than-equal non-match": {
			params: []Restriction{{Column: "an_int", Value: 100, Operator: OperatorGreaterThanEqualTo}},
			match:  false,
		},
		"nullString greater-than match": {
			params: []Restriction{{
				Column:   "a_null_string",
				Value:    sql.NullString{Valid: true, String: "Rql Null String"},
				Operator: OperatorGreaterThan,
			}},
			match: true,
		},
		"nullString greater-than non-match": {
			params: []Restriction{{
				Column:   "a_null_string",
				Value:    sql.NullString{Valid: true, String: "Sql Null String"},
				Operator: OperatorGreaterThan,
			}},
			match: false,
		},
		"time less-than match": {
			params: []Restriction{{Column: "a_time", Value: now.Add(1 * time.Second), Operator: OperatorLessThan}},
			match:  true,
		},
		"time less-than non-match": {
			params: []Restriction{{Column: "a_time", Value: now.Add(-1 * time.Second), Operator: OperatorLessThan}},
			match:  false,
		},
		"nullTime less-than match": {
			params: []Restriction{{Column: "a_null_time", Value: now.Add(1 * time.Second), Operator: OperatorLessThan}},
			match:  true,
		},
		"nullTime less-than non-match": {
			params: []Restriction{{Column: "a_null_time", Value: now.Add(-1 * time.Second), Operator: OperatorLessThan}},
			match:  false,
		},
		"string in match": {
			params: []Restriction{{Column: "a_string", Value: "aSting,aString,aStrong", Operator: OperatorIn}},
			match:  true,
		},
		"string in non-match": {
			params: []Restriction{{Column: "a_string", Value: "aSting,aStrong,aStrung", Operator: OperatorIn}},
			match:  false,
		},
		"string not in match (not in operator)": {
			params: []Restriction{{Column: "a_string", Value: "aSting,aStrong,aStrung", Operator: OperatorNotIn}},
			match:  true,
		},
		"string in match (not in operator)": {
			params: []Restriction{{Column: "a_string", Value: "aSting,aStrong,aString", Operator: OperatorNotIn}},
			match:  false,
		},
		"string in array match": {
			params: []Restriction{{Column: "a_string", Value: []string{"aSting", "aString", "aStrong"}}},
			match:  true,
		},

		"multiple column match": {
			params: []Restriction{
				{Column: "a_string", Value: "aString"},
				{Column: "an_int", Value: 50},
			},
			match: true,
		},
		"multiple column partial match": {
			params: []Restriction{
				{Column: "a_string", Value: "aString"},
				{Column: "an_int", Value: 55},
			},
			match: false,
		},
		"match nested column": {
			params: []Restriction{{Column: "n_string", Value: "nString"}},
			match:  true,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			foundRec, err := bm.CompareParams(tc.params, tRec)
			require.NoError(t, err, "CompareParams should not return an error")
			if tc.match {
				require.True(t, foundRec, "should find a record when expected")
			} else {
				require.False(t, foundRec, "should not find a record when not expected")
			}
		})
	}
}
