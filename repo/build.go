package repo

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"time"

	"github.com/lib/pq"
)

// package constants for builder
const (
	// Operators -
	OperatorEqual              = "="  // default operator for non-array values
	OperatorIn                 = "in" // default operator for array values
	OperatorNotIn              = "not in"
	OperatorBetween            = "between"
	OperatorLike               = "like"
	OperatorIsNull             = "is null"
	OperatorIsNotNull          = "is not null"
	OperatorNotEqualTo         = "!="
	OperatorLessThanEqualTo    = "<="
	OperatorLessThan           = "<"
	OperatorGreaterThanEqualTo = ">="
	OperatorGreaterThan        = ">"
	OperatorArrayContains      = "@>"
	OperatorArrayContainedIn   = "<@"

	// Clause Options -
	ClauseOrderByAscending  = "order_by_asc"
	ClauseOrderByDescending = "order_by_desc"
	ClauseOrderBy           = "order_by"
	ClauseForUpdate         = "for_update"
	ClauseLimit             = "limit"
	ClauseOffset            = "offset"

	// for update lock options
	LockOptionSkip   = "skip"
	LockOptionNoWait = "nowait"

	// error details
	ErrBetweenStringHasTwoVals = "String value for between operator on >%s< should have 2 values separated by a comma"
	ErrBetweenArrayHasTwoVals  = "Array value for between operator on >%s< should have exactly 2 values"
	ErrBetweenBounds           = "Upper bound must not be less than lower bound for between operator"
)

// list of supported operators
var operatorList = []string{OperatorEqual, OperatorIn, OperatorBetween, OperatorLike, OperatorIsNull, OperatorIsNotNull,
	OperatorNotEqualTo, OperatorLessThanEqualTo, OperatorLessThan, OperatorGreaterThanEqualTo, OperatorGreaterThan,
	OperatorArrayContains}

// list of unary operators
var unaryOperators = []string{OperatorIsNull, OperatorIsNotNull}

// list of operators that support array values
var arrayOperators = []string{OperatorIn, OperatorBetween}

// lock options
var updateOptions = map[string]string{
	"nowait":  ForUpdateNowait,
	"skip":    ForUpdateSkip,
	"default": ForUpdateNowait,
}

// dirRE is a reqular expression to determine if an order by direction is specified
var dirRE = regexp.MustCompile(`(?iU)^.+sc`)

// ErrOperatorNotSupported is the error for an unrecognised operator specification in repo.Restriction
func (r *Base) ErrOperatorNotSupported(op string) *Error {
	return NewRepoError(r.TableName, "operator not supported", op)
}

// ErrEmptyParam is the error for a missing repo.Restriction Value when one is expected
func (r *Base) ErrEmptyParam(param string) *Error {
	return NewRepoError(r.TableName, "value for param is empty", param)
}

// sqlFromParamsAndOperator appends clauses to a base sql statement for the provided parameters and options
//  inputs:
//  - initialSQL: statement to which clauses are appended
//  - params:     restriction clause specifinq column, operator and values
//  - options:    values for additional clauses e.g. order by, limit
//  returns:
//  - string:                 the generated SQL statement
//  - map[string]inetrface{}; parameter list to pass to execution call
//  - error:                  any error encountered
func (r *Base) sqlFromParamsAndOperator(
	initialSQL string,
	params []Restriction,
	options *Options,
) (string, QueryParams, error) {

	l := r.Logger.With().Str("function", "sqlFromParamsAndOperator").Logger()

	whereRE := regexp.MustCompile(`(?i)\sWHERE\s`)

	joiner := "WHERE"
	if whereRE.MatchString(initialSQL) {
		joiner = "AND"
	}

	var sqlStmt strings.Builder
	sqlStmt.WriteString(strings.TrimSpace(initialSQL))
	sqlStmt.WriteByte('\n')

	// container for params to be passed to the query
	queryParams := QueryParams{}
	var err error

	for _, param := range params {
		var clause string
		newParams := QueryParams{}

		if !r.hasColumn(param.Column) {
			l.Warn().Str("column", param.Column).Msg(ErrInvalidColumn)
			return "", nil, NewRepoError(r.TableName, ErrInvalidColumn, param.Column)
		}

		if param.Operator == "" {
			pvrv := reflect.ValueOf(param.Value)

			switch pvrv.Kind() {
			case reflect.Array, reflect.Slice:
				param.Operator = OperatorIn
			case reflect.Chan, reflect.Func, reflect.Map:
				l.Warn().Str("type", pvrv.Kind().String()).Msg("Unsupported type in value")
				return "", nil, NewRepoError(r.TableName, "value type not supported", pvrv.Kind().String())
			default:
				param.Operator = OperatorEqual
			}
		}

		switch param.Operator {
		case OperatorEqual:
			clause = fmt.Sprintf("%s %s %s :%s\n", joiner, param.Column, param.Operator, param.Column)
		case OperatorIn, OperatorNotIn:
			clause, newParams, err = r.inClause(param.Operator, param.Column, joiner, param.Value)
			if err != nil {
				return "", nil, err
			}
		case OperatorBetween:
			clause, newParams, err = r.betweenClause(param.Column, joiner, param.Value)
			if err != nil {
				return "", nil, err
			}
		case OperatorNotEqualTo, OperatorLessThanEqualTo, OperatorGreaterThanEqualTo, OperatorLessThan, OperatorGreaterThan, OperatorLike:
			clause = fmt.Sprintf("%s %s %s :%s\n", joiner, param.Column, param.Operator, param.Column)
		case OperatorArrayContains:
			clause = fmt.Sprintf("%s :%s = ANY(%s)\n", joiner, param.Column, param.Column)
		case OperatorIsNull, OperatorIsNotNull:
			clause = fmt.Sprintf("%s %s %s\n", joiner, param.Column, param.Operator)
			if param.Value != nil {
				l.Warn().Str("value", fmt.Sprintf("%+v", param.Value)).Msg("Value provided for unary operator - will be ignored")
				param.Value = nil
			}

		default:
			l.Warn().Str("operator", param.Operator).Str("column", param.Column).Msg("Unsupported operator")
			return "", nil, r.ErrOperatorNotSupported(param.Operator)
		}

		sqlStmt.WriteString(clause)
		joiner = "AND"

		if len(newParams) > 0 {
			for k, v := range newParams {
				queryParams[k] = v
			}
		} else if param.Value != nil {
			queryParams[param.Column] = param.Value
		}
	}

	// handle options

	// ORDER BY
	orderer := "ORDER BY"
	if options != nil && len(options.OrderBy) > 0 {
		for i, order := range options.OrderBy {
			if !r.hasColumn(order.Column) {
				l.Warn().Str("column", order.Column).Msg(ErrInvalidColumn)
				return "", nil, NewRepoError(r.TableName, ErrInvalidColumn, order.Column)
			}

			if i > 0 {
				orderer = ","
			}
			dir := strings.ToUpper(dirRE.FindString(order.Direction))
			if (dir != "" && dir != "ASC" && dir != "DESC") || (dir == "" && order.Direction != "") {
				dir = "ASC"
				l.Warn().Msgf("Unsupported value given for sort direction >%s<, defauting to >%s<", order.Direction, dir)
			}
			sqlStmt.WriteString(strings.TrimSpace(fmt.Sprintf("%s %s %s", orderer, order.Column, order.Direction)))
		}
		sqlStmt.WriteByte('\n')
	} else if r.OrderByFunc != nil {
		sqlStmt.WriteString(fmt.Sprintf("%s %s\n", orderer, r.OrderByFunc()))
	}

	// FOR UPDATE
	if options != nil && options.ForUpdate != "" {
		if str, ok := updateOptions[options.ForUpdate]; ok {
			sqlStmt.WriteString(str)
		} else {
			// use repo default lock option
			sqlStmt.WriteString(r.LockOption)
		}
		sqlStmt.WriteByte('\n')
	}

	// LIMIT
	if options != nil && options.Limit != 0 {
		sqlStmt.WriteString(fmt.Sprintf("LIMIT %d\n", options.Limit))
	}

	// OFFSET
	if options != nil && options.Offset != 0 {
		sqlStmt.WriteString(fmt.Sprintf("OFFSET %d\n", options.Offset))
	}

	return sqlStmt.String(), queryParams, nil
}

// betweenClause handles the between operator conditions
func (r *Base) betweenClause(param, joiner string, val interface{}) (string, QueryParams, error) {
	var clause strings.Builder
	newParams := QueryParams{}
	lowerBound := param + "_low"
	upperBound := param + "_high"

	switch vt := val.(type) {
	// only handles strings as a list, ie passing "1,2,3" would be compared as ["1","2","3"]
	case string:
		if vt == "" {
			return "", nil, NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenStringHasTwoVals, param), 0)
		}
		split := strings.Split(vt, ",")
		if len(split) != 2 {
			return "", nil, NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenStringHasTwoVals, param), len(split))
		}
		newParams[lowerBound] = strings.TrimSpace(split[0])
		newParams[upperBound] = strings.TrimSpace(split[1])

		if split[1] < split[0] {
			return "", nil, NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%v < %v", split[1], split[0]))
		}

	case []string:
		if len(vt) != 2 {
			return "", nil, NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenArrayHasTwoVals, param), len(vt))
		}
		newParams[lowerBound] = vt[0]
		newParams[upperBound] = vt[1]

		if vt[1] < vt[0] {
			return "", nil, NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%v < %v", vt[1], vt[0]))
		}

	case pq.StringArray:
		if len(vt) != 2 {
			return "", nil, NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenArrayHasTwoVals, param), len(vt))
		}
		newParams[lowerBound] = vt[0]
		newParams[upperBound] = vt[1]

		if vt[1] < vt[0] {
			return "", nil, NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%v < %v", vt[1], vt[0]))
		}

	case []int64:
		if len(vt) != 2 {
			return "", nil, NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenArrayHasTwoVals, param), len(vt))
		}
		newParams[lowerBound] = vt[0]
		newParams[upperBound] = vt[1]

		if vt[1] < vt[0] {
			return "", nil, NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%v < %v", vt[1], vt[0]))
		}

	case []int32:
		if len(vt) != 2 {
			return "", nil, NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenArrayHasTwoVals, param), len(vt))
		}
		newParams[lowerBound] = vt[0]
		newParams[upperBound] = vt[1]

		if vt[1] < vt[0] {
			return "", nil, NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%v < %v", vt[1], vt[0]))
		}

	case []int16:
		if len(vt) != 2 {
			return "", nil, NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenArrayHasTwoVals, param), len(vt))
		}
		newParams[lowerBound] = vt[0]
		newParams[upperBound] = vt[1]

		if vt[1] < vt[0] {
			return "", nil, NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%v < %v", vt[1], vt[0]))
		}

	case []int8:
		if len(vt) != 2 {
			return "", nil, NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenArrayHasTwoVals, param), len(vt))
		}
		newParams[lowerBound] = vt[0]
		newParams[upperBound] = vt[1]

		if vt[1] < vt[0] {
			return "", nil, NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%v < %v", vt[1], vt[0]))
		}

	case []int:
		if len(vt) != 2 {
			return "", nil, NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenArrayHasTwoVals, param), len(vt))
		}
		newParams[lowerBound] = vt[0]
		newParams[upperBound] = vt[1]

		if vt[1] < vt[0] {
			return "", nil, NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%v < %v", vt[1], vt[0]))
		}

	case []time.Time:
		if len(vt) != 2 {
			return "", nil, NewRepoError(r.TableName, fmt.Sprintf(ErrBetweenArrayHasTwoVals, param), len(vt))
		}
		newParams[lowerBound] = vt[0]
		newParams[upperBound] = vt[1]

		if vt[1].Before(vt[0]) {
			return "", nil, NewRepoError(r.TableName, ErrBetweenBounds, fmt.Sprintf("%v < %v", vt[1], vt[0]))
		}

	default:
		return "", nil, NewRepoError(r.TableName, "unhandled type for use with between operator", fmt.Sprintf("%T", vt))
	}

	clause.WriteString(fmt.Sprintf("%s %s BETWEEN :%s AND :%s\n", joiner, param, lowerBound, upperBound))

	return clause.String(), newParams, nil
}

// inClause processes various array types into an IN clause list
func (r *Base) inClause(op, param, joiner string, vals interface{}) (string, QueryParams, error) {
	newParams := QueryParams{}

	switch vt := vals.(type) {
	// are handling each type separately to be able to easily iterate over the array
	case string:
		if vt != "" {
			sVals := strings.Split(vt, ",")
			for idx, val := range sVals {
				paramName := fmt.Sprintf("%s%d", param, idx)
				newParams[paramName] = strings.TrimSpace(val)
			}
		}
	case []string:
		for idx, val := range vt {
			paramName := fmt.Sprintf("%s%d", param, idx)
			newParams[paramName] = val
		}
	case pq.StringArray:
		for idx, val := range vt {
			paramName := fmt.Sprintf("%s%d", param, idx)
			newParams[paramName] = val
		}
	case []int64:
		for idx, val := range vt {
			paramName := fmt.Sprintf("%s%d", param, idx)
			newParams[paramName] = val
		}
	case []int32:
		for idx, val := range vt {
			paramName := fmt.Sprintf("%s%d", param, idx)
			newParams[paramName] = val
		}
	case []int16:
		for idx, val := range vt {
			paramName := fmt.Sprintf("%s%d", param, idx)
			newParams[paramName] = val
		}
	case []int8:
		for idx, val := range vt {
			paramName := fmt.Sprintf("%s%d", param, idx)
			newParams[paramName] = val
		}
	case []int:
		for idx, val := range vt {
			paramName := fmt.Sprintf("%s%d", param, idx)
			newParams[paramName] = val
		}
	}

	if len(newParams) == 0 {
		return "", nil, r.ErrEmptyParam(param)
	}

	var clause strings.Builder
	counter := 0
	clause.WriteString(fmt.Sprintf("%s %s %s (", joiner, param, op))
	for key := range newParams {
		if counter == 0 {
			clause.WriteString(fmt.Sprintf(":%s", key))
		} else {
			clause.WriteString(fmt.Sprintf(", :%s", key))
		}
		counter++
	}
	clause.WriteString(")\n")

	return clause.String(), newParams, nil
}
