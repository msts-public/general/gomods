package repo

// Restriction specifies a column restriction to apply to a query
type Restriction struct {
	Column   string
	Operator string
	Value    interface{}
}

// Ordering specifies columns to order query results by
type Ordering struct {
	Column    string
	Direction string // should only be ASC or DESC
}

// Options specifies options for non-restriction clauses to apply to a query
type Options struct {
	OrderBy   []Ordering
	ForUpdate string
	Limit     int
	Offset    int
}

// QueryParams is the format in which parameters are provided to the database layer for queries
type QueryParams = map[string]interface{}

// ColumnFilters specifies columns to exclude from lists and statements
type ColumnFilters struct {
	Create map[string]*string
	Update map[string]*string
	Return map[string]*string
}
