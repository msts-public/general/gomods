package repo

import (
	"database/sql"

	"github.com/jackc/pgtype"
)

type TestTable struct {
	ID        string         `db:"id"`
	ColStr    string         `db:"col_string"`
	ColInt    int            `db:"col_int"`
	ColUUID   pgtype.UUID    `db:"col_uuid"`
	CreatedAt string         `db:"created_at"`
	UpdatedAt string         `db:"updated_at"`
	DeletedAt sql.NullString `db:"deleted_at"`
}

func setCols() map[string]Columns {
	cols := GetColumns(TestTable{})

	var createCols, updateCols, returnCols Columns

	for _, c := range cols {
		switch c {
		case "id", "created_at":
			createCols = append(createCols, c)
			returnCols = append(returnCols, c)
		case "deleted_at":
			returnCols = append(returnCols, c)
		default:
			createCols = append(createCols, c)
			updateCols = append(updateCols, c)
			returnCols = append(returnCols, c)
		}
	}

	return map[string]Columns{
		ColumnsCreate: createCols,
		ColumnsUpdate: updateCols,
		ColumnsReturn: returnCols,
	}
}
