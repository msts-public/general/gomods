// +build !test_integration

package repo

import (
	"testing"

	// "github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
)

func TestToList(t *testing.T) {
	require := require.New(t)

	cols := Columns{"col1", "col2", "col3"}
	require.Equal("col1, col2, col3", cols.ToList(), "should get expected list")

	require.Equal("a.col1, a.col2, a.col3", cols.ToPrefixedList("a."), "should get expected list")

	require.Equal("col1 = :col1,\n\t  col2 = :col2,\n\t  col3 = :col3", cols.ToUpdateList(), "should get expected list")
}

type TestRec struct {
	ID   string `db:"id"`
	Col1 string `db:"co1_1"`
	Col2 string
	Col3 string `json:"col_3"`
	Col4 string `db:"col_4"`
}

type nestedRec struct {
	TestRec
	Col5 int `db:"col_5"`
}

func TestGetColumns(t *testing.T) {
	require := require.New(t)

	tr := TestRec{}
	cols := GetColumns(tr)
	require.NotEmpty(cols, "should get an array of values")
	require.Len(cols, 3, "should return all columns with 'db' tag")

	cols2 := GetColumns(&tr)
	require.NotEmpty(cols2, "should get an array of values for pointer to struct")

	cols3 := GetColumns("tr")
	require.Empty(cols3, "should get an empty array for non-struct input")

	ntr := nestedRec{}
	ncols := GetColumns(ntr)
	require.NotEmpty(ncols, "should get an array of values")
	require.Len(ncols, 4, "should return all columns with 'db' tag in nested struct")
}
